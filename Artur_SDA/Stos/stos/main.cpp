/*
 * main.cpp
 *
 *  Created on: 12.04.2017
 *      Author: Admin
 */


#include <iostream>

const int maksymalnyRozmiarStosu = 5;

struct Stos
{
	int wierzcholek; //pozycja gdzie zapisac nastepny element
	int dane[maksymalnyRozmiarStosu];
};

int main()
{
	Stos liczby;

	liczby.wierzcholek = 0;

	for (int i = 0; i < maksymalnyRozmiarStosu; ++i)
	{

		liczby.dane[i] = 0;
	}

	std::cout << "rozmiar " << liczby.wierzcholek << std::endl;

	// wstawic liczbe na sztyt stosu
	liczby.dane[liczby.wierzcholek] = 4; // zapisz dane na wierzcholku
	liczby.wierzcholek += 1; // przesuwam wierzcholek o jeden

	// zobacz co jest na wierzchu stosu
	int wierzch = liczby.dane[liczby.wierzcholek - 1]; //ostatni wstawiony element ktory znajduje sie o jeden niizej niz miejsce na nowy element
	std::cout << "wierzcholek " << wierzch << std::endl;

	std::cout << "rozmiar " << liczby.wierzcholek << std::endl;

	// wstawic liczbe na sztyt stosu
	liczby.dane[liczby.wierzcholek] = 7; // zapisz dane na wierzcholku
	liczby.wierzcholek += 1; // przesuwam wierzcholek o jeden
	std::cout << "rozmiar " << liczby.wierzcholek << std::endl;

	// zobacz co jest na wierzchu stosu
	int wierzch2 = liczby.dane[liczby.wierzcholek - 1];
	std::cout << "wierzcholek " << wierzch2 << std::endl;

	// zdjemi liczbe ze stosu

	liczby.wierzcholek -= 1; // przesuwam wierzcholek o jeden
	int zdjete = liczby.dane[liczby.wierzcholek]; // zapisz dane na wierzcholku

	std::cout << "rozmiar " << liczby.dane[liczby.wierzcholek - 1] << std::endl;

	return 0;
}

