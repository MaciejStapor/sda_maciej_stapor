#include <iostream>

const int MAKSYMALNY_ROZMIAR_KOLEJKI = 5;

class Kolejka
{
private:

    int dane[MAKSYMALNY_ROZMIAR_KOLEJKI];
    int koniec;

public:

    Kolejka(): dane{0,0,0,0,0},koniec(0)
    {
    }

    void pokazKolejke()
    {
        std::cout << koniec << " [";

        for (int i = 0; i < koniec; ++i)
        {
            std::cout << dane[i] << ' ';
        }

        std::cout << "]" << std::endl;
    }
    void wstaw(int nowaWartosc)
    {

        if (koniec>MAKSYMALNY_ROZMIAR_KOLEJKI-1)
        {
            std::cout << "pelna" << std::endl;
        }
        else
        {
            // nowa osoba do kolejki
            dane[koniec] = nowaWartosc;
            koniec += 1;
        }
    }


    void usuwanie ()
    {
        if (koniec > 0)
        {
            int obsluzony = dane[0];
            for (int i = 1; i < koniec; ++i)
            {
                dane[i-1] = dane[i];
            }
            koniec -= 1;
        }
    }
};





int main()
{

    Kolejka naNarty;

    naNarty.pokazKolejke();

    naNarty.wstaw(90);

    naNarty.pokazKolejke();

    naNarty.wstaw(76);

    naNarty.pokazKolejke();

    naNarty.wstaw(54);;

    naNarty.pokazKolejke();


    /* if (naNarty.koniec > 0)
     {
         int obsluzony = naNarty.dane[0];
         for (int i = 1; i < naNarty.koniec; ++i)
         {
             naNarty.dane[i-1] = naNarty.dane[i];
         }
         naNarty.koniec -= 1;
     }*/
    naNarty.usuwanie();

    naNarty.pokazKolejke();

    naNarty.wstaw(32);

    naNarty.pokazKolejke();

    naNarty.usuwanie();

    naNarty.pokazKolejke();

    naNarty.usuwanie();

    naNarty.pokazKolejke();

    naNarty.usuwanie();

    naNarty.pokazKolejke();

    return 0;
}
