#include <iostream>

const int MAKSYMALNY_ROZMIAR_STOSU = 5;

class Stos
{
private:
    int wierzcholek; // pozycja w tablicy "dane" - gdzie zapisa� nowy element
    int dane[MAKSYMALNY_ROZMIAR_STOSU];

public:

    Stos()
    {
        wierzcholek = 0;
        for (int i = 0; i < MAKSYMALNY_ROZMIAR_STOSU; ++i)
        {
            dane[i] = 0;
        }
    }


    int pokazRozmiar()
    {
        // poka� rozmiar stosu
        //std::cout << "Rozmiar: " << wierzcholek << std::endl;

        return wierzcholek;
    }

    int pokazWierzcholek()
    {
        // sprawd�, co jest na wierzchu stosu
        // je�li stos jest pusty, to poinformuj o tym
        if (wierzcholek == 0)
        {
            std::cout << "Stos jest pusty!" << std::endl;
            return 0 ;
        }
        else
        {
            // w przeciwnym wypadku - poka� wierzcho�ek
            // ostatni wstawiony element znajduje si� o 1 ni�ej ni� miejsce na nowy element


            int wierzch = dane[wierzcholek - 1];
            return wierzch;
            //std::cout << "Na g�rze jest: " << wierzch << std::endl;

        }
    }
    void wstaw(int nowaWartosc)
    {
        if (wierzcholek >= MAKSYMALNY_ROZMIAR_STOSU)
        {
            std::cerr << "Stos pe�ny!" << std::endl;
            return;
        }

        // wstawi� liczb� na szczyt stosu
        dane[wierzcholek] = nowaWartosc; // zapisz dane na wierzcho�ku stosu
        wierzcholek += 1;
        			 				// przesuwam wierzcho�ek
    }

    int zdejmij()
    {
        if (wierzcholek == 0)
        {
            std::cerr << "Pr�ba zdj�cia z pustego stosu!" << std::endl;
            return 0;
        }

        wierzcholek -= 1; // przesuwam wierzcho�ek na pierwszy istniej�cy element
        int zdjete = dane[wierzcholek]; // odczytaj dane z wierzchu stosu
        return zdjete;
    }

};







int main()
{
    // nowy stos
    Stos liczby;

    // zerowanie stosu


    //liczby.pokazRozmiar();
    std::cout << "Rozmiar: " << liczby.pokazRozmiar() << std::endl;

    liczby.wstaw( 4);

    //liczby.pokazWierzcholek();
    std::cout << "Na g�rze jest: " << liczby.pokazWierzcholek() << std::endl;

    std::cout << "Rozmiar: " << liczby.pokazRozmiar() << std::endl;

    liczby.wstaw( 7);

    std::cout << "Na g�rze jest: " << liczby.pokazWierzcholek() << std::endl;

    std::cout << "Rozmiar: " << liczby.pokazRozmiar() << std::endl;

    int zdjete1 = liczby.zdejmij();
    std::cout << "Zdj�te: " << zdjete1 << std::endl;

    std::cout << "Na g�rze jest: " << liczby.pokazWierzcholek() << std::endl;

    std::cout << "Rozmiar: " << liczby.pokazRozmiar() << std::endl;
    int zdjete2 = liczby.zdejmij();
    std::cout << "Zdj�te: " << zdjete2 << std::endl;
    std::cout << "Na g�rze jest: " << liczby.pokazWierzcholek() << std::endl;

    std::cout << "Rozmiar: " << liczby.pokazRozmiar() << std::endl;
    int zdjete3 = liczby.zdejmij();
    std::cout << "Zdj�te: " << zdjete3 << std::endl;

    liczby.wstaw( 42);
    liczby.wstaw( 128);
    liczby.wstaw( -1);
    liczby.wstaw( 8);
    liczby.wstaw( -4);
    liczby.wstaw(10);
    liczby.wstaw(100);

    std::cout << "Rozmiar: " << liczby.pokazRozmiar() << std::endl;
    std::cout << "Na g�rze jest: " << liczby.pokazWierzcholek() << std::endl;

    liczby.wstaw(100);

    return 0;
}



