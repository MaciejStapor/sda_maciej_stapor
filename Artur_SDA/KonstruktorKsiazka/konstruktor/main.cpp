#include <iostream>
#include <string>

class Ksiazka
{
public:
    Ksiazka()
    {
        tytul = "brak";
        autor = "brak";
        iloscStron = 0;
        cena = 0.0;
    }
    Ksiazka(std::string tytul2, std::string autor2, int iloscStron2,
            double cena2)
    {
        tytul = tytul2;
        autor = autor2;
        iloscStron = iloscStron2;
        cena = cena2;
    }

    void wypiszSie()
    {
        std::cout << "Tytu�: " << tytul << std::endl;
        std::cout << "Autor: " << autor << std::endl;
        std::cout << "Ilo�� stron: " << iloscStron << std::endl;
        std::cout << "Cena: " << cena << std::endl;
    }

private:
    std::string tytul;
    std::string autor;
    int iloscStron;
    double cena;
};

int main()
{
    const int ILOSC_KSIAZEK = 5;
    Ksiazka ksiazki[ILOSC_KSIAZEK];

    ksiazki[0] = Ksiazka ("Hobbit, czyli tam i z powrotem", "J.R.R. Tolkien",
                       300, 25.0);
    ksiazki[1] = Ksiazka ("Dru�yna pier�cienia", "J.R.R. Tolkien", 500,
                        40.0);
    ksiazki[2] = Ksiazka ("Kroniki Amberu", "Roger Zelazny", 550, 40.0);
    ksiazki[3] = Ksiazka ("Diuna", "F. Herbert", 600, 50.5);
    ksiazki[4] = Ksiazka ("Kroniki Jakuba W�drowycza", "Andrzej Pilipiuk",
                       350, 36.25);

    for (int i = 0; i < ILOSC_KSIAZEK; ++i)
    {
        ksiazki[i].wypiszSie();
    }

    return 0;
}
