#include <iostream>
#include <string>
using namespace std;

int main() {

	string rzym = "IX";
	int liczba = 0;

	for (unsigned i=0; i<=rzym.size(); i++)
	{
		if(rzym[i] == 'I')
		{
			if(rzym[i+1] == 'X' || rzym[i+1] == 'V')
			{
				liczba -= 1;
			}
			else
			{
				liczba += 1;
			}
		}
		else if(rzym[i] == 'V')
		{
			if(rzym[i+1] == 'L' || rzym[i+1] == 'X')
			{
				liczba -= 5;
			}
			else
			{
				liczba += 5;
			}
		}
		else if(rzym[i] == 'X')
		{
			if(rzym[i+1] == 'L' || rzym[i+1] == 'C')
			{
				liczba -= 10;
			}
			else
			{
				liczba += 10;
			}
		}
		else if(rzym[i] == 'L')
		{
			if(rzym[i+1] == 'C' || rzym[i+1] == 'D')
			{
				liczba -= 50;
			}
			else
			{
				liczba += 50;
			}
		}
		else if(rzym[i] == 'C')
		{
			if(rzym[i+1] == 'D' || rzym[i+1] == 'M')
			{
				liczba -= 100;
			}
			else
			{
				liczba += 100;
			}
		}
		else if(rzym[i] == 'D')
		{
			if(rzym[i+1] == 'M')
			{
				liczba -= 500;
			}
			else
			{
				liczba += 500;
			}
		}
		else if(rzym[i] == 'M')
		{
			liczba += 1000;
		}
	}

	cout << "Liczbya rzymska:  " << rzym << "  To liczba arabska:  "<< liczba << endl;


	return 0;
}
