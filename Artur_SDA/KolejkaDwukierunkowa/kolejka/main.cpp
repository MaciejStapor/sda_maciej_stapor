/*
 * main.cpp
 *
 *  Created on: 12.04.2017
 *      Author: Admin
 */


#include <iostream>
#include <cstdlib>
#include <windows.h>

const int maks = 10;



class KolejkaDwukierunkowa
{
private:

    int poczatek;
    int koniec;
    int dane[maks];

public:

    KolejkaDwukierunkowa ()
    {
        poczatek = 0;
        koniec = 0;
        for(int i = 0; i<maks; i++)
        {
            dane[i] = 0;
        }
    }

    void usuwaniePoczatek()
    {
        if(pusta())
        {
            std::cout << "Kolejka pusta!" << std::endl;
            Sleep (1000);
        }
        else
        {
            for(int i = 0; i<koniec ; i++)
            {
                dane[i] = dane[i+1];
            }
            koniec--;
        }
    }

    void  usuwanieKoniec()
    {

        if(pusta())
        {
            std::cout << "Kolejka pusta!" << std::endl;
            Sleep (1000);
        }
        else
        {
            koniec--;
        }
    }

    void dodawanieKoniec ()
    {

        int liczba;
        std::cout << "Podaj liczbe: ";
        std::cin >> liczba;

        if (pelna())
        {
            std::cout << "Kolejka pelna!" << std::endl;
            Sleep (1000);
        }
        else
        {

            dane[koniec]=liczba;
            koniec++;
        }
    }

    bool pelna()
    {
        if(koniec>=maks)return true;
        else return false;
    }
    bool pusta ()
    {

        if(koniec<=0)return true;
               else return false;
    }

    void dodawaniePoczatek ()
    {
        int liczba;
        std::cout << "Podaj liczbe: ";
        std::cin >> liczba;
        if (pelna())
        {
            std::cout << "Kolejka pelna!" << std::endl;
            Sleep (1000);
        }
        else
        {
            for(int i = koniec; i>=0; i--)
            {
                dane[i+1]=dane[i];

            }
            dane[poczatek]=liczba;
            koniec++;
        }
    }

    void pokaz ()
    {
        std::cout << ".........................................." <<std::endl;
        std::cout << "[ ";

        for(int i = 0 ; i<koniec ; i++)
        {
            std::cout << " " << dane[i] << " ";
        }

        std::cout << " ]" << std::endl;
        std::cout << ".........................................." <<std::endl;
    }

    void menu ()
    {
        std::cout << "Menu:" << std::endl;
        std::cout << "1. Dodaj z przodu: " << std::endl;
        std::cout << "2. Dodaj z tylu: " << std::endl;
        std::cout << "3. Odejmi z przodu: " << std::endl;
        std::cout << "4. Odejmi z tylu: " << std::endl;
        std::cout << "5. Wyjscie: " << std::endl;

    }

};

int main()
{
    KolejkaDwukierunkowa liczb;
    int pozycja;



    do
    {
        liczb.pokaz();

        liczb.menu();

        std::cout << " Wybierz pozycje z listy: ";
        std::cin >> pozycja;

        switch (pozycja)
        {

        case 1:
        {
            liczb.dodawaniePoczatek();
            system("cls");
            break;
        }
        case 2:
        {
            liczb.dodawanieKoniec();
            system("cls");
            break;
        }
        case 3:
        {
            liczb.usuwaniePoczatek();
            system("cls");
            break;
        }
        case 4:
        {
            liczb.usuwanieKoniec();
            system("cls");
            break;
        }
        case 5:
        {
            return -1;
        }
        default :
        {
            std::cout << "Niepoprawna wartosc" <<std::endl;
            Sleep (1000);
            system("cls");
            break;
        }
        }
    }
    while (pozycja != 5);



    return 0;
}

