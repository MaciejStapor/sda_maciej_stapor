/*
 * mian.cpp
 *
 *  Created on: 12.04.2017
 *      Author: Admin
 */
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <stdio.h>

//.............................................................
const int maksWielkosc = 10;

//..........................................................
struct Stos
{
    int  wierzcholek;
    int dane [maksWielkosc];
};
//................................................................
int dodawanie (Stos& dodawanieLiczb, int nowaLiczba)
{
    if(maksWielkosc<=dodawanieLiczb.wierzcholek)
    {
        std::cerr << "Stos pelen" << std::endl;
        Sleep(1000);
        return 0;
    }

    std::cout << "Podaj  liczbe ... ";
    std::cin >> nowaLiczba;

    dodawanieLiczb.dane[dodawanieLiczb.wierzcholek] = nowaLiczba;
    dodawanieLiczb.wierzcholek++;
    return nowaLiczba;

}
//......................................................................
void odejmowanie (Stos& odejmowanieLiczb)
{
    if(odejmowanieLiczb.wierzcholek<=0)
    {
        std::cout << " Stos pusty " << std::endl;
        Sleep(1000);
    }
    odejmowanieLiczb.wierzcholek--;
    odejmowanieLiczb.dane[odejmowanieLiczb.wierzcholek] = 0;
}
//........................................................................
void pokazStos (Stos& pokazywanie)
{
    std::cout<< "...................................................." << std::endl;
    std::cout<< "Stos liczb : " << std::endl;
    for (int i = 0 ; i < maksWielkosc ; ++i)
    {
        std::cout << pokazywanie.dane[i] << std::endl;
    }
    std::cout<< "....................................................." << std::endl;
}

int main()
{
    int wybor;
    Stos liczby;
    liczby.wierzcholek=0;
    for (int i = 0 ; i < maksWielkosc ; ++i)
    {
        liczby.dane[i] = 0 ;
    }

    do
    {
        pokazStos(liczby);
        std::cout << "Menu : " << std::endl;
        std::cout << "1. Dodawanie..." << std::endl;
        std::cout << "2. Odejmowanie..." << std::endl;
        std::cout << "3. Wyjscie..." << std::endl;
        std::cout << "Wybierz pozycje z listy : " ;
        std::cin >> wybor;

        switch (wybor)
        {
        case 1:
            dodawanie(liczby, 0);
            system("cls");
            break;
        case 2:
            odejmowanie(liczby);
            system("cls");
            break;
        case 3:
            std::cout << "Wyjscie ? Napewno ?" << std::endl;
            break;
        default:
            std::cout << "Blad !!" << std::endl;
            Sleep(1000);
            system("cls");

        }

    }
    while (wybor != 3);

    getchar();
    getchar();

    return 0;
}



