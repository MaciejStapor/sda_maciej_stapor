#include <iostream>
#include <cstdlib>
using namespace std;

int
main (int argc, char* argv[])
{

  int a = atoi (argv[1]);
  int b = atoi (argv[2]);

//      cout << "dodawanie  " << tak + liczba2 << endl;
//      cout << "odejmowanie  " << tak - liczba2 << endl;
//      cout << "mnozenie  " << tak * liczba2 << endl;
//      cout << "dzielenie  " << tak / liczba2 << endl;
//      cout << "reszta  " << tak % liczba2 << endl;
//      cout << "dodawanie  " << (tak <= liczba2) << endl;
//      cout << "porownywanie " << (tak >= liczba2) << endl;
//      cout << "porownywanie " << ( tak != liczba2) << endl;
//      cout << "inkrementacja dekrementacja " << --tak << ++liczba2 << endl;
//      cout << "przesuniecie bitowe  " <<(tak <<= liczba2) << endl;
//      cout << "przesuniecie bitowe  " <<(tak >>= liczba2) << endl;
//      cout << "przesuniecie bitowe lub " << (tak &= liczba2) << endl;
//      cout << "przesuniecie bitowe rozne " <<(tak |= liczba2) << endl;
//      cout << "przesuniecie bitowe albo " <<(tak ^= liczba2) << endl;
//      cout << "nadpisywanie  " << (tak = liczba2) << endl;

  cout << (a = b)       << endl;
  cout << (a += b)      << endl;
  cout << (a -= b)      << endl;

  cout << (a /= b)      << endl;
  cout << (a %= b)      << endl;
  cout << (a &= b)      << endl;
  cout << (a |= b)      << endl;
  cout << (a ^= b)      << endl;
  cout << (a <<= b)     << endl;
  cout << (a >>= b)     << endl;
  cout << ++a           << endl;
  cout << --a           << endl;
  cout << a++           << endl;
  cout << a--           << endl;
  cout << +a            << endl;
  cout << -a            << endl;
  cout << a + b         << endl;
  cout << a - b         << endl;
  cout << a * b         << endl;
  cout << a / b         << endl;
  cout << a % b         << endl;
  cout << ~a            << endl;
  cout << (a & b)       << endl;
  cout << (a | b)       << endl;
  cout << (a ^ b)       << endl;
  cout << a << b        << endl;
  cout << (a >> b)      << endl;
  cout << !a            << endl;
  cout << (a && b)      << endl;
  cout << (a || b)      << endl;
  cout << (a == b)      << endl;
  cout << (a != b)      << endl;
  cout << (a < b)       << endl;
  cout << (a > b)       << endl;
  cout << (a <= b)      << endl;
  cout << (a >= b)      << endl;

  return 0;
}
