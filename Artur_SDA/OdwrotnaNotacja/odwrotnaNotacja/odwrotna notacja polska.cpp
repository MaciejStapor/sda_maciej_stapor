#include <iostream>
#include <string>



const std::string RPN[] =
	{ "3 2 + 4 -", "1 1 1 1 + + +", "5 1 2 + 4 * + 3 -",
		"1 1 + 2 + 3 1 1 + 7 - 9 / * -" };
const int wyniki[] =
	{ 1, 4, 14, 4 };

int wylicz(const std::string wyrazenie)
    {
    int wierzcholek = 0;
    int dane[20];



    for (unsigned i=0; i<wyrazenie.size(); i++)
	{
	if (wyrazenie[i] == ' ')
	    {
	    continue;
	    }
	else if (wyrazenie[i] == '+')
	    {
	    dane[wierzcholek-2]=dane[wierzcholek-2]+dane[wierzcholek-1];
	    wierzcholek--;
	    }
	else if (wyrazenie[i] == '-')
	    {
	    dane[wierzcholek-2]=dane[wierzcholek-2]-dane[wierzcholek-1];
	    	    wierzcholek--;
	    }
	else if (wyrazenie[i] == '*')
	    {
	    dane[wierzcholek-2]=dane[wierzcholek-2]*dane[wierzcholek-1];
	    	    wierzcholek--;
	    }
	else if (wyrazenie[i] == '/')
	    {
	    dane[wierzcholek-2]=dane[wierzcholek-2]/dane[wierzcholek-1];
	    	    wierzcholek--;
	    }
	else
	    {
	    dane[wierzcholek]=wyrazenie[i] - '0';
	    wierzcholek++;
	    }
	}
    return dane[0];
    }

void sprawdz(const std::string rpn, int oczekiwanyWynik)
    {
    int wynik = wylicz(rpn);
    if (wynik == oczekiwanyWynik)
	{
	std::cout << "Cacy ";
	}
    else
	{
	std::cout << "Be   ";
	}
    std::cout << rpn << " => " << wynik << std::endl;
    }

int main(int argc, char* argv[])
    {
    for (int i = 0; i < 4; ++i)
	{
	sprawdz(RPN[i], wyniki[i]);
	}

    return 0;
    }
