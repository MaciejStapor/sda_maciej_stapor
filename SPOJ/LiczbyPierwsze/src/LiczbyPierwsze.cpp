#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	int l=0;
	int n=0;
	cin>>n;
	int licznik=0;

	for(int i=0; i<n; i++)
	{
		cin>>l;
		for(int k=1; k<=sqrt(l); k++)
		{
			if(l==1)
			{
				licznik+=1;
			}
			else if( l%k == 0 && k!=1 )
			{
				licznik+=1;
			}
		}

		if(!licznik)
		{
			cout<<"TAK"<<endl;
		}
		else
		{
			cout<<"NIE"<<endl;
		}
		licznik=0;
	}
	return 0;
}
