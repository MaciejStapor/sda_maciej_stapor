#include <iostream>

using namespace std;

int main()
{
	string tabLitery;
	cin>>tabLitery;
	int suma=0;

	for(unsigned int i=0; i<tabLitery.size(); i++)
	{
		if(tabLitery[i] == 'a' ){suma+=1;}
		else if(tabLitery[i] == 'b' ){suma+=2;}
		else if(tabLitery[i] == 'c' ){suma+=3;}
		else if(tabLitery[i] == 'd' ){suma+=4;}
		else if(tabLitery[i] == 'e' ){suma+=5;}
		else if(tabLitery[i] == 'f' ){suma+=6;}
		else if(tabLitery[i] == 'g' ){suma+=7;}
		else if(tabLitery[i] == 'h' ){suma+=8;}
		else if(tabLitery[i] == 'i'){suma+=9;}
		else if(tabLitery[i] == 'k' ){suma+=10;}
		else if(tabLitery[i] == 'l' ){suma+=20;}
		else if(tabLitery[i] == 'm' ){suma+=30;}
		else if(tabLitery[i] == 'n' ){suma+=40;}
		else if(tabLitery[i] == 'o' ){suma+=50;}
		else if(tabLitery[i] == 'p' ){suma+=60;}
		else if(tabLitery[i] == 'q' ){suma+=70;}
		else if(tabLitery[i] == 'r' ){suma+=80;}
		else if(tabLitery[i] == 's' ){suma+=90;}
		else if(tabLitery[i] == 't' ){suma+=100;}
		else if(tabLitery[i] == 'v' ){suma+=200;}
		else if(tabLitery[i] == 'x' ){suma+=300;}
		else if(tabLitery[i] == 'y'){suma+=400;}
		else if(tabLitery[i] == 'z' ){suma+=500;}
	}

	cout<<suma<<endl;

	return 0;
}
