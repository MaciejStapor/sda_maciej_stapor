#include <iostream>
#include <cmath>

using namespace std;



int main()
{

  int liczbaWierszy=1;
  cin>>liczbaWierszy;
  cin.ignore();
  string* tabStr=new string[liczbaWierszy];

  for(int i=0; i<liczbaWierszy; i++)
    {
      getline(cin, tabStr[i]);
     }

  char duze[26];
  char male[26];
  int pomocnicza=0;

  for(int i=65; i<=90; i++, pomocnicza++)
    {
      duze[pomocnicza]=i;
    }

  pomocnicza=0;

  for(int i=97; i<=122; i++, pomocnicza++)
    {
      male[pomocnicza]=i;
    }

  int licznikDuze[26];
  int licznikMale[26];


  for(int i=0; i<26; i++)
    {
      licznikDuze[i]=0;
      licznikMale[i]=0;
    }






  for(int p=0; p<liczbaWierszy; p++)
    {
      for(unsigned int i=0; i<tabStr[p].size(); i++)
	{
	  for(int k=0; k<26; k++)
	    {
	      if(tabStr[p][i]==duze[k])
		{
		  licznikDuze[k]+=1;
		}
	      else if(tabStr[p][i]==male[k])
		{
		  licznikMale[k]+=1;
		}
	    }
	}
    }


  for(int i=0; i<26; i++)
    {
      if(licznikMale[i]>=1) cout<<male[i]<<" "<<licznikMale[i]<<endl;
    }

  for(int i=0; i<26; i++)
    {
      if(licznikDuze[i]>=1) cout<<duze[i]<<" "<<licznikDuze[i]<<endl;
    }



  delete[] tabStr;

  return 0;
}
