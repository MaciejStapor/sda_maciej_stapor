/*
 * zad.hpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#ifndef ZAD_HPP_
#define ZAD_HPP_

#include <iostream>
#include <fstream>
using namespace std;

class Plik
{
	fstream plik;
	int mIndeks;
	int mflagi;

public:

	Plik(string nazwa);
	Plik(string nazwa, int indeks);
	Plik(string nazwa, int indeks, int flagi);
	void wyborZapisz(int wybor);
	void suma(int suma, int jeden, int dwa);
	~Plik();

	void zapisz(string text);
	void otworz();


};




#endif /* ZAD_HPP_ */
