/*
 * zad.cpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */
#include <iostream>
#include <fstream>
#include <ctime>
#include <iomanip>
#include "zad.hpp"

using namespace std;


Plik::Plik(string nazwa): plik(nazwa.c_str(), ofstream::out), mIndeks(3), mflagi(3)
{
	//	plik.open("C:\\Users\\RENT\\Desktop\\Eclipse Cpp\\eclipse-workspace\\ide\\SzymonRAII\\RAII\\zadania.txt", ios::in);

	time_t t;
	t=time(NULL);

	struct tm* current = localtime(&t);

	plik<<"Data: "<<current->tm_year +1900<<"."
			<< current->tm_mon+1<<"."
			<< current->tm_mday<< "  Godzina: "
			<< current->tm_hour<<":"
			<< current->tm_min<<":"
			<< current->tm_sec
			<<endl;
}

Plik::Plik(string nazwa, int indeks): mIndeks(indeks), mflagi(3)
{
	if(indeks==1)
	{
		cerr<<"Za niski indeks "<<endl;
		cerr<<"Plik: "<< nazwa<< " nie utworzyl sie "<<endl;
	}
	else
	{
		cout<<"Plik: "<< nazwa<< " udalo sie utworzyc"<<endl;
		plik.open(nazwa.c_str(), ofstream::out);

		time_t t;
		t=time(NULL);

		struct tm* current = localtime(&t);

		plik<<"Data: "<<current->tm_year +1900<<"."
				<< current->tm_mon+1<<"."
				<< current->tm_mday<< "  Godzina: "
				<< current->tm_hour<<":"
				<< current->tm_min<<":"
				<< current->tm_sec
				<<endl;
	}
}

Plik::Plik(string nazwa, int indeks, int flaga): plik(nazwa.c_str(), ofstream::out), mIndeks(indeks), mflagi(flaga)
{
	if(indeks==1)
	{
		cerr<<"Za niski indeks "<<endl;
		cerr<<"Plik: "<< nazwa<< " nie utworzyl sie "<<endl;
	}
	else if(flaga == 2 || flaga == 3)
	{
		cout<<"Plik: "<< nazwa<< " udalo sie utworzyc"<<endl;
		plik.open(nazwa.c_str(), ofstream::out);

		time_t t;
		t=time(NULL);

		struct tm* current = localtime(&t);

		plik<<"Data: "<<current->tm_year +1900<<"."
				<< current->tm_mon+1<<"."
				<< current->tm_mday<< "  Godzina: "
				<< current->tm_hour<<":"
				<< current->tm_min<<":"
				<< current->tm_sec
				<<endl;
	}
}




void Plik::wyborZapisz(int wybor)
{
	switch(wybor)
	{
	case 1:

		plik<< "wybrano opcje Suma: "<<endl;
		break;
	case 2:
		plik<< "wybrano opcje Roznica: "<<endl;
		break;
	case 3:
		plik<< "wybrano opcje Iloczyn: "<<endl;
		break;
	case 4:
		plik<< "wybrano opcje Wyjscie... "<<endl;
		break;
	default:
		plik<<"Erorr"<<endl;
	}

}

void Plik::suma(int suma, int jeden, int dwa)
{
	plik<<"Wybrano liczby: "<<jeden<<" i "<<dwa<<" wynikiem dzialania jest "<<suma<<endl;
}


void Plik::zapisz(string text)
{
	plik<<text<<endl;
}

Plik::~Plik()
{
	plik<<"koniec   kalkulator";
	plik.close();
}

void Plik::otworz()
{

}
