/*
 * gra.hpp
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef GRA_HPP_
#define GRA_HPP_

//////////////////////////////////////////////////////////////////Plansza

class Plansza
{
	int mSzerokoscX;
	int mWysokoscY;

public:

	Plansza(int wysokoscX=10, int szerokoscY=10);
};

//////////////////////////////////////////////////////////////////Postac

class Postac
{
protected:

	int mZycie;
	int mAtak;
	int mPozycjaX, mPozycjaY;
	char mSymbol;
	int mSzybkosc;

public:
	Postac(int zycie=0, int atak=0,	int pozycjaX=0, int pozycjaY=0, char symbol=0, int szybkosc=0);

	bool atakFunkcja(int x, int y);
	int ruch(int x, int y);

	void setAtak(int atak);
	int getPozycjaX() const;
	int getPozycjaY() const;
	void setSymbol(char symbol);
	void setSzybkosc(int szybkosc);
	void setZycie(int zycie);

};

//////////////////////////////////////////////////////////////////Strzelec

class Strzelec: public Postac
{

public:
	Strzelec(int zycie=3, int atak=1, char symbol='S', int szybkosc=1);

	bool atakFunkcja(int x, int y);
	int ruch(int x, int y);

};

//////////////////////////////////////////////////////////////////Wojownik

class Wojownik: public Postac
{

public:
	Wojownik(int zycie=3, int atak=1, char symbol='W', int szybkosc=1);

	bool atakFunkcja(int x, int y);
	int ruch(int x, int y);
};

//////////////////////////////////////////////////////////////////Kawaleria

class Kawaleria: public Postac
{

public:
	Kawaleria(int zycie=3, int atak=1, char symbol='K', int szybkosc=1);

	bool atakFunkcja(int x, int y);
	int ruch(int x, int y);
};

#endif /* GRA_HPP_ */
