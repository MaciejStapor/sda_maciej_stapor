/*
 * gra.cpp

 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */
#include <iostream>
#include "gra.hpp"
using namespace std;

//////////////////////////////////////////////////////////////////Postac

Postac::Postac(int zycie, int atak,	int pozycjaX, int pozycjaY, char symbol, int szybkosc): mZycie(zycie), mAtak(atak), mPozycjaX(pozycjaX), mPozycjaY(pozycjaY), mSymbol (symbol), mSzybkosc(szybkosc) {}


//////////////Funkcje

bool Postac::atakFunkcja(int x, int y)
{
	cout<<"Postac"<<endl;

	return true;
}

int Postac::ruch(int x, int y)
{
	return 0;
}

void Postac::setAtak(int atak)
{
	mAtak = atak;
}

int Postac::getPozycjaX() const
{
	return mPozycjaX;
}

int Postac::getPozycjaY() const
{
	return mPozycjaY;
}

void Postac::setSymbol(char symbol)
{
	mSymbol = symbol;
}

void Postac::setSzybkosc(int szybkosc)
{
	mSzybkosc = szybkosc;
}

void Postac::setZycie(int zycie)
{
	mZycie = zycie;
}

//////////////////////////////////////////////////////////////////Strzelec

Strzelec::Strzelec(int zycie, int atak, char symbol, int szybkosc):Postac(zycie, atak, 0, 0, symbol, szybkosc)
{
//	Postac::mZycie=zycie;
//	Postac::mAtak=atak;
//	Postac::mSymbol=symbol;
//	Postac::mSzybkosc=szybkosc;
}

//////////////Funkcje

bool Strzelec::atakFunkcja(int x, int y)
{
	cout<<"Strzelec:  puf..."<<endl;

	return true;
}

int Strzelec::ruch(int x, int y)
{
	return 0;
}

//////////////////////////////////////////////////////////////////Wojownik

Wojownik::Wojownik(int zycie, int atak, char symbol, int szybkosc):Postac(zycie, atak, 0, 0, symbol, szybkosc)
{
//	Postac::mZycie=zycie;
//	Postac::mAtak=atak;
//	Postac::mSymbol=symbol;
//	Postac::mSzybkosc=szybkosc;
}

//////////////Funkcje

bool Wojownik::atakFunkcja(int x, int y)
{
	cout<<"Wojownik:  ciach..."<<endl;

	return true;
}

int Wojownik::ruch(int x, int y)
{
	return 0;
}

//////////////////////////////////////////////////////////////////Kawaleria

Kawaleria::Kawaleria(int zycie, int atak, char symbol, int szybkosc):Postac(zycie, atak, 0, 0, symbol, szybkosc)
{
//	Postac::mZycie=zycie;
//	Postac::mAtak=atak;
//	Postac::mSymbol=symbol;
//	Postac::mSzybkosc=szybkosc;
}

//////////////Funkcje

bool Kawaleria::atakFunkcja(int x, int y)
{
	cout<<"Kawaleria:  ihaha...."<<endl;

	return true;
}

int Kawaleria::ruch(int x, int y)
{
	return 0;
}
