/*
 * zad.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef ZAD_HPP_
#define ZAD_HPP_
#include <string>
class NaASCI
{

	int mSuma;
	std::string napis;
public:
	NaASCI();
	int getSuma() const;
	void setSuma(int suma);
	const std::string& getNapis() const;
	void setNapis(const std::string& napis);
	int suma();
};




#endif /* ZAD_HPP_ */
