/*
 * main.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */


#include "zad.hpp"
#include "gtest/gtest.h"


TEST(Konstruktor,naASCI)
{
	NaASCI a;
	EXPECT_EQ(0, a.getSuma());
	EXPECT_EQ("", a.getNapis());
}

TEST(NaASCI, suma)
{
	NaASCI a;
	a.setNapis("ab");
	EXPECT_EQ(195, a.suma());
	a.setSuma(0);
	a.setNapis("b");
	EXPECT_EQ(98, a.suma());
}

TEST(NaASCI, geterySetery)
{
	NaASCI a;
	a.setNapis("a");
	EXPECT_EQ("a", a.getNapis());
	a.setSuma(2);
	EXPECT_EQ(2, a.getSuma());
}


int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
