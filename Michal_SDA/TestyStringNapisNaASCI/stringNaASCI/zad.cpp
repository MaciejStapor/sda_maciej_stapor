/*
 * zad.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "zad.hpp"

NaASCI::NaASCI(): mSuma(0), napis("")
{

}

int NaASCI::getSuma() const {
	return mSuma;
}

void NaASCI::setSuma(int suma) {
	mSuma = suma;
}

const std::string& NaASCI::getNapis() const {
	return napis;
}

void NaASCI::setNapis(const std::string& napis) {
	this->napis = napis;
}


int NaASCI::suma()
{
	for(unsigned int i=0; i<napis.size(); i++)
	{
		mSuma+=napis[i];
	}

	return mSuma;
}
