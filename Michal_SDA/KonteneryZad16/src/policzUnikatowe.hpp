/*
 * policzUnikatowe.hpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#ifndef POLICZUNIKATOWE_HPP_
#define POLICZUNIKATOWE_HPP_
#include <iostream>
#include <map>
#include <algorithm>
using namespace std;

bool usunSpacje(char& first)
{
	return first==' ';
}

class PoliczUnikatowe
{
	map<string, map<string, int> > mMapaStr;

public:

	void policzUnikatowe(string napis, map<string, int>& nowa)
	{
		map<string, map<string, int> >::iterator it = mMapaStr.find(napis);

		if(it != mMapaStr.end())
		{
			nowa = it->second;
		}
		else
		{
			string tmp = napis;

			string::iterator it = remove_if(napis.begin(), napis.end(), usunSpacje);
			napis.resize(distance(napis.begin(), it));

			sort(napis.begin(), napis.end());
			it = unique(napis.begin(), napis.end());
			napis.resize(distance(napis.begin(), it));

			map<string, int> tmpMap;
			tmpMap.insert(make_pair(napis, napis.size()));

			mMapaStr.insert(make_pair(tmp, tmpMap));
			map<string,  map<string, int> >::iterator its = --(mMapaStr.begin());
			nowa = its->second;
		}
	}

};




#endif /* POLICZUNIKATOWE_HPP_ */
