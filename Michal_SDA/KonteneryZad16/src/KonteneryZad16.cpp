//============================================================================
// Name        : KonteneryZad16.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "policzUnikatowe.hpp"
using namespace std;

void pokaz(map<string, int>& nowa, string napis)
{
	for(map<string, int>::iterator it = nowa.begin(); it != nowa.end(); it++)
	{
		cout<<"'"<<napis<<"'"<<" posiada "<<it->second<<" unikatowych litery: "<<it->first<<endl;
	}
}

int main() {

	PoliczUnikatowe stringMap;

	map<string, int> mapaTmp;
	string napis = "ala ma kota a kot ma ale";

	stringMap.policzUnikatowe(napis, mapaTmp);
	pokaz(mapaTmp, napis);


	return 0;
}
