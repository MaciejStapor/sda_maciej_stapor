/*
 * cup.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef CUP_HPP_
#define CUP_HPP_
#include "liquid.hpp"
#include <cstdlib>

class Cup
{
protected:
	Liquid** mContent;
	size_t mSize;
	void resize();
	void clear();
	float amountOfLiquid();



public:

	Cup();
	virtual ~Cup();
	void add(Liquid*);
	void takeSip(float);
	void spill();
	virtual void show();
	 Liquid** getContent() ;
	size_t getSize() const;
};




#endif /* CUP_HPP_ */
