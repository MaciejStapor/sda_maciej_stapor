/*
 * castomer.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef CUSTOMER_HPP_
#define CUSTOMER_HPP_
#include "cup.hpp"
#include <iostream>
#include <cstring>

class Customer
{
	std::string mName;
	Cup* mCup;

public:

	Customer(std::string, Cup*);
	void show();
	~Customer();
};



#endif /* CUSTOMER_HPP_ */
