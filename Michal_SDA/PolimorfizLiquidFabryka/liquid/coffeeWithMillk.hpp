/*
 * coffeeWithMillk.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef COFFEEWITHMILLK_HPP_
#define COFFEEWITHMILLK_HPP_

#include "cup.hpp"
#include <cstdlib>

class CoffeeWithMillk: virtual public Cup
{
	Liquid** mContent;
	size_t mSize;

public:
	CoffeeWithMillk();
	void show();
};




#endif /* COFFEEWITHMILLK_HPP_ */
