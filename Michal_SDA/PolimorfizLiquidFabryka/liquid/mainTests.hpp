/*
 * mainTests.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#include "gtest/gtest.h"
#include "millk.hpp"
#include "liquid.hpp"
#include "coffee.hpp"

TEST(Millk, get)
{
	Millk a(2.5, 200);
	EXPECT_EQ(2.5, a.getFat());
}

TEST(Coffee, get)
{
	Coffee a(3.0, 100);
	EXPECT_EQ(3.0, a.getCaffeine());
}

int main(int argc, char** argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
