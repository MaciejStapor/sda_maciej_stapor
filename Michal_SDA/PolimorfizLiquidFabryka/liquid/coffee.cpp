/*
 * coffee.cpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */
#include "coffee.hpp"
#include <iostream>

Coffee::Coffee(): mCaffeine(0)
{

}
Coffee::Coffee(float caffeine, float amount): mCaffeine(caffeine)
{
	add(amount);
}
Coffee::~Coffee()
{

}
void Coffee::remove(float amount)
{
	mAmount-=amount;
}
void Coffee::removeAll()
{
	mAmount=0;
}
void Coffee::add(float amount)
{
	mAmount+=amount;
}

float Coffee::getCaffeine() const {
	return mCaffeine;
}

void Coffee::show()
{

	std::cout<<"Amount of Coffee "<<mAmount<<" ml."<<" caffeine "<<mCaffeine<<"%"<<std::endl;
}



