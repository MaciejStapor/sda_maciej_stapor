/*
 * coffeeWithRum.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef COFFEEWITHRUM_HPP_
#define COFFEEWITHRUM_HPP_

#include "cup.hpp"
#include <cstdlib>

class CoffeeWithRum: virtual public Cup
{
	Liquid** mContent;
	size_t mSize;

public:
	CoffeeWithRum();
	void show();
};



#endif /* COFFEEWITHRUM_HPP_ */
