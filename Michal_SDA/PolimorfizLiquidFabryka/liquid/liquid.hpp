/*
 * liquid.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef LIQUID_HPP_
#define LIQUID_HPP_

class Liquid
{
protected:
	float mAmount;
public:
	Liquid(): mAmount(0){};
	virtual ~Liquid(){};
	virtual void remove(float)=0;
	virtual void removeAll()=0;
	virtual void add(float)=0;
	virtual void show()=0;
	float getAmount() const {
		return mAmount;
	}
};



#endif /* LIQUID_HPP_ */
