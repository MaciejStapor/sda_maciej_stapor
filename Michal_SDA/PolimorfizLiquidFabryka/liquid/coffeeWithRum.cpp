/*
 * coffeeWithRum.cpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */
#include "coffeeWithRum.hpp"
#include "coffee.hpp"
#include "millk.hpp"
#include "rum.hpp"
#include <iostream>

CoffeeWithRum::CoffeeWithRum():mContent(0), mSize(0)
{
	Cup coffeeRum;

	coffeeRum.add(new Millk(3.5, 50));
	coffeeRum.add(new Coffee(2.6, 200));
	coffeeRum.add(new Rum(20, Rum::Dark));

	this->mContent=coffeeRum.getContent();
	this->mSize=coffeeRum.getSize();
}
void CoffeeWithRum::show()
{
	std::cout<<"Cup of Coffee with Rum: "<<std::endl;
	for(size_t i=0; i<mSize; i++ )
	{
		mContent[i]->show();
	}
}



