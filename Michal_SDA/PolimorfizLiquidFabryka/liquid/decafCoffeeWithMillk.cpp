/*
 * decafCoffeeWithMillk.cpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */
#include "decafCoffeeWithMillk.hpp"
#include "decaf.hpp"
#include "millk.hpp"
#include <iostream>

DecafCoffeeWithMillk::DecafCoffeeWithMillk():mContent(0), mSize(0)
{
	Cup deCafCup;

	deCafCup.add(new Millk(3.5, 50));
	deCafCup.add(new Decaf(200));

	this->mContent=deCafCup.getContent();
	this->mSize=deCafCup.getSize();
}
void DecafCoffeeWithMillk::show()
{
	std::cout<<"Cup of DecafCoffee: "<<std::endl;
	for(size_t i=0; i<mSize; i++ )
	{
		mContent[i]->show();
	}
}

