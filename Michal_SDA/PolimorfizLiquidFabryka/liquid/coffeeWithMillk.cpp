/*
 * coffeeWithMmillk.cpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#include "coffeeWithMillk.hpp"
#include "coffee.hpp"
#include "millk.hpp"
#include <iostream>

CoffeeWithMillk::CoffeeWithMillk():mContent(0), mSize(0)
{
	Cup coffeeMillk;

	coffeeMillk.add(new Millk(3.5, 50));
	coffeeMillk.add(new Coffee(2.6, 200));

	this->mContent=coffeeMillk.getContent();
	this->mSize=coffeeMillk.getSize();
}
void CoffeeWithMillk::show()
{
	std::cout<<"Cup of Coffee with millk: "<<std::endl;
	for(size_t i=0; i<mSize; i++ )
	{
		mContent[i]->show();
	}
}


