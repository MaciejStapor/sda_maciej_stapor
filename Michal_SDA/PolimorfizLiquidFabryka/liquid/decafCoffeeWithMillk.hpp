/*
 * decafCoffeeWithMillk.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef DECAFCOFFEEWITHMILLK_HPP_
#define DECAFCOFFEEWITHMILLK_HPP_
#include "cup.hpp"
#include <cstdlib>

class DecafCoffeeWithMillk: virtual public Cup
{
	Liquid** mContent;
	size_t mSize;

public:
	DecafCoffeeWithMillk();
	void show();
};




#endif /* DECAFCOFFEEWITHMILLK_HPP_ */
