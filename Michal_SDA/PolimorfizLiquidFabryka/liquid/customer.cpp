/*
 * client.cpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#include "customer.hpp"





Customer::Customer(std::string name, Cup* cup):mName(name), mCup(cup)
{

}
void Customer::show()
{
	std::cout<<"Name: "<<mName<<std::endl;
	mCup->show();
}

Customer::~Customer()
{
	delete mCup;
}
