/*
 * mianNormal.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#include "millk.hpp"
#include "coffee.hpp"
#include "cup.hpp"
#include "rum.hpp"
#include "decaf.hpp"
#include "decafCoffeeWithMillk.hpp"
#include "customer.hpp"
#include "coffeeWithMillk.hpp"
#include "coffeeWithRum.hpp"

int main()
{


Customer customer1("Chris", new DecafCoffeeWithMillk);
Customer customer2("Tom", new CoffeeWithMillk);
Customer customer3("Jane", new CoffeeWithRum);

customer1.show();
customer2.show();
customer3.show();

	return 0;
}

