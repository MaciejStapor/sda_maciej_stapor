/*
 * karty.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef KARTA_HPP_
#define KARTA_HPP_

class Karta
{


public:

	enum Figury
	{
		As=0,
		Krol,
		Dama,
		Jopek,
		dziesiec,
		dziewiec,
		osiem,
		siedem,
		szesc,
		piec,
		cztery,
		trzy,
		dwa,
		FiguryEnd
	};

	enum Kolory
	{
		Kier=0,
		Karo,
		Pik,
		Trefl,
		KoloryEnd

	};
	Karta(Kolory w,Figury f, int wartosc);
	Karta();
	Figury getFigura() const;
	void setFigura(Figury figura);
	Kolory getKolor() const;
	void setKolor(Kolory kolor);
	int getWartosc() const;
	void setWartosc(int wartosc);
	void stworzKarte(Karta::Kolory w,Karta::Figury f);

private:
	int mWartosc;
	Figury mFigura;
	Kolory mKolor;

	int policzWartosc(Figury a);

};



#endif /* KARTA_HPP_ */
