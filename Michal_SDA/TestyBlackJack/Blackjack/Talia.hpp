/*
 * Talia.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef TALIA_HPP_
#define TALIA_HPP_
#include "karta.hpp"

class Talia
{

	Karta *mKarty;
	int mLicznikKart;
	int poczatek;

public:
	Talia();
	~Talia();
	void tasuj();
	Karta pobierzKarte();
};



#endif /* TALIA_HPP_ */
