/*
 * Talia.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */
#include "Talia.hpp"
#include "karta.hpp"
#include <ctime>
#include <cstdlib>



Talia::Talia():mKarty(0), mLicznikKart(0), poczatek(-1)
{
	srand(time(NULL));

	mLicznikKart=Karta::FiguryEnd*Karta::KoloryEnd;

	mKarty=new Karta[mLicznikKart];


	for(int w=0; w<Karta::KoloryEnd; w++)
	{
		for(int k=0; k<Karta::FiguryEnd; k++)
		{
			mKarty[w*Karta::FiguryEnd+k].stworzKarte((Karta::Kolory)w, (Karta::Figury)k);
		}
	}
}


Talia::~Talia()
{
	delete[] mKarty;
}

void Talia::tasuj()
{
	for(int i=0; i<mLicznikKart; i++)
	{
		int pos = rand()%mLicznikKart;

		Karta tym=mKarty[i];

		mKarty[i]=mKarty[pos];

		mKarty[pos]=tym;
	}
}

Karta Talia::pobierzKarte()
{
	if(poczatek<mLicznikKart){poczatek++;}
	else
	{
		Karta kar;
		return kar;
	}
	return mKarty[poczatek];
}
