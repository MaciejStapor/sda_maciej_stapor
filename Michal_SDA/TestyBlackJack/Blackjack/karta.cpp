/*
 * karty.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "karta.hpp"

Karta::Figury Karta::getFigura() const {
	return mFigura;
}

void Karta::setFigura(Figury figura) {
	mFigura = figura;
}

Karta::Kolory Karta::getKolor() const {
	return mKolor;
}

void Karta::setKolor(Kolory kolor) {
	mKolor = kolor;
}

int Karta::getWartosc() const {
	return mWartosc;
}

void Karta::setWartosc(int wartosc) {
	mWartosc = wartosc;
}

void Karta::stworzKarte(Karta::Kolory w,Karta::Figury f)
{
	mKolor=w;
	mFigura=f;

	mWartosc=policzWartosc(f);
}

Karta::Karta():mKolor(KoloryEnd), mFigura(FiguryEnd), mWartosc(0)
{

}


int Karta::policzWartosc(Figury a)
{
	int wartosc;
	switch(a)
	{
	case As:
	{
		return wartosc=11;
		break;
	}
	case Krol:
	case Dama:
	case Jopek:
	case dziesiec:
	{
		wartosc=10;
		break;
	}
	case dziewiec:
	{
		wartosc=9;
		break;
	}
	case osiem:
	{
		wartosc=8;
		break;
	}
	case siedem:
	{
		wartosc=7;
		break;
	}
	case szesc:
	{
		wartosc=6;
		break;
	}
	case piec:
	{
		wartosc=5;
		break;
	}
	case cztery:
	{
		wartosc=4;
		break;
	}
	case trzy:
	{
		wartosc=3;
		break;
	}
	case dwa:
	{
		wartosc=2;
		break;
	}
	default:
	{
		wartosc=0;
		break;
	}
	}

	return wartosc;
}

