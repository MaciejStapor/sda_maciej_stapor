/*
 * gracz.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef GRACZ_HPP_
#define GRACZ_HPP_
#include <string>
#include "reka.hpp"

class Gracze
{

	std::string mImie;
	enum Stan
	{
		aktywny,
		czeka,
		przekroczony
	};

	Reka reka;

	void dobierz();
	void pas();

};



#endif /* GRACZ_HPP_ */
