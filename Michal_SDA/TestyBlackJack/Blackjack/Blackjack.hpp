/*
 * Blackjack.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef BLACKJACK_HPP_
#define BLACKJACK_HPP_
#include <string>
#include "gracz.hpp"
#include "karta.hpp"
#include  "Talia.hpp"


class BlackJack
{
int mIloscGraczy;
int mObecnyGracz;
Gracze *gracze;
Karta karta;
Talia talia;

public:

void sprawdzStanGry();
void podajZwyciezce();

};

#endif /* BLACKJACK_HPP_ */
