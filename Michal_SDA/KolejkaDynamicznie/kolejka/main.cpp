/*
 * main.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */
#include <iostream>

#include "kolejka.hpp"

using namespace std;

int main()
{
	Kolejka kolejka;

	kolejka.pokaz();

	kolejka.dodawanie(23);
	kolejka.dodawanie(2);
	kolejka.dodawanie(18);

	kolejka.pokaz();

	kolejka.usun();
	kolejka.pokaz();


	return 0;
}

