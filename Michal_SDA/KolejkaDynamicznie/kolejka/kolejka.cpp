/*
 * kolejkaP.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "kolejka.hpp"

#include <iostream>

using namespace std;

Kolejka::Kolejka()
{
	poczatek=0;
	koniec=0;
}


void Kolejka::dodawanie(int wartosc)
{
	if(poczatek!=0)
	{
		Wezel *nowy=new Wezel(wartosc);
		koniec->nastepny=nowy;
		koniec=nowy;
	}
	else
	{
		Wezel *nowy=new Wezel(wartosc);
		koniec=nowy;
		poczatek=nowy;
	}

}

void Kolejka::usun()
{
	if(poczatek!=0)
		{
			Wezel *nowy=poczatek;
			poczatek=poczatek->nastepny;
			delete nowy;
		}
		else
		{
			cerr<<"kolejka pusta!!"<<endl;
		}
}

void Kolejka::pokaz()
{
	Wezel *nowy;
	nowy=poczatek;
	cout<<"{ ";
	while(nowy!=0)
	{
		cout<<"["<<nowy->mDane<<"]";
		nowy=nowy->nastepny;
	}
	cout<<" }"<<endl;


}
