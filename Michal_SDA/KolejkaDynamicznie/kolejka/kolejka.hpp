/*
 * kolejkaP.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef KOLEJKAP_CPP_
#define KOLEJKAP_CPP_

class Kolejka
{
	class Wezel
	{
	public:
		int mDane;
		Wezel *nastepny;

		Wezel(int wartosc)
		{
			mDane=wartosc;
			nastepny=0;
		}

	};

	Wezel *poczatek;
	Wezel *koniec;
public:
	Kolejka();

	void dodawanie(int wartosc);
	void usun();
	void pokaz();

};




#endif /* KOLEJKAP_CPP_ */
