//============================================================================
// Name        : KonteneryZad3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

ostream& operator<<(ostream& os, vector<char> nowy)
{
	os<<"[ ";
	for(vector<char>::iterator it = nowy.begin(); it != nowy.end(); it++ )
	{
		os<<*it<<", ";
	}
	os<<"]"<<endl;

	return os;
}

vector<char> vectorWspolnychLiter(string jeden, string dwa)
{
	vector<char> pierwszy;
	vector<char> drugi;

	for(unsigned int i = 0; i < jeden.size(); i++)
		{
		pierwszy.push_back(jeden[i]);
		}
	for(unsigned int i = 0; i < dwa.size(); i++)
		{
			drugi.push_back(dwa[i]);
		}
	sort(pierwszy.begin(), pierwszy.end());
	vector<char>::iterator it = unique(pierwszy.begin(), pierwszy.end());
	pierwszy.resize(distance(pierwszy.begin(), it));
//	cout<<pierwszy;

	sort(drugi.begin(), drugi.end());
	it = unique(drugi.begin(), drugi.end());
	drugi.resize(distance(drugi.begin(), it));
//	cout<<drugi;

	vector<char> wynikowy;
	for(vector<char>::iterator it = pierwszy.begin(); it != pierwszy.end(); it++)
	{
		for(vector<char>::iterator itk = drugi.begin(); itk != drugi.end(); itk++)
		{
			if(*it == *itk && *it != ' ')
			{
			wynikowy.push_back(*it);
			}
		}
	}
	return wynikowy;
}

int main() {

	string jeden = "ala ma kota";
	string dwa = "pawel ma psa";

	cout<<"Wektor wspolnych liter: ";
	vector<char> wynikowy = vectorWspolnychLiter(jeden, dwa);
	cout<<wynikowy;
	return 0;
}
