/*
 * cezarTekst.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include <iostream>
#include "cezarTekst.hpp"

using namespace std;
CezarTekst::CezarTekst()
{
	cout<<"CezarTekst"<<endl;
}

CezarTekst::CezarTekst(string lancuch): SzyfrowanyTekst(lancuch)
{
	cout<<"CezarTekst"<<endl;
}
CezarTekst::~CezarTekst()
{
	cout<<"~CezarTekst"<<endl;
}

void CezarTekst::szyfruj()
{
	if(mCzyZaszyfrowany==false)
	{

		for(unsigned int i = 0; i < (mLancuch.length()); ++i)
		{
			if((mLancuch[i])>=97 && (mLancuch[i])<=122-3)
			{
				mLancuch[i]=int(mLancuch[i])+3;
			}

			else if (mLancuch[i]>=123-3 && mLancuch[i]<=122)
			{

				mLancuch[i]=int(mLancuch[i])-26+3;

			}
		}

		mCzyZaszyfrowany=true;
	}
}

void CezarTekst::wypisz()
{
	if(mCzyZaszyfrowany==true)
	{
		cout<<"Cezar tekst: "<<mLancuch<<endl;
	}
}
