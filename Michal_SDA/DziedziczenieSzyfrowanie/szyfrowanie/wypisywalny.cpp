/*
 * wypisywalny.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include <iostream>
#include <algorithm>
#include "wypisywalny.hpp"

using namespace std;

string Wypisywalny::getLancuch() const
{
	return mLancuch;
}

void Wypisywalny::setLancuch(string lancuch)
{
	transform(lancuch.begin(), lancuch.end(), lancuch.begin(), ::tolower);
	this->mLancuch = lancuch;
}

Wypisywalny::Wypisywalny(): mLancuch(" ")
{

	cout<<"Tworz�: "<<mLancuch<<endl;
}

Wypisywalny::Wypisywalny(string lancuch)
{
	setLancuch(lancuch);
	cout<<"Tworz�: "<<mLancuch<<endl;
}

void Wypisywalny::wypisz()
{
	cout << "Tekst:   "<<mLancuch<<endl;
}

Wypisywalny::~Wypisywalny()
{
	cout<<"Niszcz�: "<<mLancuch<<endl;
}
