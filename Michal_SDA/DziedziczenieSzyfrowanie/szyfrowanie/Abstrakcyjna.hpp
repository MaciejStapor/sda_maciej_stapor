/*
 * Abstrakcyjna.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */
#include <iostream>
#include "wypisywalny.hpp"

using namespace std;

#ifndef ABSTRAKCYJNA_HPP_
#define ABSTRAKCYJNA_HPP_

class SzyfrowanyTekst: public Wypisywalny
{
protected:
	bool mCzyZaszyfrowany;

public:
	SzyfrowanyTekst();
	SzyfrowanyTekst(string lancuch);
	virtual void szyfruj()=0;
	virtual void wypisz()=0;
	virtual ~SzyfrowanyTekst();
	bool isCzyZaszyfrowany() const;
};



#endif /* ABSTRAKCYJNA_HPP_ */
