/*
 * cezarTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include <iostream>
#include "Abstrakcyjna.hpp"

using namespace std;


#ifndef CEZARTEKST_HPP_
#define CEZARTEKST_HPP_

class CezarTekst: public SzyfrowanyTekst
{


public:
	CezarTekst();
	CezarTekst(string lancuch);
	~CezarTekst();
	void szyfruj();
	void wypisz();
};



#endif /* CEZARTEKST_HPP_ */
