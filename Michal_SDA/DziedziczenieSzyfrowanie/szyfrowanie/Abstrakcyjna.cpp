/*
 * Abstrakcyjna.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */


#include <iostream>
#include "Abstrakcyjna.hpp"


using namespace std;


SzyfrowanyTekst::SzyfrowanyTekst():mCzyZaszyfrowany(false)
{
	cout<<"SzyfrowanyTekst"<<endl;
}

SzyfrowanyTekst::SzyfrowanyTekst(string lancuch):Wypisywalny(lancuch), mCzyZaszyfrowany(false)
{
	cout<<"SzyfrowanyTekst"<<endl;
}


bool SzyfrowanyTekst::isCzyZaszyfrowany() const
{
	return mCzyZaszyfrowany;
}

SzyfrowanyTekst::~SzyfrowanyTekst()
{
	cout<<"~SzyfrowanyTekst"<<endl;
}

