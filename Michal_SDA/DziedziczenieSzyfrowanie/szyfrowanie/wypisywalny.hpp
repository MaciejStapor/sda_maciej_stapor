/*
 * wypisywalny.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include <iostream>

using namespace std;

#ifndef WYPISYWALNY_HPP_
#define WYPISYWALNY_HPP_

class Wypisywalny
{
protected:
	string mLancuch;
public:
	string getLancuch() const;
	void setLancuch(string lancuch);
	Wypisywalny();
	Wypisywalny(string lancuch);
	void wypisz();
	~Wypisywalny();

};



#endif /* WYPISYWALNY_HPP_ */
