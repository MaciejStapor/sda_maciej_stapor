/*
 * PodstawieniowyTekst.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */
#include <iostream>
#include "PodstawieniowyTekst.hpp"

using namespace std;


PodstawieniowyTekst::PodstawieniowyTekst()
{
	cout<<"PodstawieniowyTekst"<<endl;
}
PodstawieniowyTekst::PodstawieniowyTekst(string lancuch): CezarTekst(lancuch)
{
	cout<<"PodstawieniowyTekst"<<endl;
}
PodstawieniowyTekst::~PodstawieniowyTekst()
{
	cout<<"~PodstawieniowyTekst"<<endl;
}
void PodstawieniowyTekst::szyfruj()
{
	if(mCzyZaszyfrowany==false)
	{
		char pom;
		unsigned int k = (mLancuch.length()-1);
		for(unsigned int i = 0; i < (mLancuch.length()/2); ++i)
		{

			pom=mLancuch[i];
			mLancuch[i]=mLancuch[k];
			mLancuch[k]=pom;
			--k;


		}
		mCzyZaszyfrowany=true;

	}
}
void PodstawieniowyTekst::wypisz()
{
	cout<<"Podstawieniowy: "<<mLancuch<<endl;
}
