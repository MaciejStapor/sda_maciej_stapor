/*
 * PodstawieniowyTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include <iostream>
#include "cezarTekst.hpp"

using namespace std;

#ifndef PODSTAWIENIOWYTEKST_HPP_
#define PODSTAWIENIOWYTEKST_HPP_
class PodstawieniowyTekst: public CezarTekst
{
public:
	PodstawieniowyTekst();
	PodstawieniowyTekst(string lancuch);
	~PodstawieniowyTekst();
	void szyfruj();
	void wypisz();
};




#endif /* PODSTAWIENIOWYTEKST_HPP_ */
