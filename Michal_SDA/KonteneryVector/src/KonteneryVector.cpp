//============================================================================
// Name        : KonteneryVector.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>

using namespace std;



int main() {

	vector<int> vectInt(5, 8);

	cout<<"operator[] [ ";
	for(unsigned int i=0; i<vectInt.size(); i++)
	{
		cout<<vectInt[i]<<", ";
	}
	cout<<"]"<<endl;

	cout<<"Capacity "<<vectInt.capacity()<<", size "<<vectInt.size()<<", max size "<<vectInt.max_size()<<endl;
	vectInt.reserve(20);
	cout<<"Capacity "<<vectInt.capacity()<<", size "<<vectInt.size()<<", max size "<<vectInt.max_size()<<endl;
	vectInt.resize(30, 12);
	cout<<"Capacity "<<vectInt.capacity()<<", size "<<vectInt.size()<<", max size "<<vectInt.max_size()<<endl;

	cout<<"operator[] [ ";
	for(unsigned int i=0; i<vectInt.size(); i++)
	{
		cout<<vectInt[i]<<", ";
	}
	cout<<"]"<<endl;


	cout<<"pop_back: [ ";
	while(!vectInt.empty())
	{
		cout<<vectInt.back()<<", ";
		vectInt.pop_back();
	}
	cout<<"]"<<endl;

	vector<string> vectStr;
	vectStr.push_back("dan");
	vectStr.push_back("kuku");
	vectStr.push_back("max");
	vectStr.push_back("karo");
	vectStr.push_back("tom");

	cout<<"at():  [ ";
	for(unsigned int i=0; i<vectStr.size(); i++)
	{
		cout<<vectStr.at(i)<<", ";
	}
	cout<<"]"<<endl;





	cout<<"back: "<<vectStr.back()<<endl;
	cout<<"front: "<<vectStr.front()<<endl;

	return 0;
}
