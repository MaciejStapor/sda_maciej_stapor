#ifndef _DRAW_HPP_
#define _DRAW_HPP_

#include "board.hpp"
#include "score.hpp"
#include "gobject.hpp"

void draw(const board *board, const gobject *snake, const gobject *fruit, const score *score);

#endif /* _DRAW_HPP_ */
