#ifndef _GOBJECT_HPP_
#define _GOBJECT_HPP_

#include <cstddef>

struct position {
  int x;
  int y;
};

struct gobject {
  position *pos;
  size_t npos;
  char symbol;
};

/**
 * Sets the gobject members to an initial state allowing for gobject_resize and
 * gobject_destory to successfully operate on a struct gobject.
 */
void gobject_init(gobject *obj);

/**
 * Destroys the gobject by freeing any memory block pointed to by pos that was
 * created via a call to gobject_resize. gobject_destory should only be called
 * after a call to gobject_init.
 */
void gobject_destory(gobject *obj);

/**
 * Changes the size of the memory block pointed to by the struct pos member such
 * that nmemb elements will fit within the block. The contents will be unchanged
 * in the range from the start of the region up to the minimum of the old and
 * new sizes. If the new size is larger than the old size, the added memory will
 * not be initialized. If nmemb is equal to zero, and pos is not NULL, then any
 * allocated memory is freed. Unless ptr is NULL, it must have been returned by
 * an earlier call to gobject_resize.
 */
void gobject_resize(gobject *obj, size_t nmemb);

/**
 * Compares each pos in obj1 with those in obj2 and returns 1 if a match is
 * found. Otherwise, 0 is return.
 */
int gobject_collision(const gobject *obj1, const gobject *obj2);

/**
 * Moves the first pos object to (x_pos, y_pos) and moves all the remaining pos
 * objects relative the first movement.
 */
void gobject_move(gobject *obj, int x_pos, int y_pos);

/**
 * Moves the first pos from its current location relative to x_delta and
 * y_delta. Each of the remain pos elements are updates such that pos N is moves
 * to pos N-1.
 */
void gobject_slither(gobject *obj, int x_delta, int y_delta);

/**
 * Returns 1 if any pos member contained with obj matches that of pos. Otherwise
 * 0 is returned.
 */
int gobject_occupying(const gobject *obj, const position *pos);

#endif /* _GOBJECT_HPP_ */
