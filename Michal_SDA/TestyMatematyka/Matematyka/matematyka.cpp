/*
 * silnia.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */
#include "matematyka.hpp"




int Matematyka::policzSilnia(int wartosc)
{

	if(wartosc==0)
	{
		return 1;
	}
	else if(wartosc==1)
	{
		return 1;
	}

	return wartosc*policzSilnia(wartosc-1);
}

int Matematyka::fibonacci(int wartosc)
{


	if(wartosc==0)
	{
		return 0;
	}
	else if (wartosc == 1)
	{
		return 1;
	}
	else
	{
		return (fibonacci(wartosc-1)+fibonacci(wartosc-2));
	}


	return -1;
}

int Matematyka::nwd(int jeden, int dwa)
{

	while(jeden!=dwa)
	{
		dwa=dwa-jeden;
		jeden=jeden-dwa;
	}

	return jeden;

}

void Kalendarz::setDzien(int dzien) {


	switch (mMiesiac)
	{
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
	{
		if(dzien>=1 && dzien<=31)
		{
			mCzyPoprawan=true;
			mDzien = dzien;
		}
		else
		{
			mCzyPoprawan=false;
			mDzien=-1;
		}
		break;
	}

	case 4:
	case 6:
	case 9:

	{
		if(dzien>=1 && dzien<=30)
		{
			mCzyPoprawan=true;
			mDzien = dzien;
		}
		else
		{
			mCzyPoprawan=false;
			mDzien=-1;
		}
		break;
	}
	case 2:

	{
		if(czyPrzystepny(mRok))
		{
			if(dzien>=1 && dzien<=29)
			{
				mCzyPoprawan=true;
				mDzien = dzien;
			}
			else
			{
				mCzyPoprawan=false;
				mDzien=-1;
			}
		}
		else
		{
			if(dzien>=1 && dzien<=28)
			{
				mCzyPoprawan=true;
				mDzien = dzien;
			}
			else
			{
				mCzyPoprawan=false;
				mDzien=-1;
			}
		}
		break;
	}

	}



}

void Kalendarz::setMiesiac(int miesiac) {
	if(miesiac>=1 && miesiac<=12)
	{
		mCzyPoprawan=true;
		mMiesiac = miesiac;
	}
	else
	{
		mCzyPoprawan=false;
		mMiesiac=-1;
	}

}

void Kalendarz::setRok(int rok) {

	if(rok>=1)
	{
		mCzyPoprawan=true;
		mRok = rok;
	}
	else
	{
		mCzyPoprawan=false;
		mRok=-1;
	}

}

Kalendarz::Kalendarz():mDzien(0), mMiesiac(0), mRok(0), mCzyPoprawan(false), mCzyPrzystepy(false)
{

}
bool Kalendarz::czyPoprawnaData(int dzien, int miesiac, int rok)
{

	setRok(rok);
	if(mCzyPoprawan == false){ return false;}
	setMiesiac(miesiac);
	if(mCzyPoprawan == false){ return false;}
	setDzien(dzien);
	if(mCzyPoprawan == false){ return false;}

	return true;
}

bool Kalendarz::isCzyPoprawan() const {
	return mCzyPoprawan;
}

int Kalendarz::getDzien() const {
	return mDzien;
}

int Kalendarz::getMiesiac() const {
	return mMiesiac;
}

int Kalendarz::getRok() const {
	return mRok;
}

bool Kalendarz::czyPrzystepny(int rok)
{
	if((mRok%4==0 && mRok%100!=0) || mRok%400==0)
	{
		return mCzyPrzystepy=true;
	}
return false;
}
