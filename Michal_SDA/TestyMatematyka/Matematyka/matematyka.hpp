/*
 * silnia.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef MATEMATYKA_HPP_
#define MATEMATYKA_HPP_

class Matematyka
{

public:

	int policzSilnia(int wartosc);
	int fibonacci(int wartosc);
	int nwd(int jeden, int dwa);
};

class Kalendarz
{
	int mDzien;
	int mMiesiac;
	int mRok;
	bool mCzyPoprawan;
	bool mCzyPrzystepy;

	void setDzien(int dzien);
	void setMiesiac(int miesiac);
	void setRok(int rok);
public:
	Kalendarz();
bool czyPrzystepny(int rok);
	bool czyPoprawnaData(int dzien, int miesiac, int rok);
	bool isCzyPoprawan() const;
	int getDzien() const;
	int getMiesiac() const;
	int getRok() const;
};


#endif /* MATEMATYKA_HPP_ */
