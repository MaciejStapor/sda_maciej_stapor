/*
 * main.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */
#include "gtest/gtest.h"
#include "matematyka.hpp"

TEST(Matematyka,policzSilnia)
{
	Matematyka a;
	EXPECT_EQ(6,a.policzSilnia(3));
	EXPECT_EQ(1,a.policzSilnia(0));
	EXPECT_EQ(1,a.policzSilnia(1));
	EXPECT_EQ(362880,a.policzSilnia(9));
}

TEST(Matematyka,fibonacci)
{
	Matematyka a;
	EXPECT_EQ(0,a.fibonacci(0));
	EXPECT_EQ(1,a.fibonacci(1));
	EXPECT_EQ(5,a.fibonacci(5));
	EXPECT_EQ(55,a.fibonacci(10));
}

TEST(Matematyka,nwd)
{
	Matematyka a;
	EXPECT_EQ(6, a.nwd(12, 18));
}

TEST(Matematyka,Kalendarz)
{
	Kalendarz a;
	EXPECT_TRUE(a.czyPoprawnaData(24, 04, 2017));
	EXPECT_TRUE(a.czyPoprawnaData(29, 02, 2012));
}


TEST(Matematyka,Konstruktor)
{
	Kalendarz a;
	EXPECT_EQ(0, a.getDzien());
	EXPECT_EQ(0, a.getMiesiac());
	EXPECT_EQ(0, a.getRok());
	EXPECT_FALSE( a.isCzyPoprawan());
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}



