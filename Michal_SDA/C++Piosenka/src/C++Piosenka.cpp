//============================================================================
// Name        : C++Piosenka.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <memory>
#include <algorithm>

using namespace std;

class Piosenka
{
	string mTytul;
	string mWykonawca;
	float mDlugosc;
public:
	Piosenka(string tytul, string wykonawca, float dlugosc)
	:mTytul(tytul)
	,mWykonawca(wykonawca)
	,mDlugosc(dlugosc)
	{

	}

	void pokaz()
	{
		cout<<"tytul: "<<mTytul<<", Wykonawca "<<mWykonawca<<", dlugosc "<<mDlugosc<<endl;
	}

//	bool operator==(Piosenka& one)
//	{
//		return this->mTytul.compare(one.mTytul) && this->mWykonawca.compare(one.getWykonawca()) && this->mDlugosc == one.getDlugosc();
//	}

	float getDlugosc() const {
		return mDlugosc;
	}

	const string& getTytul() const {
		return mTytul;
	}

	const string& getWykonawca() const {
		return mWykonawca;
	}
};
class Odtwarzacz
{
	vector<shared_ptr<Piosenka>> mPtr;
	shared_ptr<Piosenka> mAktualny;

public:

	void dodajPiosenke(Piosenka nowa)
	{
		mPtr.push_back(make_shared<Piosenka> (nowa));
	}
	void odtworz()
	{
		if(mAktualny == nullptr)
			mAktualny = mPtr[0];
		mAktualny->pokaz();
	}
	void nastepna()
	{
		auto it = find(mPtr.begin(), mPtr.end(), mAktualny);

		if(it != (mPtr.end()-1))
		{
			advance(it, 1);
			mAktualny = *it;
		}
		else
		{
			mAktualny = *(mPtr.begin());
		}

		odtworz();
	}

	void poprzednia()
	{
		auto it = find(mPtr.begin(), mPtr.end(), mAktualny);
		if(it != (mPtr.begin()))
		{
			advance(it, -1);
			mAktualny = *it;
		}

		else
		{
			mAktualny = *(mPtr.begin());
		}
		odtworz();
	}

};

int main() {

Odtwarzacz nowy;

nowy.dodajPiosenke(Piosenka("ala", "karolina", 3.34));
nowy.dodajPiosenke(Piosenka("cos", "Tomek", 1.22));
nowy.dodajPiosenke(Piosenka("kto", "ania", 5.22));

nowy.odtworz();
nowy.nastepna();
nowy.nastepna();
nowy.nastepna();
nowy.nastepna();
nowy.nastepna();

	return 0;
}
