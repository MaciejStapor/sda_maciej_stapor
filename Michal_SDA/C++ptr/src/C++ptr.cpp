//============================================================================
// Name        : C++ptr.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <memory>
using namespace std;

class Kurczak
{
public:
	Kurczak()
	{
		cout<<"Kurczak"<<endl;
	}

	~Kurczak()
	{
		cout<<"~Kurczak"<<endl;
	}
};

void fun()
{
	Kurczak* zwykly = new Kurczak();
	unique_ptr<Kurczak> nowy (new Kurczak);

//	delete zwykly;
}

class Produkt
{
	float mCena;
	string mNazwa;

public:
	Produkt(float cena, string nazwa)
	:mCena(cena)
	,mNazwa(nazwa)
	{

	}

	Produkt()
	:Produkt(0, "")
	{

	}

	void pokaz()
	{
		cout<<"Cena: "<<mCena<<", nazwa "<<mNazwa<<endl;
	}

};



int main() {

//	fun();

	unique_ptr<Produkt> ptr1 (new Produkt(23.3, "box"));
	unique_ptr<Produkt> ptr2 (new Produkt);

	shared_ptr<Produkt> ptr3 = make_shared<Produkt>(11.3, "rower");
	shared_ptr<Produkt> ptr4 = make_shared<Produkt>();

	shared_ptr<Produkt> ptr5[4] = {{make_shared<Produkt>(2.34, "cos")}, {make_shared<Produkt>(334.2, "samochod")}, {make_shared<Produkt>(2.22, "jako")}, {make_shared<Produkt>(4, "guma")}};

	for(int i = 0; i < 4; i++)
		ptr5[i]->pokaz();

	return 0;
}
