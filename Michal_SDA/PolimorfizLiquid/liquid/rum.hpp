/*
 * rum.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef RUM_HPP_
#define RUM_HPP_
#include "liquid.hpp"

class Rum: public Liquid
{
public:
enum Colour
{
	Dark,
	White
};
private:
Colour mColour;
public:
	Rum();
	Rum(float,Colour);
	~Rum();
	void remove(float);
	void removeAll();
	void add(float);
	void show();
	float getFat() const;
};




#endif /* RUM_HPP_ */
