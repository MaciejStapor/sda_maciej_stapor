/*
 * decaf.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef DECAF_HPP_
#define DECAF_HPP_
#include "coffee.hpp"


class Decaf: virtual public Coffee
{

	float mCaffeine;
public:
	Decaf();
	Decaf(float);
	~Decaf();
	void remove(float);
	void removeAll();
	void add(float);
	void show();

};


#endif /* DECAF_HPP_ */
