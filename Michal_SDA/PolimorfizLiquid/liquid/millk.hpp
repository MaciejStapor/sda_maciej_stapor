/*
 * millk.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef MILLK_HPP_
#define MILLK_HPP_
#include "liquid.hpp"

class Millk: public Liquid
{

	float mFat;
public:
	Millk();
	Millk(float, float);
	~Millk();
	void remove(float);
	void removeAll();
	void add(float);
	void show();
	float getFat() const;
};



#endif /* MILLK_HPP_ */
