/*
 * cup.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef CUP_HPP_
#define CUP_HPP_
#include "liquid.hpp"
#include <cstdlib>

class Cup
{
	Liquid** mContent;
	size_t mSize;
	void resize();
	void clear();
	float amountOfLiquid();


public:
	Cup();
	void add(Liquid*);
	void takeSip(float);
	void spill();
	void show();

};




#endif /* CUP_HPP_ */
