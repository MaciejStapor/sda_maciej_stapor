/*
 * rum.cpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

/*
 * millk.cpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */
#include "rum.hpp"
#include <iostream>
Rum::Rum(): mColour(Dark)
{

}
Rum::Rum(float amount, Colour colour): mColour(colour)
{
	add(amount);
}
Rum::~Rum()
{

}
void Rum::remove(float amount)
{
	mAmount-=amount;
}
void Rum::removeAll()
{
	mAmount=0;
}
void Rum::add(float amount)
{
	mAmount+=amount;
}
void Rum::show()
{
	switch(mColour)
	{
	case Dark:
		std::cout<<"Amount of Dark Rum "<<mAmount<<" ml."<<std::endl;
		break;
	case White:
		std::cout<<"Amount of White Rum "<<mAmount<<" ml."<<std::endl;
		break;
	}

}


