/*
 * millk.cpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */
#include "millk.hpp"
#include <iostream>
	Millk::Millk(): mFat(0)
	{

	}
	Millk::Millk(float fat, float amount): mFat(fat)
	{
		add(amount);
	}
	Millk::~Millk()
	{

	}
	void Millk::remove(float amount)
	{
		mAmount-=amount;
	}
	void Millk::removeAll()
	{
		mAmount=0;
	}
	void Millk::add(float amount)
	{
		mAmount+=amount;
	}
	void Millk::show()
	{
		std::cout<<"Amount of Millk "<<mAmount<<" ml."<<" fat "<<mFat<<"%"<<std::endl;
}

float Millk::getFat() const {
	return mFat;
}
