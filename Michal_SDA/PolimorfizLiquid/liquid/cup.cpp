/*
 * cup.cpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */
#include <iostream>
#include "cup.hpp"


void Cup::resize()
{
	if(mSize==0)
	{
		mContent=new Liquid*[++mSize];
	}
	else
	{
		Liquid** tmp=new Liquid*[++mSize];
		for(size_t i=0; i<mSize-1; i++)
		{
			tmp[i]=mContent[i];
		}
		delete[] mContent;
		mContent=tmp;
	}
}

void Cup::clear()
{
	for(size_t i=0; i<mSize-1; i++)
	{
		delete mContent[i];
	}

	delete[] mContent;
	mContent=0;
	mSize=0;
}

float Cup::amountOfLiquid()
{
	float tmp=0;

	for(size_t i=0; i<mSize; i++)
	{
		tmp += mContent[i]->getAmount();
	}

	return tmp;
}



Cup::Cup(): mContent(0), mSize(0)
{

}
void Cup::add(Liquid* liquid)
{
	resize();
	mContent[mSize-1]=liquid;

}
void Cup::takeSip(float amount)
{
	if(mSize==0 || amount<=0)
	{
		//nothing to do;
	}
	else
	{
		float tmp=amountOfLiquid();

		if(tmp>amount)
		{
			for(size_t i=0; i<mSize; i++)
			{
				float percent = mContent[i]->getAmount()/tmp;
				mContent[i]->remove(amount*percent);
			}
		}
		else
		{
			clear();
		}


	}
}

void Cup::spill()
{
	if(mSize==0)
	{
		//nothing to do;
	}
	else
	{
		for(size_t i=0; i<mSize; i++)
		{
			mContent[i]->removeAll();
		}
		clear();
	}

}

void Cup::show()
{
	if(mSize==0)
	{
		std::cout<<"Empty"<<std::endl;
	}
	else
	{

		for(size_t i=0; i<mSize; i++)
		{
			mContent[i]->show();
		}
	}
}


