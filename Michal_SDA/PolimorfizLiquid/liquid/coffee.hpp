/*
 * coffee.hpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */

#ifndef COFFEE_HPP_
#define COFFEE_HPP_
#include "liquid.hpp"

class Coffee: public Liquid
{
protected:
	float mCaffeine;
public:
	Coffee();
	Coffee(float, float);
	~Coffee();
	void remove(float);
	void removeAll();
	void add(float);
	void show();
	float getCaffeine() const;
};




#endif /* COFFEE_HPP_ */
