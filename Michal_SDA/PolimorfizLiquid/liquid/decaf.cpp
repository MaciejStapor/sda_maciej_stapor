/*
 * decaf.cpp
 *
 *  Created on: 09.05.2017
 *      Author: Admin
 */
#include "decaf.hpp"
#include <iostream>

Decaf::Decaf(): mCaffeine(0)
{

}
Decaf::Decaf(float amount): mCaffeine(0)
{
	add(amount);
}
Decaf::~Decaf()
{

}
void Decaf::remove(float amount)
{
	mAmount-=amount;
}
void Decaf::removeAll()
{
	mAmount=0;
}
void Decaf::add(float amount)
{
	mAmount+=amount;
}

void Decaf::show()
{

	std::cout<<"Amount of DecafCoffee "<<mAmount<<" ml."<<std::endl;
}




