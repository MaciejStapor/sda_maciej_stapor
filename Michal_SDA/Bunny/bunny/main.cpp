/*
 * bunny.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */
#include "bunny.hpp"


using namespace std;

string getRandomName();

void breed(TwoWayList<Bunny>& bunnyList)
{
	for(unsigned int i=0; i<bunnyList.size(); i++)
	{
		if(bunnyList.getValue(i)->isMale())
		{
			for(unsigned int k=0; k<bunnyList.size(); k++)
				if(!(bunnyList.getValue(k)->isMale()))
				{
					bunnyList.addEnd(Bunny(getRandomName(),(bunnyList.getValue(k)->getColor())));
				}
			break;
		}
	}
}

int countMutated(TwoWayList<Bunny>& bunnyList)
{
	int count=0;
	for(unsigned int i=0; i<bunnyList.size(); i++)
		{
			if(bunnyList.getValue(i)->isVampire())
				count++;
		}
	return count;
}

void spreadMutation(TwoWayList<Bunny>& bunnyList)
{
	for(int i=0; i<countMutated(bunnyList); i++)
			{
				bunnyList.getValue(rand()%bunnyList.size())->setVampire(true);

			}
}

void killHalf(TwoWayList<Bunny>& bunnyList)
{
	if(bunnyList.size()>999)
	{
		for(int i=0; i<500; i++)
		{
		bunnyList.deletePosition(rand()%bunnyList.size());
		}

	}
}

void update(TwoWayList<Bunny>& bunnyList)
{
	if(bunnyList.size() > 1000)
	{
		killHalf(bunnyList);
	}

	//		for()
	{
		//zwieksz wiek krolikow - jesli zwroci bool to trzeba zabic kroliczka(delete z listy)
		//rozmnozyc
		//zmutowac
		//wypisujemy co sie dzieje
	}
}

string getRandomName()
{
	size_t nameCount = 7;
	std::string names[nameCount];

	names[0]="max";
	names[1]="tom";
	names[2]="kik";
	names[3]="pop";
	names[4]="tix";
	names[5]="mat";
	names[6]="chic";
	names[7]="kok";
	names[8]="cmok";

	int random = rand() % nameCount;

	return names[random];
}


int main()
{


	srand (time(NULL));

	TwoWayList<Bunny> kroliczkowo;

	kroliczkowo.addEnd(Bunny(getRandomName()));
	kroliczkowo.addEnd(Bunny(getRandomName()));
	kroliczkowo.addEnd(Bunny(getRandomName()));
	kroliczkowo.addEnd(Bunny(getRandomName()));
	kroliczkowo.addEnd(Bunny(getRandomName()));

	while(!kroliczkowo.empty())
	{
		update(kroliczkowo);
	}


	cout<<"Nie ma kroliczkow :("<<endl;

	return 0;
}



