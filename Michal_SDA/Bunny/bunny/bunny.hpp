/*
 * bunny.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef BUNNY_HPP_
#define BUNNY_HPP_
#include <string>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include "list.hpp"

class Bunny
{
	enum Sex
	{
		Male,
		Female
	};

	enum Color
	{
		white,
		brown,
		black,
		spotted
	};

	Sex mSex;
	Color mColor;
	unsigned int mAge;
	std::string mName;
	bool mVampire;

	void randSex()
	{
		mSex=(rand()%2==1) ? Male : Female;
	}

	void randColor()
	{
		switch(rand()%4)
		{
		case 0:
			setColor(white);
			break;
		case 1:
			setColor(spotted);
			break;
		case 2:
			setColor(brown);
			break;
		case 3:
			setColor(black);
			break;
		}
	}

	void randVampire()
	{
		mVampire=(rand()%100<2) ? true : false;
	}

	void show()
	{
		std::cout<<"Bunny "<<getName()<<" was born!"<<std::endl;
	}


public:
	Bunny(std::string name)
	: mAge(0)
	, mName(name)
	{
		show();
		randSex();
		randVampire();
		randColor();
	}

	Bunny(std::string name, Color color)
	:  mColor(color)
	, mAge(0)
	, mName(name)
	{
		show();
		randSex();
		randVampire();
	}
	unsigned int getAge() const {
		return mAge;
	}

	Color getColor() const {
		return mColor;
	}

	const std::string& getName() const {
		return mName;
	}

	Sex getSex() const {
		return mSex;
	}

	bool isVampire() const {
		return mVampire;
	}

	void setVampire(bool vampire) {
		mVampire = vampire;
	}

	void setColor(Color color) {
		mColor = color;
	}

	bool happyBirthday()
	{
		mAge++;
		if(mVampire)
		return mAge<50?false:true;
		else
		return mAge<10?false:true;
	}

	bool isVampire()
	{
		return mVampire?true:false;
	}

	bool isMale()
	{
		return mSex==Male?true:false;
	}

};



#endif /* BUNNY_HPP_ */
