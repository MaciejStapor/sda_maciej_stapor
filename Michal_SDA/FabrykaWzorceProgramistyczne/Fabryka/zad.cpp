/*
 * zad.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include <iostream>
#include "zad.hpp"

using namespace std;
Processor::Processor(int numer): numerSer(numer)
{

}

ProcessorAMG::ProcessorAMG(int numer):Processor(numer)
{

}

ProcessorIntel::ProcessorIntel(int numer):Processor(numer)
{

}

void ProcessorAMG::process()
{
	cout<<"AMG in process"<<endl;
}



void ProcessorIntel::process()
{
	cout<<"Intel in process"<<endl;
}





Cooler::~Cooler()
{
	cout<<"~Cooler"<<endl;
}



void CoolerAMG::cool()
{
	cout<<"AMG cooling"<<endl;
}


void CoolerIntel::cool()
{
	cout<<"Intel cooling"<<endl;
}



AbstractSemiconductorFactory::AbstractSemiconductorFactory(): numer(0)
{

}


Processor* AMDSemiconductorFactory::createProcessor ()
{

	numer++;
	return new ProcessorAMG(numer);
}
Cooler* AMDSemiconductorFactory::createCooler ()
{
	return new CoolerAMG();
}



Processor* IntelSemiconductorFactory::createProcessor ()
{

		numer+=12;
	return new ProcessorIntel(numer);
}
Cooler* IntelSemiconductorFactory::createCooler ()
{
	return new CoolerIntel();
}


void Computer::run()
{
	cout<<"........................................................"<<endl;
	cout<<"Nazywam sie: "<<name<<endl;
	cout<<"Numer ser. "<<processor->numerSer<<endl;
	processor->process();
	cooler->cool();

}

Computer::Computer(string name1, AbstractSemiconductorFactory& factory): name(name1)
{
processor = factory.createProcessor();
cooler = factory.createCooler();
}


Computer::~Computer()
{
	delete processor;
	delete cooler;
}
