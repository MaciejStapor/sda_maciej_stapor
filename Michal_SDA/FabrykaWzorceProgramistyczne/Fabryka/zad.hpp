/*
 * zad.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef ZAD_HPP_
#define ZAD_HPP_
#include <iostream>
using namespace std;

class Processor
{


public:
	int numerSer;
	Processor(int numer);
	virtual void process()=0;
	virtual ~Processor()
	{
		cout<<"~Processor"<<endl;
	}
};

class ProcessorAMG: public Processor
{

public:
	ProcessorAMG(int numer);
	void process();
	~ProcessorAMG()
	{
		cout<<"~ProcessorAMG"<<endl;
	}
};

class ProcessorIntel: public Processor
{

public:
	ProcessorIntel(int numer);
	void process();
	~ProcessorIntel()
		{
			cout<<"~ProcessorIntel"<<endl;
		}
};


class Cooler
{
public:

	virtual void cool()=0;
	virtual ~Cooler();

};

class CoolerAMG: public Cooler
{
public:
	 void cool();

};

class CoolerIntel: public Cooler
{
public:
	void cool();

};

class AbstractSemiconductorFactory
{
protected:
	int numer;
public:
	AbstractSemiconductorFactory();
	virtual Processor* createProcessor ()=0;
	virtual Cooler* createCooler ()=0;
	virtual ~AbstractSemiconductorFactory()
	{
		cout<<"~AbstractSemiconductorFactory"<<endl;
	}
};

class AMDSemiconductorFactory: public AbstractSemiconductorFactory
{
public:
	 Processor* createProcessor ();
	 Cooler* createCooler ();
};

class IntelSemiconductorFactory: public AbstractSemiconductorFactory
{
public:
	 Processor* createProcessor ();
	 Cooler* createCooler ();
};


class Computer
{
	string name;
	Processor* processor;
	Cooler* cooler;
public:
	Computer(string name1, AbstractSemiconductorFactory& factpry);
	void run();
	~Computer();
};






#endif /* ZAD_HPP_ */
