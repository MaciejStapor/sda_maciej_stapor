/*
 * main.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#include <iostream>
#include "zad.hpp"

using namespace std;

int main()
{
IntelSemiconductorFactory intelFactory;
AMDSemiconductorFactory AMDFactory;

Computer intelPC("pc1", intelFactory);
Computer amdPC("pc2", AMDFactory);
Computer intelPC3("pc3", intelFactory);
Computer intelPC4("pc4", intelFactory);
Computer intelPC5("pc5", intelFactory);
Computer amdPC2("pc6", AMDFactory);
Computer amdPC3("pc7", AMDFactory);

intelPC.run();
amdPC.run();
intelPC3.run();
intelPC4.run();
intelPC5.run();
amdPC2.run();
amdPC3.run();
	return 0;
}



