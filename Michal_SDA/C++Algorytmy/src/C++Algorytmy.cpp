//============================================================================
// Name        : C++Algorytmy.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

	vector<int> nowy(201);

	iota(nowy.begin(), nowy.end(), -100);

	for(auto& one : nowy)
		cout<<one<<", ";
	cout<<endl;

	if(all_of(nowy.begin(), nowy.end(),[](int one){return one>0;} ))
		cout<<"Wszystkie sa dodatnie"<<endl;
	else
		cout<<"nie wszystkie sa dodatnie"<<endl;

	if(any_of(nowy.begin(), nowy.end(),[](int one){return one%3 == 0 && one%5 == 0;} ))
		cout<<"istnieje liczba podzielna przez 3, 5 i 30"<<endl;
	else
		cout<<"nie istnieje liczba podzielna przez 3, 5 i 30"<<endl;

	auto it0 = remove_if(nowy.begin(), nowy.end(), [](int one){return one == 0;});
	nowy.resize(distance(nowy.begin(), it0));
	for(auto& one : nowy)
		cout<<one<<", ";
	cout<<endl;

	if(none_of(nowy.begin(), nowy.end(),[](int one){return one == 0;} ))
		cout<<"nie ma zera"<<endl;
	else
		cout<<"jest zero"<<endl;

	if(is_sorted(nowy.begin(), nowy.end()))
		cout<<"posortowany"<<endl;
	else
		cout<<"nie posortowany"<<endl;

	vector<int> nastepny(201);
	auto it = copy_if(nowy.begin(), nowy.end(), nastepny.begin(), [](int one){return one > 90 || one < -90;});
	nastepny.resize(distance(nastepny.begin(), it));
	for(auto& one : nastepny)
		cout<<one<<", ";
	cout<<endl;

	vector<int> nastepny1(10);
	auto it78 = find_if(nowy.begin(), nowy.end(), [](int one){return one == 78;});
	copy_n(it78, 10, nastepny1.begin());
	for(auto& one : nastepny1)
		cout<<one<<", ";
	cout<<endl;

	return 0;
}
