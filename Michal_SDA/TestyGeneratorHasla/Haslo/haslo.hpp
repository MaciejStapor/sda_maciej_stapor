/*
 * haslo.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef HASLO_HPP_
#define HASLO_HPP_
#include <string>

class GeneratorHasla
{
	std::string mHaslo;
	int mDlugosc;

public:
	GeneratorHasla();
	int getDlugosc() const;
	void setDlugosc(int dlugosc);
	const std::string& getHaslo() const;
	void setHaslo(const std::string& haslo);
	std::string hasloRand();

};




#endif /* HASLO_HPP_ */
