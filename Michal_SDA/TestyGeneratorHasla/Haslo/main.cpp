/*
 * main.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "haslo.hpp"
#include "gtest/gtest.h"
#include <iostream>

TEST(Konstruktor, generatorHasla)
{
	GeneratorHasla a;
	EXPECT_EQ(0, a.getDlugosc());
	EXPECT_EQ("", a.getHaslo());
}

TEST(GeneratorHasla, geterySetery)
{
	GeneratorHasla a;
	a.setDlugosc(3);
	EXPECT_EQ(3, a.getDlugosc());
	a.setHaslo("abc");
	EXPECT_EQ("abc", a.getHaslo());
}

TEST(GeneratorHasla, hasloRand)
{
	GeneratorHasla a;
	a.setDlugosc(7);
	EXPECT_EQ(7, a.getDlugosc());
	EXPECT_NE(" ", a.getHaslo());
}



int main(int argc, char **argv)
{
	GeneratorHasla a;
	a.setDlugosc(23);
	a.hasloRand();
	std::cout<<"Haslo " << a.getHaslo()<<std::endl;
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
