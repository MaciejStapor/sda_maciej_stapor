/*
 * haslo.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "haslo.hpp"
#include<ctime>
#include <cstdlib>

int GeneratorHasla::getDlugosc() const {
	return mDlugosc;
}

void GeneratorHasla::setDlugosc(int dlugosc) {
	mDlugosc = dlugosc;
}

const std::string& GeneratorHasla::getHaslo() const {
	return mHaslo;
}

void GeneratorHasla::setHaslo(const std::string& haslo) {
	mHaslo = haslo;
}
GeneratorHasla::GeneratorHasla(): mHaslo(""), mDlugosc(0)
{
srand(time(NULL));
}


std::string GeneratorHasla::hasloRand()
{
for(int i=0; i<mDlugosc; i++)
{
	mHaslo+=rand()%36+60;
}

	return mHaslo;
}
