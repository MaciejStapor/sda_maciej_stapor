//============================================================================
// Name        : C++11.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
#include <cmath>
#include <vector>
#include <list>
#include <tuple>

using namespace std;

#define POTEGA(x) pow(2, x)

constexpr int potega(int x)
{
	return pow(2, x);
}

constexpr int silnia(int x)
{
	return x <= 1 ? 1 : x*silnia(x - 1);
}

class Fish
{
public:
	virtual void swim(int speed)
	{
		for(int i = 0; i < speed; i++)
			cout<<"plum ";
		cout<<endl;
	}
	virtual ~Fish() = default;
};

class GoldFish final: public Fish
{
public:
	void swim(int speed) override
	{
		for(int i = 0; i < speed; i++)
			cout<<"Goldplum ";
		cout<<endl;
	}
};

//class WhiteGoldFish: public GoldFish
//{
//public:
//	void swim(int speed) override
//	{
//		for(int i = 0; i < speed; i++)
//			cout<<"WhiteGoldplum ";
//		cout<<endl;
//	}
//};

class NonCopy
{
public:
	NonCopy() = default;
	NonCopy(const NonCopy& one) = delete;
	~NonCopy() = default;
	NonCopy& operator=(const NonCopy& one) = delete;
};

class MaszynaMnozaca
{
protected:
	float mX;
public:
	MaszynaMnozaca(float x)
	:mX(x)
	{

	}

	float pomnoz(int x) = delete;

	virtual float pomnoz(float x) final
	{
		return x*mX;
	}

	virtual ~MaszynaMnozaca() = default;
};

class MaszynaMnozacoDodajaca: public MaszynaMnozaca
{
public:
	MaszynaMnozacoDodajaca(float x)
	:MaszynaMnozaca(x)
	{

	}

	float pomnoz(int x) = delete;

//	float pomnoz(float x)
//	{
//		return x*mX*3;
//	}

	float dodaj (float x)
	{
		return x+mX;
	}

	virtual ~MaszynaMnozacoDodajaca() = default;
};

tuple<int, float, string> krotkaFun(int id)
{
	if(id == 1)
		return make_tuple(32, 1.233, "ala");
	else if(id == 2)
		return make_tuple(11, 23.34, "marek");
	else
		return make_tuple(23, 33.2, "ania");
}


int main() {

//	int* x = nullptr;
//	constexpr

	int jeden = potega(1);
	int dwa = potega(2);
	int trzy = potega(3);
	int cztery = potega(4);
	int piec = potega(5);

//	int jeden = POTEGA(1);
//	int dwa = POTEGA(2);
//	int trzy = POTEGA(3);
//	int cztery = POTEGA(4);
//	int piec = POTEGA(5);

	cout<<"Potega "<<jeden<<" "<<dwa<<" "<<trzy<<" "<<cztery<<" "<<piec<<" "<<endl;
	cout<<"Silnia "<<silnia(4)<<endl;

	Fish* one;
	GoldFish second;
	one = &second;
	one->swim(4);

	NonCopy nonCopy1;
	NonCopy nonCopy2;
//	nonCopy1 = nonCopy2;

	MaszynaMnozaca maszynaMnozaca1(3.01);
//	maszynaMnozaca1.pomnoz(23);
	cout<<"maszynaMnozaca "<<maszynaMnozaca1.pomnoz(23.23f)<<endl;

	vector<int> vec;

	for(int i = 0; i < 100; i++)
		vec.push_back(i);

	for(auto it = vec.begin(); it != vec.end(); advance(it, 2))
		cout<<*it<<" ";
	cout<<endl;

	for(auto& one : vec)
		cout<<one<<" ";
	cout<<endl;

	float tabF[5] = {2.3, 4.33, 22.22, 90.01, 0.01};

	for(auto& one : tabF)
		cout<<one<<" ";
	cout<<endl;

	for(int i = 0; i < 5; i++)
		cout<<tabF[i]<<" ";
	cout<<endl;

	list<int> vecL;

	for(int i = 0; i < 10; i++)
		vecL.push_back(i);

	for(auto& one : vecL)
		one*=2;

	for(const auto& one : vecL)
		cout<<one<<" ";
	cout<<endl;

	auto lambda = [](){cout<<"Hello World";};
	lambda();
	cout<<endl;

	int x = 10;
	int y = 20;
	auto dodaj = [](int x, int y){return x+y;};

	cout<<dodaj(x, y);
	cout<<endl;

	auto krotka = krotkaFun(1);
	cout<<get<0>(krotka)<<", "<<get<1>(krotka)<<", "<<get<2>(krotka)<<endl;



	return 0;
}
