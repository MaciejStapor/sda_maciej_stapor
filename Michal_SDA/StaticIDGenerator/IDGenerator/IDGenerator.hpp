/*
 * IDGenerator.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef IDGENERATOR_HPP_
#define IDGENERATOR_HPP_


class IDGenerator
{
	static int mID;
public:
	static int getId() ;
};



#endif /* IDGENERATOR_HPP_ */
