//============================================================================
// Name        : KonteneryAdapteryKontenerow.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stack>
#include <vector>
using namespace std;

string reverseString(string& newStr)
{
	string tmp;
	stack<char, vector<char> > reverseString;
	for(unsigned int i=0; i<newStr.size();i++)
	{
		reverseString.push(newStr[i]);
	}

	while(!reverseString.empty())
	{
		tmp+=reverseString.top();
		reverseString.pop();
	}
	return tmp;
}

int main() {

	string m="evil";
	cout<<m<<", reverse string: "<<reverseString(m)<<endl;

	return 0;
}
