//============================================================================
// Name        : Wyjatki.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>

using namespace std;

double dziel(double, double);

int main()
{
	double a,b;
	cout<<"Podaj dwie liczby: ";
	cin>>a>>b;

	try
	{
		cout<<"Wynik: "<<dziel(a,b)<<endl;
	}
	catch(string& dziel)
	{
		cout<<dziel<<endl;
	}

	return 0;
}

double dziel(double a, double b)
{
	if(b==0)
	{
		string dziel = "Blad!! Nie dziel przez 0";
		throw dziel;
	}

	return a/b;
}
