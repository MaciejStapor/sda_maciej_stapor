//============================================================================
// Name        : KonteneryZad7.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
#include <vector>
#include <numeric>
using namespace std;

ostream& operator<<(ostream& os, vector<int> nowy)
{
	os<<"[ ";
	for(vector<int>::iterator it = nowy.begin(); it != nowy.end(); it++ )
	{
		os<<*it<<", ";
	}
	os<<"]"<<endl;

	return os;
}

int kwadrat(int& x)
{
	x*=x;
	return x;
}

vector<int> vectorDoKwadratu(int x, int y)
{
	vector<int> nowy;
	for(int i = x; i < y; i++)
			nowy.push_back(i);

	for_each(nowy.begin(), nowy.end(), kwadrat);

	return nowy;
}

int sumaVector(vector<int> nowy)
{
	return accumulate(nowy.begin(), nowy.end(), 0);
}

int main() {

	int x = 12;
	int y = 20;

	vector<int> nowy = vectorDoKwadratu(x, y);
	cout<<"vector kwardatow: ";
	cout<<nowy;

	cout<<"Suma kwadratow: "<<sumaVector(nowy)<<endl;

	return 0;
}
