//============================================================================
// Name        : KonteneryZad17.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
#include <list>
using namespace std;

bool ileSpacji(char& jeden)
{
	return jeden == ' ';
}
class CzyTakieSameStr
{
	string mNapis;
public:
	CzyTakieSameStr(string napis)
	:mNapis(napis)
{

}
	bool operator()(string& first)
	{
		return !mNapis.compare(first);
	}
};

void wyczyscDodatkoweSpacje(string& napis)
{
	while(napis[0] == ' ')
	{
		napis.erase(0, 1);
	}
	while(*(--napis.end()) == ' ')
	{
		int tmp = napis.size();
		tmp--;
		napis.erase(tmp, 1);
	}

	for(unsigned int i = 0; i < napis.size()-1; i++ )
	{
		if(napis[i] == napis[i+1])
		{
			napis.erase(i, 1);
			--i;
		}

	}
}

list<string> listaSlow(string napis)
{
	list<string> slowa;
	int count = count_if(napis.begin(), napis.end(), ileSpacji);
	count++;
	for(int i = 0; i < count; i++)
	{
		int pos = napis.find(' ');
		string tmpStr = napis.substr(0, pos);

		slowa.push_back(tmpStr);
		napis.erase(0, pos+1);
	}

	return slowa;
}

void pokazWystapienia(list<string> nowa, string napis)
{
	list<string> tmp = nowa;
	tmp.sort();
	tmp.resize(distance(tmp.begin(), (unique(tmp.begin(), tmp.end()))));

	for(list<string>::iterator it = tmp.begin(); it != tmp.end(); it++)
	{
		int iloscWystapnien = count_if(nowa.begin(), nowa.end(), CzyTakieSameStr(*it));

		cout<<"Slowo: '"<<*it<<"' wystepuje "<<iloscWystapnien<<" razy."<<endl;
	}
}

int main() {

string napis = "     ala      ma  ma albo    nie  kota    kota  ";

wyczyscDodatkoweSpacje(napis);
list<string> slowa = listaSlow(napis);
pokazWystapienia(slowa, napis);

	return 0;
}
