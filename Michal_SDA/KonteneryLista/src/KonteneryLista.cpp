//============================================================================
// Name        : KonteneryLista.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>

using namespace std;

bool sortInt(const int& first, const int& second)
{
	return first>second;
}

bool sortStr(const string& first, const string& second)
{
	return first.size()>second.size();
}

bool removeGreater(const int& value)
{
	return value<10;
}


int main() {

	list<int> listInt;

	listInt.push_back(2);
	listInt.push_back(15);
	listInt.push_back(1);
	listInt.push_back(100);
	listInt.push_back(2);


	bool pal=true;
	list<int>::reverse_iterator ikt = listInt.rbegin();

	for(list<int>::iterator it = listInt.begin(); it != listInt.end();it++ , ikt++)
	{
		if(*it!=*ikt)
		{
			pal=false;
			break;
		}
	}

	pal?cout<<"lista jest palindromem"<<endl : cout<<"lista nie jest palindromem"<<endl;

	cout<<"iterator: [ ";
	for(list<int>::iterator it = listInt.begin(); it != listInt.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;

	listInt.sort();

	cout<<"sort: [ ";
	for(list<int>::iterator it = listInt.begin(); it != listInt.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;

	listInt.sort(sortInt);

	cout<<"sort funkcja: [ ";
	for(list<int>::iterator it = listInt.begin(); it != listInt.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;

	list<int> listInt2;

	listInt2.push_back(22);
	listInt2.push_back(12);
	listInt2.push_back(102);
	listInt2.push_back(2);
	listInt2.push_back(22);

	cout<<"iterator list 1.: [ ";
	for(list<int>::iterator it = listInt.begin(); it != listInt.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;

	cout<<"iterator list 2.: [ ";
	for(list<int>::iterator it = listInt2.begin(); it != listInt2.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;


	listInt2.splice(listInt2.begin(), listInt, listInt.begin(), --listInt.end());

	cout<<"iterator list 1+2.: [ ";
	for(list<int>::iterator it = listInt2.begin(); it != listInt2.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;

	listInt2.remove_if(removeGreater);

	cout<<"iterator list 1+2 remove<10.: [ ";
	for(list<int>::iterator it = listInt2.begin(); it != listInt2.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;


	cout<<"iterator list 1.: [ ";
	for(list<int>::iterator it = listInt.begin(); it != listInt.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;

	listInt2.merge(listInt);

	cout<<"iterator list 1+2 merge.: [ ";
	for(list<int>::iterator it = listInt2.begin(); it != listInt2.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;

	listInt2.remove(22);

	cout<<"iterator list 1+2 remove 22.: [ ";
	for(list<int>::iterator it = listInt2.begin(); it != listInt2.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;

	list<string> listStr;
	listStr.push_back("dan");
	listStr.push_back("kukud");
	listStr.push_front("maxss");
	listStr.push_back("kadkjjro");
	listStr.push_back("tom");
	listStr.push_front("front");
	listStr.push_back("back");

	cout<<"back: "<<listStr.back()<<endl;
	cout<<"front: "<<listStr.front()<<endl;

	cout<<"iterator: [ ";
	for(list<string>::iterator it = listStr.begin(); it != listStr.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;

	listStr.sort();

	cout<<"sort: [ ";
	for(list<string>::iterator it = listStr.begin(); it != listStr.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;

	listStr.sort(sortStr);

	cout<<"sort po dlugosci: [ ";
	for(list<string>::iterator it = listStr.begin(); it != listStr.end();it++)
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;


	cout<<"pop_front: [ ";
	while(!listStr.empty())
	{
		cout<<listStr.front()<<", ";
		listStr.pop_front();
	}
	cout<<"]"<<endl;


	return 0;
}
