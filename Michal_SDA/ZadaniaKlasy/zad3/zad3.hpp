/*
 * zad3.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef ZAD3_HPP_
#define ZAD3_HPP_

class Czas
{
	int mGodziny, mMinuty, mSekundy;
public:
	Czas();

	Czas(int godziny, int minuty, int sekundy);
	int getGodziny() const;

	void setGodziny(int godziny);
	int getMinuty() const;

	void setMinuty(int minuty);

	int getSekundy() const;

	void setSekundy(int sekundy);

	void wypisz();

	void przesunGodzine(int g);

	void przesunSekundy(int s);

	void przesunMinuty(int m);

	Czas obliczRoznice(Czas& nowy);


};



#endif /* ZAD3_HPP_ */
