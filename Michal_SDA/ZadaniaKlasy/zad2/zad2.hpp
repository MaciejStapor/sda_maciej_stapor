/*
 * zad2.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef ZAD2_HPP_
#define ZAD2_HPP_

class Data
{
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;
public:
	Data();

	Data(unsigned int dzien, unsigned int miesiac, unsigned int rok);
	unsigned int getDzien() const;
	void setDzien(unsigned int dzien);
	unsigned int getMiesiac() const;
	void setMiesiac(unsigned int miesiac);
	unsigned int getRok() const;
	void setRok(unsigned int rok);
	void wypisz();
	void przesunDzien(int d);
	void przesunMiesiac(int d);
	void przesunRok(int d);
	Data roznica(Data nowa);
};


#endif /* ZAD2_HPP_ */
