/*
 * zad1.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#include <iostream>
#include <cmath>
#include "zad1.hpp"
using namespace std;



Punkt::Punkt()
{

}
Punkt::Punkt(int x, int y):mX(x), mY(y)
{

}

int Punkt::getX() const
{
	return mX;
}

void Punkt::setX(int x)
{
	mX = x;
}

int Punkt::getY() const
{
	return mY;
}

void Punkt::setY(int y)
{
	mY = y;
}

void Punkt::wypisz()
{
	cout<<"["<<mX<<", "<<mY<<"]"<<endl;
}

void Punkt::przesunPunkt(Punkt& nowy)
{
	mX++;
	mY++;
}

void Punkt::przesunXY(int x, int y)
{
	mX=x;
	mY=y;
}

void Punkt::przesunX(int& x)
{
	mX=x;
}
void Punkt::przesunY(int& y)
{
	mY=y;
}
double Punkt::obliczOdleglosc(Punkt& nowy)
{
	return sqrt(pow((nowy.mX-mX),2)+pow((nowy.mY-mY),2));
}

double Punkt::obliczOdleglosc(double x, double y)
{
	return sqrt(pow((x-mX),2)+pow((y-mY),2));
}


KolorowyPunkt::KolorowyPunkt(Kolor kolor, int x, int y):Punkt(x,y)
{
	mKolor=kolor;
}
void KolorowyPunkt::wypisz()
{
	cout<<"["<<mX<<", "<<mY<<"]  ";

	switch(mKolor)
	{
	case bialy:
		cout<<" Bialy " << endl;
		break;
	case czarny:
		cout<<" Czarny " << endl;
		break;
	}
}

