/*
 * zad1.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */
#include <iostream>
using namespace std;
#ifndef ZAD1_HPP_
#define ZAD1_HPP_

class Punkt
{
protected:
	int mX,mY;
public:



	Punkt();

	Punkt(int x=2, int y=2);


	int getX() const;


	void setX(int x);

	int getY() const;


	void setY(int y);


	void wypisz();


	void przesunPunkt(Punkt& nowy);


	void przesunXY(int x, int y);


	void przesunX(int& x);

	void przesunY(int& y);

	double obliczOdleglosc(Punkt& nowy);


	double obliczOdleglosc(double x, double y);

};

class KolorowyPunkt: public Punkt

{
	int mKolor;

public:
	enum Kolor
		{
			bialy,
			czarny
		};

	KolorowyPunkt(Kolor kolor, int x, int y);

	void wypisz();

};




#endif /* ZAD1_HPP_ */
