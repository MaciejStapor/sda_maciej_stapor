/*
 * zad4.cpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */
#include <iostream>
#include "zad4.hpp"
#include <cmath>
using namespace std;



Data::Data()
{
	mDzien=1;
	mMiesiac=1;
	mRok=1900;
}

Data::Data(unsigned int dzien, unsigned int miesiac, unsigned int rok)
{

	if(dzien>31)mDzien=31;
	else if(dzien<1)mDzien=1;
	else mDzien=dzien;

	if(miesiac>12)mMiesiac=12;
	else if(miesiac<1)mMiesiac=1;
	else mMiesiac=miesiac;

	if(rok<1900)mRok=1900;
	else mRok=rok;

}

unsigned int Data::getDzien() const
{
	return mDzien;
}

void Data::setDzien(unsigned int dzien)
{
	if(dzien>31)mDzien=31;
	else if(dzien<1)mDzien=1;
	else mDzien=dzien;
}

unsigned int Data::getMiesiac() const
{
	return mMiesiac;
}

void Data::setMiesiac(unsigned int miesiac)
{
	if(miesiac>12)mMiesiac=12;
	else if(miesiac<1)mMiesiac=1;
	else mMiesiac=miesiac;
}

unsigned int Data::getRok() const
{
	return mRok;
}

void Data::setRok(unsigned int rok)
{
	if(rok<1900)mRok=1900;
	else mRok=rok;
}

void Data::wypisz()
{
	cout << "Data: "<< mDzien<<"."<<mMiesiac<<"."<<mRok<<endl;
}

void Data::przesunDzien(int d)
{
	if ((mDzien+d)<=31) mDzien+=d;
	else
	{
		int pomocnicza=0;
		mDzien=(mDzien+d)%31;
		pomocnicza=(mDzien+d)/31;
		przesunMiesiac(pomocnicza);
	}

}

void Data::przesunMiesiac(int d)
{
	if ((mMiesiac+d)<=31) mMiesiac+=d;
	else
	{
		int pomocnicza=0;
		mMiesiac=(mMiesiac+d)%31;
		pomocnicza=(mMiesiac+d)/31;
		przesunRok(pomocnicza);
	}

}

void Data::przesunRok(int d)
{
	 mRok+=d;

}

Data Data::roznica(Data nowa)
{
	mDzien=abs(mDzien-nowa.mDzien);
	mMiesiac=abs(mMiesiac-nowa.mMiesiac);
	mRok=abs(mRok-nowa.mRok);
	return nowa;
}




Czas::Czas()
{

}

Czas::Czas(int godziny, int minuty, int sekundy): mGodziny(godziny), mMinuty(minuty), mSekundy(sekundy)
{

}

int Czas::getGodziny() const
{
	return mGodziny;
}

void Czas::setGodziny(int godziny)
{
	mGodziny = godziny;
}

int Czas::getMinuty() const
{
	return mMinuty;
}

void Czas::setMinuty(int minuty)
{
	mMinuty = minuty;
}

int Czas::getSekundy() const
{
	return mSekundy;
}

void Czas::setSekundy(int sekundy)
{
	mSekundy = sekundy;
}

void Czas::wypisz()
{
	cout<<" godzina: "<<mGodziny<<":"<<mMinuty<<":"<<mSekundy<<endl;
}

void Czas::przesunGodzine(int g)
{
	if((mGodziny+=g) < 24)
	{
		mGodziny+=g;
	}
	else
	{
		(mGodziny+=g)%=24;
	}
}

void Czas::przesunSekundy(int s)
{
	if((mSekundy+=s) < 61)
	{
		mSekundy+=s;
	}
	else
	{
		int pomocnicza=0;
		mSekundy=(mSekundy+s)%60;
		pomocnicza=(mSekundy+s)/60;
		przesunMinuty(pomocnicza);
	}
}

void Czas::przesunMinuty(int m)
{
	if((mMinuty+=m) < 61)
	{
		mMinuty+=m;
	}
	else
	{
		int pomocnicza=0;
		mMinuty=(mMinuty+m)%60;
		pomocnicza=(mMinuty+m)/60;
		przesunGodzine(pomocnicza);
	}
}

Czas Czas::obliczRoznice(Czas& nowy)
{


	nowy.mSekundy=abs(nowy.mSekundy-mSekundy);
	nowy.mMinuty=abs(nowy.mMinuty-mMinuty);
	nowy.mGodziny=abs(nowy.mGodziny-mGodziny);

	return nowy;
}

DataCzas::DataCzas()
{
	mDzien=0;
	mMiesiac=0;
	mRok=0;
	mGodziny=0;
	mMinuty=0;
	mSekundy=0;

}
DataCzas::DataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok, int godziny, int minuty, int sekundy )
{
	mDzien=dzien;
	mMiesiac=miesiac;
	mRok=rok;
	mGodziny=godziny;
	mMinuty=minuty;
	mSekundy=sekundy;

}
DataCzas::DataCzas(Data nowa, Czas nowy)
{
//	nowa.mDzien=dzien;
//	nowa.mMiesiac=miesiac;
//	nowa.mRok=rok;
//	nowy.mGodziny=godziny;
//	nowy.Minuty=minuty;
//	nowy.mSekundy=sekundy;
}

	void DataCzas::wypisz()
	{
		cout<<"................"<<endl;
		cout << "Data: "<< mDzien<<"."<<mMiesiac<<"."<<mRok<<endl;
		cout<<" godzina: "<<mGodziny<<":"<<mMinuty<<":"<<mSekundy<<endl;
		cout<<"................"<<endl;
	}

	void DataCzas::przesun(DataCzas nowa)
	{

	}



