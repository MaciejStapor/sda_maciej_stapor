/*
 * zad4.hpp
 *
 *  Created on: 28.03.2017
 *      Author: RENT
 */

#ifndef ZAD4_HPP_
#define ZAD4_HPP_

class Data
{
public:
	unsigned int mDzien;
	unsigned int mMiesiac;
	unsigned int mRok;
public:
	Data();

	Data(unsigned int dzien, unsigned int miesiac, unsigned int rok);
	unsigned int getDzien() const;
	void setDzien(unsigned int dzien);
	unsigned int getMiesiac() const;
	void setMiesiac(unsigned int miesiac);
	unsigned int getRok() const;
	void setRok(unsigned int rok);
	void wypisz();
	void przesunDzien(int d);
	void przesunMiesiac(int d);
	void przesunRok(int d);
	Data roznica(Data nowa);
};

class Czas
{
protected:
	int mGodziny, mMinuty, mSekundy;
public:
	Czas();

	Czas(int godziny, int minuty, int sekundy);
	int getGodziny() const;

	void setGodziny(int godziny);
	int getMinuty() const;

	void setMinuty(int minuty);

	int getSekundy() const;

	void setSekundy(int sekundy);

	void wypisz();

	void przesunGodzine(int g);

	void przesunSekundy(int s);

	void przesunMinuty(int m);

	Czas obliczRoznice(Czas& nowy);


};

class DataCzas : public Data, public Czas
{
public:

	DataCzas();
	DataCzas(unsigned int dzien, unsigned int miesiac, unsigned int rok, int godziny, int minuty, int sekundy );
	DataCzas(Data nowa, Czas nowy);

	void wypisz();
	void przesun(DataCzas nowa);
};




#endif /* ZAD4_HPP_ */
