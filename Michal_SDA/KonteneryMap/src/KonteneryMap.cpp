//============================================================================
// Name        : KonteneryMap.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <utility>
#include <map>

using namespace std;

int main() {


	pair<string, int> ocena("max", 2);
	pair<string, int> ocena2;
	ocena2=make_pair("chris", 6);


	map<int, string> pracownik;

	pracownik.insert(pair<int, string>(1, "tomek"));
	pracownik.insert(pair<int, string>(2, "grazyna"));
	pracownik.insert(pair<int, string>(3, "hela"));

	for(map<int, string>::iterator i=pracownik.begin(); i!=pracownik.end(); i++)
	{
		cout<<"nr. "<<i->first<<", imie: "<<i->second<<endl;
	}


	map<int, string>::iterator i=pracownik.find(3);
	cout<<"find   nr. "<<i->first<<", imie: "<<i->second<<endl;


	return 0;
}
