//============================================================================
// Name        : KonteneryZad6.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

bool czyPalindromReverse(string napis)
{
	vector<char> nowy;
	for(unsigned int i = 0; i < napis.size(); i++)
	{
		nowy.push_back(napis[i]);
	}

	vector<char> odwrocony = nowy;
	reverse(odwrocony.begin(), odwrocony.end());
	vector<char>::iterator itk = odwrocony.begin();

	for(vector<char>::iterator it = nowy.begin(); it != nowy.end(); it++, itk++)
	{
		if(*it != *itk)
			return false;
	}

	return true;
}

bool czyPalindromEqual(string napis)
{
	vector<char> nowy;
	for(unsigned int i = 0; i < napis.size(); i++)
	{
		nowy.push_back(napis[i]);
	}

	vector<char> odwrocony = nowy;
	reverse(odwrocony.begin(), odwrocony.end());

	return (equal(nowy.begin(), nowy.end(), odwrocony.begin())?true:false);
}

int main() {

	string napis = "ala ma kota";
	cout<<"Funkcja revers: "<< (czyPalindromReverse(napis)?"napis jest palindromem":"napis nie jest palindromem")<<endl;

	string napis2 = "ikar lapal raki";
	cout<<"Funkcja revers: "<< (czyPalindromReverse(napis2)?"napis jest palindromem":"napis nie jest palindromem")<<endl;

	cout<<"Funkcja equal: "<< (czyPalindromEqual(napis)?"napis jest palindromem":"napis nie jest palindromem")<<endl;

	cout<<"Funkcja equal: "<< (czyPalindromReverse(napis2)?"napis jest palindromem":"napis nie jest palindromem")<<endl;


	return 0;
}
