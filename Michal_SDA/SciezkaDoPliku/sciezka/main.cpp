/*
 * main.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include <iostream>
#include "zad.hpp"

using namespace std;

int main()
{

	ZadaniaNaPliku plik(ZadaniaNaPliku::otwarty);

	plik.otworz();
	plik.pobierz();
	plik.zamknij();

	return 0;
}


