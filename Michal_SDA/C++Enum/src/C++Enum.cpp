//============================================================================
// Name        : C++Enum.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

enum class kolorKota
{
	czarny,
	bialy,
	czerwony,
	zielony
};

enum class kolorPsa
{
	czarny,
	bialy,
	szary,
	fioletowy
};


int main() {

	kolorKota kot = kolorKota::czarny;

	kolorPsa pasa = kolorPsa::bialy;

	return 0;
}
