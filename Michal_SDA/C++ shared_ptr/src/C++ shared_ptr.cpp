//============================================================================
// Name        : C++.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <memory>
using namespace std;

class Kurczaczek
{
	string mName;
public:
	Kurczaczek(string name)
	:mName(name)
	{

	}

	void pokaz()
	{
		cout<<"name: "<<mName<<endl;
	}
};

int main() {

	vector<shared_ptr<Kurczaczek>> vecKurczaczek;

//	Kurczaczek jeden("ala");
	shared_ptr<Kurczaczek> jeden = make_shared<Kurczaczek>("ala");

	for(int i = 0; i < 5; i++)
	{
		vecKurczaczek.push_back(jeden);
		cout<<"count: "<<jeden.use_count()<<endl;
	}

	vecKurczaczek[0].reset();

	cout<<"count po: "<<jeden.use_count()<<endl;

	vecKurczaczek.clear();

	cout<<"count po: "<<jeden.use_count()<<endl;

	return 0;
}
