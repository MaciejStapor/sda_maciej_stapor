//============================================================================
// Name        : KonteneryWczytywanieZPliku.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <list>
#include <algorithm>

using namespace std;

class Osoba
{
	string mImie;
	string mNazwizko;
	int mWiek;
public:
	Osoba():mImie(0), mNazwizko(0), mWiek(0)
	{

	}
	Osoba(string imie, string nazwisko, int wiek)
	: mImie(imie)
	, mNazwizko(nazwisko)
	, mWiek(wiek)
	{

	}

	void pokaz ()
	{
		cout<<"Imie: "<<mImie<<"\nNazwisko: "<<mNazwizko<<"\nWiek: "<<mWiek<<"\n............."<<endl;
	}

	int getWiek() const {
		return mWiek;
	}
};


struct SredniaWieku
{
	float mSuma;
	int mRozmiarListy;
	int mLicznik;
	SredniaWieku(int liczba): mSuma(0), mRozmiarListy(liczba), mLicznik(1)
	{
	}

	void operator() (Osoba& nowa)
	{
		if(mRozmiarListy>mLicznik)
		{
			mSuma+=nowa.getWiek();
			mLicznik++;
		}
		else
		{
			mSuma+=nowa.getWiek();
			mSuma=mSuma/mLicznik;
		}
	}
};

int main() {

	fstream plik;
	plik.open("imiona.txt", ios::in);

	if(!plik.good())
	{
		cout<<"Nie udalo sie otworzyc pliku!\nKoniec programu!"<<endl;
		return -1;
	}

	list<Osoba> listaOsob;

	string imie;
	string nazwisko;
	int wiek;

	while(!plik.eof())
	{
		plik>>imie>>nazwisko>>wiek;
		listaOsob.push_back(Osoba(imie, nazwisko, wiek));
	}

	for(list<Osoba>::iterator it=listaOsob.begin(); it != listaOsob.end(); it++)
	{
		it->pokaz();
	}


	SredniaWieku suma=for_each(listaOsob.begin(), listaOsob.end(), SredniaWieku(listaOsob.size()));

	cout<<"Srednia wieku: "<<suma.mSuma<<endl;


	plik.close();



	return 0;
}
