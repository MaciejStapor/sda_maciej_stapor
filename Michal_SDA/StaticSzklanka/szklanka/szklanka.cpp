#include "szklanka.hpp"

int Szklanka::mCena=0;

int Szklanka::getCena() {
	return mCena;
}

void Szklanka::setCena( int cena) {
	mCena = cena;
}

Szklanka::Szklanka(int pojemnosc)
:mPojemnosc(pojemnosc)
{

}

int Szklanka::getPojemnosc() const {
	return mPojemnosc;
}

void Szklanka::setPojemnosc(int pojemnosc) {
	mPojemnosc = pojemnosc;
}
