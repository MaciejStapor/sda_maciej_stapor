#include <iostream>
#include "szklanka.hpp"


using namespace std;

static void pokaz()
{
	cout<<"Funkcja pokaz"<<endl;
}

int main()
{

	cout<<"cena bez obiektu "<<Szklanka::getCena()<<endl;

	Szklanka::setCena(123);

	cout<<"cena bez obiektu ale po zmianie zmiennej static "<<Szklanka::getCena()<<endl;


	Szklanka a(13);
	Szklanka b(28);

	cout<<"przed zmiana zmiennej static,  a: "<<a.getCena()<<" b: "<<b.getCena()<<endl;
	b.setCena(33);
	cout<<"po zmianie zmiennej static,  a: "<<a.getCena()<<" b: "<<b.getCena()<<endl;

	pokaz();

	return 0;
}



