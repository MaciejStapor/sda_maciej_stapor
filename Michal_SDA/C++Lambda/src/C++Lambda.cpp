//============================================================================
// Name        : C++Lambda.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class wieksze
{
	int mLiczba;
public:
	wieksze(int x)
: mLiczba(x)
{

}
	void operator()(int one)
	{
		if(one > mLiczba)
			cout<<one<<" ";
	}
};

bool wiekszeNiz4(int& one)
{
	return one > 4;
}

int main() {

	vector<int> vecInt;

	for(int i = 1; i < 101; i++)
		vecInt.push_back(i);

	int tmp = 20;
	int count = 0;
	int dzielnik = 5;

	for_each(vecInt.begin(), vecInt.end(), [](int& one){cout<<one<<" ";});
	cout<<endl;

	for_each(vecInt.begin(), vecInt.end(), [tmp](int& one){if(one > tmp)cout<<one<<" ";});
	cout<<endl;
	for_each(vecInt.begin(), vecInt.end(), wieksze(20));
	cout<<endl;

	for_each(vecInt.begin(), vecInt.end(), [&count](int& one){if(one > 4)count++;});
	cout<<count<<endl;

	cout<<count_if(vecInt.begin(), vecInt.end(), wiekszeNiz4);
	cout<<endl;

	for_each(vecInt.begin(), vecInt.end(), [dzielnik](int& one){if(one % dzielnik == 0)cout<<one<<" ";});
	cout<<endl;

	int sum = 0;
	for_each(vecInt.begin(), vecInt.end(), [&sum](int& one){if(one % 2 == 0)sum += one;});
	cout<<sum<<endl;

	sort(vecInt.begin(), vecInt.end(), [](int& one, int& second){return one > second;});
	for_each(vecInt.begin(), vecInt.end(), [](int& one){cout<<one<<" ";});
	cout<<endl;

	return 0;
}
