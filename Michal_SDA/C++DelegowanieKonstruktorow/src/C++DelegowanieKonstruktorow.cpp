//============================================================================
// Name        : C++DelegowanieKonstruktorow.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include  <vector>
using namespace std;

class KontoBankowe
{
	string mImieWlasciciela;
	float mStanKonta;
	float mOprocentowanie;
public:
	KontoBankowe(string imie, float stanKonta, float oprocentowanie)
	:mImieWlasciciela(imie)
	,mStanKonta(stanKonta)
	,mOprocentowanie(oprocentowanie)
	{

	}

	KontoBankowe(string imie)
	:KontoBankowe(imie, 0, 0)
	{

	}

	KontoBankowe()
	:KontoBankowe("", 100, 0)
	{

	}

	void pokaz()
	{
		cout<<mImieWlasciciela<<", stan konta "<<mStanKonta<<"zl., oprocentowanie "<<mOprocentowanie<<"%"<<endl;
	}


};

class KontoBankoweVIP: public KontoBankowe
{
public:
	using KontoBankowe::KontoBankowe;

	void bezKolejki()
	{
		cout<<"..."<<endl;
	}
};

int main() {

	KontoBankowe konto1("Maciej", 3000.01f, 0.5f);
	KontoBankowe konto2("Karol");
	KontoBankowe konto3;

	konto1.pokaz();
	konto2.pokaz();
	konto3.pokaz();

	KontoBankoweVIP konto4("Ania", 324000.01f, 0.9f);
	KontoBankoweVIP konto5("Paulina");
	KontoBankoweVIP konto6;

	konto4.pokaz();
	konto5.pokaz();
	konto6.pokaz();

	vector<int> nowy = {2, 3, 4, 5, 6, 120};

	for(auto& one : nowy)
		cout<<one<<" ";
	cout<<endl;

	vector<KontoBankowe> nowy1 {{"Maciej", 663000.01f, 0.5f}, {"Karol", 223000.01f, 0.9f}, {"adam", 303400.01f, 0.25f}};

		for(auto& one : nowy1)
			one.pokaz();
		cout<<endl;

	return 0;
}
