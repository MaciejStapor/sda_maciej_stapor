#include <iostream>
#include <iostream>
#include <exception>

using namespace std;

//enum AircraftError
//{
//  WingsOnFire = 1,
//  WingBroken = 2,
//  NoRunway = 3,
//  Crahed = 4
//};

class AircraftException : public exception
{
public:
	AircraftException(const char* errMessage) :m_ErrMessage(errMessage)
{}
	// overriden what() method from exception class
	const char* what() const throw() { return m_ErrMessage; }



private:
	const char* m_ErrMessage;

};

class WingsOnFire: public AircraftException
{
public:
	WingsOnFire(const char* errMessage):AircraftException(errMessage)
{

}
};

class WingBroken: public AircraftException
{
public:
	WingBroken(const char* errMessage):AircraftException(errMessage)
{

}
};
class NoRunway: public AircraftException
{
public:
	NoRunway(const char* errMessage):AircraftException(errMessage)
{

}
};
class Crahed: public AircraftException
{
public:
	Crahed(const char* errMessage):AircraftException(errMessage)
{

}
};

int main() {
	try
	{
		throw NoRunway("NoRunway");
	}

	catch (WingsOnFire& e)
	{
		cout<<e.what()<<endl;
	}
	catch (WingBroken& e)
	{
		cout<<e.what()<<endl;
	}
	catch (NoRunway& e)
	{
		cout<<e.what()<<endl;
	}
	catch (Crahed& e)
	{
		cout<<e.what()<<endl;
	}
	catch (AircraftException& e)
	{
		cout<<e.what()<<endl;
	}
	return 0;
}
