/*
 * DziekieZwierzeta.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */
#include <iostream>

using namespace std;

#ifndef DZIEKIEZWIERZETA_HPP_
#define DZIEKIEZWIERZETA_HPP_

class DzikieZwierzeta
{
protected:
	string mImie;
public:
	virtual void dajGlos()=0;
	virtual ~DzikieZwierzeta() {}

};



#endif /* DZIEKIEZWIERZETA_HPP_ */
