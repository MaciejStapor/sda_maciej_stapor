/*
 * pies.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */
#include <iostream>

#include "DzikieZwierzeta.hpp"
using namespace std;

#ifndef PIES_HPP_
#define PIES_HPP_

class Pies: virtual public DzikieZwierzeta
{
protected:
	string mImie;
public:
	Pies();
	Pies(string x);

	void dajGlos();
};



#endif /* PIES_HPP_ */
