/*
 * KotoPies.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */
#include <iostream>
#include "Kot.hpp"
#include "pies.hpp"

using namespace std;


#ifndef KOTOPIES_HPP_
#define KOTOPIES_HPP_

class KotoPies: public Kot, public Pies
{
public:
	void dajGlos();
};



#endif /* KOTOPIES_HPP_ */
