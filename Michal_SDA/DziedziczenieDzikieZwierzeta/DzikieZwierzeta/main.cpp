/*
 * main.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#include <iostream>

#include "DzikieZwierzeta.hpp"
#include "Kot.hpp"
#include "pies.hpp"
#include "KotoPies.hpp"


using namespace std;

int main()
{

	Kot kot("burek");

	Pies pies("azor");

	kot.dajGlos();
	pies.dajGlos();


	DzikieZwierzeta *tab[5];

	tab[0]=new Pies("mikus");
	tab[1]=new Kot("MAX");
	tab[2]=new Pies("choco");
	tab[3]=new Kot("klema");
	tab[4]=new KotoPies;

	for(int i=0; i<5; i++)
	{
		tab[i]->dajGlos();
	}

	for(int i=0; i<5; i++)
	{
		delete tab[i];
	}



	return 0;
}


