/*
 * Kot.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */
#include <iostream>

#include "DzikieZwierzeta.hpp"
using namespace std;

#ifndef KOT_HPP_
#define KOT_HPP_

class Kot: virtual public DzikieZwierzeta
{
protected:
	string mImie;
public:
	Kot();
	Kot(string x);

	void dajGlos();
};




#endif /* KOT_HPP_ */
