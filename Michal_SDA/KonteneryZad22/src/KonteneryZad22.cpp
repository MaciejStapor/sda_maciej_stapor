//============================================================================
// Name        : KonteneryZad22.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <list>
#include <algorithm>
#include "dog.hpp"
#include "bird.hpp"
using namespace std;



void wypisz(Animal* nowa)
{
	nowa->whoAreYou();
}

void pokaz(list<Animal*> nowa)
{
	for_each(nowa.begin(), nowa.end(), wypisz);
}

void usun(Animal* nowa)
{
	delete nowa;
}




int main() {
	list<Animal*> listaAnimal;
	srand(time(NULL));

	int x = 0;
	for(int i = 0; i < 20; i++)
	{
		int randInt = rand()%2;
		x++;
		if(randInt == 1)
		{
			listaAnimal.push_back(new Dog(x));
		}
		else
		{
			listaAnimal.push_back(new Bird(x));
		}
	}

	list<Dog*> listDog;
	list<Bird*> listBird;

	Dog* pies;

	for(list<Animal*>::iterator it = listaAnimal.begin(); it != listaAnimal.end(); it++)
	{
		pies = dynamic_cast<Dog*>(*it);
		if(pies != 0)
			listDog.push_back(dynamic_cast<Dog*>(*it));
		else
			listBird.push_back(dynamic_cast<Bird*>(*it));
	}

	cout<<"Dog list: "<<endl;
	for_each(listDog.begin(), listDog.end(), wypisz);
	cout<<"Bird list: "<<endl;
	for_each(listBird.begin(), listBird.end(), wypisz);


	for_each(listaAnimal.begin(), listaAnimal.end(), usun);




	return 0;
}
