//============================================================================
// Name        : C++Regexp.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <regex>
#include <cassert>
using namespace std;

int main() {

	string mail;

	regex mail_regex("[a-zA-Z0-9]+@[a-z]+.[com|pl]*$");

	while(cin>>mail)
	{
		cout<< (regex_search(mail, mail_regex) ? "Mail poprawny" : "Mail nie poprawny")<<endl;
	}

	return 0;
}
