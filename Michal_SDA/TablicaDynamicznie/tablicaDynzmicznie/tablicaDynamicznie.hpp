/*
 * tablicaDynamicznie.hpp
 *
 *  Created on: 17.05.2017
 *      Author: Admin
 */

#ifndef TABLICADYNAMICZNIE_HPP_
#define TABLICADYNAMICZNIE_HPP_
#include <iostream>
#include <sstream>
#include "pusta.hpp"

struct Osoba
{
	std::string mImie;
	int mWiek;
	void pokazOsobe()
	{
		std::cout<<"Imie: "<<mImie<<", wiek: "<<mWiek<<std::endl;
	}
};

template <typename typ>

class TablicaDynamicznie
{

	unsigned int mRozmiar;
	typ** mTablica;

public:

	TablicaDynamicznie():mRozmiar(0), mTablica(0)
{

}
	~TablicaDynamicznie()
	{
		wyczysc();
	}
	void wyczysc()
	{
		if(mRozmiar == 0)
		{
			std::cout<<"Tablica pusta!"<<std::endl;
		}
		else
		{
			for(unsigned int i=0; i<mRozmiar; i++)
			{
				delete mTablica[i];
			}
			delete[] mTablica;
			mRozmiar=0;
			mTablica=0;
		}
	}
	void zwiekszRozmiar()
	{
		if(mTablica == 0)
		{
			mRozmiar++;
			mTablica=new typ*[mRozmiar-1];
		}
		else
		{
			mRozmiar++;
			typ** tymczasowa = new typ*[mRozmiar];

			for(unsigned int i=0; i<mRozmiar-1; i++)
			{
				tymczasowa[i]=mTablica[i];
			}

			delete[] mTablica;
			mTablica=tymczasowa;
		}
	}
	void zmniejszRozmiar(unsigned int x, typ nowy)
	{
		if(mRozmiar == x)
		{
			wyczysc();
		}
		else
		{
			mRozmiar-=x;

			typ** tymczasowa = new typ*[mRozmiar];
			unsigned int k = 0;
			for(unsigned int i=0; i<mRozmiar+x; i++)
			{
				if(*mTablica[i]==nowy)
				{
					continue;
				}
				else
				{
					tymczasowa[k]=mTablica[i];
					k++;
				}
			}
			delete[] mTablica;
			mTablica=tymczasowa;
		}
	}

	void dodaj(typ nowy)
	{
		zwiekszRozmiar();
		mTablica[mRozmiar-1]=new typ(nowy);
	}

	friend void operator<<(std::ostream& os, Osoba nowy);

	void pokaz()
	{
		if(mRozmiar==0)
		{
			throw PustaTablica();
		}
		else
		{
			std::cout<<" "<<std::endl;
			for(unsigned int i=0; i<mRozmiar; i++)
			{
				std::cout<<"Pozycja z tablicy nr."<<i+1<<": ";
				std::cout<<*mTablica[i];
				std::cout<<std::endl;
			}
		}

	}

	std::string pokazMiejsce(typ nowy)
	{
		std::string miejsce=" ";
		for(unsigned int i=0; i<mRozmiar; i++)
		{
			if(*mTablica[i]==nowy)
			{
				std::stringstream ss;
				ss<<i+1;
				miejsce += ss.str();
				miejsce +=", ";
			}
		}

		return miejsce;
	}
	void pokazDane(int x)
	{
		std::cout<<"Dane z miejsca "<<x<<". ";
		std::cout<<*mTablica[x-1];
		std::cout<<std::endl;
	}

	void usunDany(typ nowy)
	{
		int licznik=0;

		for(unsigned int i=0; i<mRozmiar; i++)
		{
			if(*mTablica[i]==nowy)
			{
				licznik++;
			}
		}

		if(licznik==0)
		{
			throw PustaTablica();
		}
		else
		{
			zmniejszRozmiar(licznik, nowy);
		}

	}

	typ pobierz(int x)
	{
		return *mTablica[x-1];
	}

};

void operator<<(std::ostream& os, Osoba nowy)
{
	std::cout<<"Imie: "<<nowy.mImie<<", wiek: "<<nowy.mWiek;
}
bool operator==(Osoba jeden, Osoba dwa)
				{
	if(jeden.mImie==dwa.mImie && jeden.mWiek==dwa.mWiek)
	{
		return true;
	}
	else
	{
		return false;
	}
				}


#endif /* TABLICADYNAMICZNIE_HPP_ */
