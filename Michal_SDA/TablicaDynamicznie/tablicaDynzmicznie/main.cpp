/*
 * main.cpp
 *
 *  Created on: 17.05.2017
 *      Author: Admin
 */
#include "tablicaDynamicznie.hpp"

int main()
{
	Osoba osoba1={"Maciej", 29};
	Osoba osoba2={"Karol", 30};
	Osoba osoba3={"Paulina", 11};
	Osoba osoba4=osoba2;
	Osoba osoba5={"Kasia", 25};

	TablicaDynamicznie<Osoba> tablicaOsoba;

	tablicaOsoba.dodaj(osoba1);
	tablicaOsoba.dodaj(osoba2);
	tablicaOsoba.dodaj(osoba3);

	tablicaOsoba.pokaz();
	std::cout<<"Osoba: "<<std::endl;
	osoba4.pokazOsobe();
	std::cout<<"Znajduje sie na pozycji/ach: "<<tablicaOsoba.pokazMiejsce(osoba4)<<std::endl;
	tablicaOsoba.pokazDane(3);

	std::cout<<"..........................."<<std::endl;
	try
	{
		tablicaOsoba.usunDany(osoba5);
	}
	catch(PustaTablica& ex)
	{
		std::cout<<ex.what()<<std::endl;
	}

	tablicaOsoba.pokaz();
	std::cout<<"..........................."<<std::endl;

	TablicaDynamicznie<int> tablicaInt;

	tablicaInt.dodaj(18);
	tablicaInt.dodaj(56);
	tablicaInt.dodaj(2);
	tablicaInt.dodaj(23);
	tablicaInt.dodaj(5);
	tablicaInt.dodaj(2);
	tablicaInt.dodaj(778);



	tablicaInt.pokaz();
	std::cout<<"Numer 2 znajduje sie na miejscu/ach: "<<tablicaInt.pokazMiejsce(2)<<std::endl;
	tablicaInt.pokazDane(3);

	std::cout<<"..........................."<<std::endl;
	tablicaInt.usunDany(2);
	tablicaInt.pokaz();
	std::cout<<"..........................."<<std::endl;


	std::cout<<"Na pozycji 1 znajduje sie: "<<tablicaInt.pobierz(1)<<std::endl;



	return 0;
}



