/*
 * zad3.cpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */
#include "zad3.hpp"

#include <iostream>
#include <cmath>



using namespace std;


Czas::Czas(): mSekundy(0), mMinuty(0), mGodziny(0)
{

}

int
Czas::getGodziny () const
{
  return mGodziny;
}

void
Czas::setGodziny (int godziny)
{
  mGodziny = godziny;
}

int
Czas::getMinuty () const
{
  return mMinuty;
}

void
Czas::setMinuty (int minuty)
{
  if(minuty>=60){mMinuty=60;}
  else if(minuty<=1){mMinuty=1;}
  else {mMinuty=minuty;}
}

int
Czas::getSekundy () const
{
  return mSekundy;
}

void
Czas::setSekundy (int sekundy)
{
  if(sekundy>=60){mSekundy=60;}
  else if(sekundy<=1){mSekundy=1;}
  else {mSekundy=sekundy;}

}

Czas::Czas(int sekundy, int minuty, int godziny): mSekundy(sekundy), mMinuty(minuty), mGodziny(godziny)
{

}
void Czas::wypisz()
{
  cout<<"Godzina: "<<mGodziny<<":"<<mMinuty<<":"<<mSekundy<<endl;
}

void Czas::przesunGodziny(int x)
{
  mGodziny=(mGodziny+x)%24;

}
void Czas::przesunMinuty(int x)
{
  if(mMinuty+x<=60){mMinuty+=x;}
  else
    {
      int pom=0;
      mMinuty=(mMinuty+x)%60;
      pom=(mMinuty+x)/60;
      przesunGodziny(pom);
    }

}
void Czas::przesunSekundy(int x)
{
  if(mSekundy+x<=60){mSekundy+=x;}
  else
    {
      int pom=0;
      mSekundy=(mSekundy+x)%60;
      pom=(mSekundy+x)/60;
      przesunMinuty(pom);
    }
}

Czas Czas::roznica(Czas& nowa)
{
  nowa.mGodziny=abs(nowa.mGodziny-mGodziny);
  nowa.mMinuty=abs(nowa.mMinuty-mMinuty);
  nowa.mSekundy=abs(nowa.mSekundy-mSekundy);
  return nowa;
}


