/*
 * zad3.hpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */

#ifndef ZAD3_HPP_
#define ZAD3_HPP_

class Czas
{
  int mSekundy, mMinuty, mGodziny;
public:
  Czas();
  Czas(int sekundy, int minuty, int godziny);
  int  getGodziny () const;
  void  setGodziny (int godziny);
  int  getMinuty () const;
  void  setMinuty (int minuty);
  int  getSekundy () const;
  void  setSekundy (int sekundy);
  void wypisz();
  void przesunGodziny(int x);
  void przesunMinuty(int x);
  void przesunSekundy(int x);
  Czas roznica(Czas& nowa);
};



#endif /* ZAD3_HPP_ */
