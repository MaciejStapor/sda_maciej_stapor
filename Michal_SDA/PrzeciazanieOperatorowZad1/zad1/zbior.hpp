/*
 * zbior.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef ZBIOR_HPP_
#define ZBIOR_HPP_
#include <cstdlib>
#include "punkt.hpp"

class Zbior
{
	Punkt **mPtr;
	size_t mSize_t;
	void resize();
public:

	Zbior();
	~Zbior();
	void operator+(const Punkt& punkt);
	const Punkt** getPtr() const;
	void setPtr(const Punkt**& ptr);
	size_t getSizeT() const;
	void setSizeT(size_t sizeT);
	void wypisz();
	void operator+(const Zbior &nowy);
	Punkt operator[](size_t idx)const;
	operator bool();


};






#endif /* ZBIOR_HPP_ */
