/*
 * punkt.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "punkt.hpp"
#include <iostream>

Punkt::Punkt():mX(0),mY(0)
{

}

Punkt::Punkt(float x, float y): mX(x), mY(y)
{

}
Punkt::Punkt(const Punkt& nowy): mX(nowy.getX()), mY(nowy.getY())
{

}

Punkt Punkt::operator+(const float x) const
{
	Punkt punkt(0.0, 0.0);

	punkt.setX(this->mX+x);
	punkt.setY(this->mY+x);

	return punkt;
}

Punkt Punkt::operator*(const float x) const
{
	Punkt punkt(0.0, 0.0);

	punkt.setX(this->mX*x);
	punkt.setY(this->mY*x);

	return punkt;
}

float Punkt::getX() const {
	return mX;
}

void Punkt::setX(float x) {
	mX = x;
}

float Punkt::getY() const {
	return mY;
}

void Punkt::setY(float y) {
	mY = y;
}

Punkt Punkt::operator+(const Punkt &nowy)const
{
	Punkt punkt(0.0, 0.0);

	punkt.setX(nowy.getX()+this->getX());
	punkt.setY(nowy.getY()+this->getY());

	return punkt;

}

Punkt Punkt::operator*(const Punkt &nowy)const
{
	Punkt punkt(0.0, 0.0);

	punkt.setX(nowy.getX()*this->getX());
	punkt.setY(nowy.getY()*this->getY());

	return punkt;

}

Punkt operator+( float x , Punkt  &nowy)
{
	Punkt punkt(0.0, 0.0);

	punkt.setX(nowy.getX()+x);
	punkt.setY(nowy.getY()+x);

	return punkt;
}

Punkt operator*( float x , Punkt  &nowy)
{
	Punkt punkt(0.0, 0.0);

	punkt.setX(nowy.getX()*x);
	punkt.setY(nowy.getY()*x);

	return punkt;
}


void Punkt::wypisz()
{
	std::cout<<"X "<<mX<<" Y "<<mY<<std::endl;
}

Punkt& Punkt::operator++()
		{
	this->setX(++mX);
	this->setY(++mY);

	return *this;
		}

Punkt Punkt::operator++(int)
		{
	Punkt tmp(*this);

	this->operator++();

	return tmp;
		}
