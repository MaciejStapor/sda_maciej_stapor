/*
 * zbior.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "zbior.hpp"
#include "punkt.hpp"
#include <iostream>

Zbior::Zbior(): mPtr(0), mSize_t(0)
{

}


Zbior::~Zbior()
{
	if(mSize_t!=0)
	{
		for(size_t i=0; i<mSize_t; i++)
		{
			delete mPtr[i];
		}
		delete[] mPtr;
		mSize_t=0;
	}
}



size_t Zbior::getSizeT() const {
	return mSize_t;
}

void Zbior::setSizeT(size_t sizeT) {
	mSize_t = sizeT;
}

void Zbior::operator+(const Punkt& punkt)
{
	resize();
	mPtr[mSize_t-1] = new Punkt(punkt);
}

void Zbior::resize()
{
	if(mSize_t==0)
	{
		mPtr=new Punkt*[++mSize_t];
	}
	else
	{
		Punkt **tmp =new Punkt*[++mSize_t];

		for(size_t i=0; i<mSize_t-1; i++)
		{
			tmp[i]=mPtr[i];
		}

		delete[] mPtr;
		mPtr = tmp;
	}

}

void Zbior::wypisz()
{
	std::cout<<"zbior"<<std::endl;

	for(unsigned int i=0; i<mSize_t; i++)
	{
		mPtr[i]->wypisz();
	}
}

Punkt Zbior::operator[](size_t idx) const
{
	if(idx<mSize_t)
	{
		return Punkt(*mPtr[idx]);
	}
	else
	{
		return Punkt(0,0);
	}
}

void Zbior::operator+(const Zbior &nowy)
{
	for(unsigned int i=0; i<nowy.mSize_t; i++)
	{
		*this + nowy[i];
	}
}


Zbior::operator bool()
{
	return (this->getSizeT() !=0);
}
