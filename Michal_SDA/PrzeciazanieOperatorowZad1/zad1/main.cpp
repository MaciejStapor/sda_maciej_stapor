/*
 * main.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */


#include <iostream>
#include "punkt.hpp"
#include "zbior.hpp"


using namespace std;


int main()
{

	Punkt p1(2.1, 3.2);
	Punkt p2(4.2, 5.3);
	++p1;
	p1.wypisz();

	cout<<"dodawanie punktow"<<endl;
	Punkt p3=p1+p2;
	p3.wypisz();
	cout<<"mnozenie punktow"<<endl;
	Punkt p4=p1*p2;
	p4.wypisz();
	cout<<"dodawanie liczby do punktu"<<endl;
	Punkt p5=p1+42.24;
	p5.wypisz();
	cout<<"mnozenie przez liczbe punktu"<<endl;
	Punkt p6=p1*2.24;
	p6.wypisz();


	Zbior zbior;
	Zbior zbior2;

	zbior+p1;
	zbior+p2;

	zbior2+p3;
	zbior2+p4;

	zbior.wypisz();


	zbior+zbior2;

	zbior.wypisz();

	if(zbior)
	{
		cout<<"tak!"<<endl;
	}
	else
	{
		cout<<"nie!"<<endl;
	}



	return 0;
}
