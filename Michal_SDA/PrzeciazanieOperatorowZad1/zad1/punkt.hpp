/*
 * punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{
	float mX;
	float mY;

public:

	Punkt();
	Punkt(float, float);
	Punkt(const Punkt& nowy);
	Punkt operator+(const float x) const;
	Punkt operator+(const Punkt &nowy)const;
	Punkt operator*(const float x) const;
	Punkt operator*(const Punkt &nowy)const;
	float getX() const;
	void setX(float x);
	float getY() const;
	void setY(float y);
	void wypisz();
	Punkt& operator++();
	Punkt operator++(int);
};

Punkt operator+( float x , Punkt &nowy);
Punkt operator*( float x , Punkt &nowy);


#endif /* PUNKT_HPP_ */
