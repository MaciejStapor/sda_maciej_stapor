
#include <iostream>
#include "liczbyNieujemne.hpp"


int LiczbaNieujemna::getLiczba() const {
	return mLiczba;
}

void LiczbaNieujemna::setLiczba(int liczba) {
	mLiczba = liczba;
}


LiczbaNieujemna::LiczbaNieujemna(int x)
{
	if(x<0)
	{
		throw BladKonstrukcji();
	}

	mLiczba=x;
}


LiczbaNieujemna LiczbaNieujemna::operator-(const LiczbaNieujemna& liczba) const
{
	if((this->mLiczba-liczba.getLiczba())<0)
	{
		throw ZleArgumenty();
	}

	return LiczbaNieujemna(this->mLiczba-liczba.getLiczba());
}

LiczbaNieujemna LiczbaNieujemna::operator*(const LiczbaNieujemna& liczba) const
{
	return LiczbaNieujemna(this->mLiczba*liczba.getLiczba());
}

LiczbaNieujemna LiczbaNieujemna::operator/(const LiczbaNieujemna& liczba) const
{
	if(liczba.getLiczba()==0)
	{
		throw ZleArgumenty();
	}

	return LiczbaNieujemna(this->mLiczba/liczba.getLiczba());
}

const char* LiczbaNieujemna::what ()const throw()
{
	return"C++ Exception";
}

