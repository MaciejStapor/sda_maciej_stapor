/*
 * zlaArgumenty.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef ZLAARGUMENTY_HPP_
#define ZLAARGUMENTY_HPP_

#include <exception>

class ZleArgumenty: public std::exception
{
public:
	virtual const char* what ()const throw();
};



#endif /* ZLAARGUMENTY_HPP_ */
