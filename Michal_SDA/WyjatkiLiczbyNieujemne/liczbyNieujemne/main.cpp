/*
 * main.cpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#include <iostream>
#include "liczbyNieujemne.hpp"

using namespace std;

int main()
{
	try
	{
		LiczbaNieujemna a(12);
		LiczbaNieujemna b(11);

		cout<<"Liczba pierwsza "<<a.getLiczba()<<" ,liczba druga "<<b.getLiczba()<<endl;

		LiczbaNieujemna e(a*b);
		cout<<"Mnozenie: "<<e.getLiczba()<<endl;

		try
		{
			LiczbaNieujemna c(a-b);
			cout<<"Odejmowanie: "<<c.getLiczba()<<endl;
		}
		catch(ZleArgumenty& zle)
		{
			cout<<zle.what()<<endl;
		}
		try
		{
			LiczbaNieujemna d(a/b);
			cout<<"Dzielenie: "<<d.getLiczba()<<endl;
		}
		catch(ZleArgumenty& zle)
		{
			cout<<zle.what()<<endl;
		}
	}

	catch(BladKonstrukcji& arg)
	{
		cout<<arg.what()<<endl;
	}

	return 0;
}


