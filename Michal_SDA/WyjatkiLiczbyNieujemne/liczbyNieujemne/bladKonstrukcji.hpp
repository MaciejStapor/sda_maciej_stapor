/*
 * bladKonstrukcji.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef BLADKONSTRUKCJI_HPP_
#define BLADKONSTRUKCJI_HPP_
#include <exception>

class BladKonstrukcji: public std::exception
{

public:

	virtual const char* what ()const throw();
};



#endif /* BLADKONSTRUKCJI_HPP_ */
