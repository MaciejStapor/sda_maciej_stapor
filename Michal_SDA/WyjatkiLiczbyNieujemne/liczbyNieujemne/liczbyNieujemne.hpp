/*
 * liczbyNieujemne.hpp
 *
 *  Created on: 15.05.2017
 *      Author: RENT
 */

#ifndef LICZBYNIEUJEMNE_HPP_
#define LICZBYNIEUJEMNE_HPP_
#include <exception>
#include <iostream>
#include "zlaArgumenty.hpp"
#include "bladKonstrukcji.hpp"

class LiczbaNieujemna: public std::exception
{
	int mLiczba;

public:
	LiczbaNieujemna(int);
	int getLiczba() const;
	void setLiczba(int liczba);
	void pokaz();
	LiczbaNieujemna operator-(const LiczbaNieujemna& liczba) const;
	LiczbaNieujemna operator*(const LiczbaNieujemna& liczba) const;
	LiczbaNieujemna operator/(const LiczbaNieujemna& liczba) const;
	virtual const char* what ()const throw();

};



#endif /* LICZBYNIEUJEMNE_HPP_ */
