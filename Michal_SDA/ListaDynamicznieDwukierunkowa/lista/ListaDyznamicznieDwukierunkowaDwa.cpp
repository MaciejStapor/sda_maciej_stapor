//============================================================================
// Name        : ListaDyznamicznieDwukierunkowaDwa.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "lista.hpp"

using namespace std;

int main() {

Lista lista;

lista.pokaz();
lista.dodajPrzod("Maciej", "Stapor", 29);
lista.dodajPrzod("Bartosz", "Kraswdka", 23);
lista.dodajPrzod("Paulina", "Stapor", 21);

lista.dodajTyl("Irmina", "Nagartwska", 44);
lista.pokaz();

cout<<"...................................................usunKoniec"<<endl;
lista.usunKoniec();
lista.pokaz();
cout<<"...................................................usunPoczatek"<<endl;
lista.usunPoczatek();
lista.usunPoczatek();
lista.usunPoczatek();
lista.usunPoczatek();
lista.pokaz();

cout<<"...................................................usunListe"<<endl;
lista.dodajPrzod("Maciej", "Stapor", 29);
lista.dodajPrzod("Bartosz", "Kraswdka", 23);
lista.dodajPrzod("Paulina", "Stapor", 21);

lista.dodajTyl("Irmina", "Nagartwska", 44);
lista.usunListe();
lista.pokaz();

cout<<"...................................................pokazDany"<<endl;
lista.dodajPrzod("Maciej", "Stapor", 29);
lista.dodajPrzod("Bartosz", "Kraswdka", 23);
lista.dodajPrzod("Paulina", "Stapor", 21);

lista.dodajTyl("Irmina", "Nagartwska", 44);

lista.pokazDany(3);
lista.dodajDany("Adam", "Sakowske", 66, 3);
lista.dodajDany("Poczatek", "Poczatek", 66, 1);
lista.dodajDany("Koniec", "Koniec", 66, lista.getLicznik());
lista.pokaz();
cout<<"...................................................dodajDanyJakPusta"<<endl;
lista.usunListe();
lista.dodajDany("JakPusta", "JakPusta", 12, 33);
lista.dodajDany("CoTeraz", "CoTeraz", 12, 33);
lista.pokaz();
cout<<"...................................................usunDany"<<endl;
lista.dodajPrzod("Maciej", "Stapor", 29);
lista.dodajPrzod("Bartosz", "Kraswdka", 23);
lista.dodajPrzod("Paulina", "Stapor", 21);
lista.pokaz();
cout<<"----------------"<<endl;
lista.usunDany(3);
lista.pokaz();

	return 0;
}
