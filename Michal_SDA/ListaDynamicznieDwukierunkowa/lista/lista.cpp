/*
 * lista.cpp
 *
 *  Created on: 18.04.2017
 *      Author: Admin
 */

#include <iostream>
#include "lista.hpp"

using namespace std;


Lista::Lista(): poczatek(0), koniec(0), licznik(0)
{

}
Lista::~Lista()
{

}
Lista::Wezel::Wezel(): mImie("Brak"), mNazwisko("Brak"), mWiek(0), nastepny(NULL), poprzedni(NULL)
{

}

void Lista::dodajPrzod(string imie, string nazwisko, int wiek)
{
	if(poczatek!=0)
	{
		Wezel* nowy=new Wezel;
		nowy->mImie=imie;
		nowy->mNazwisko=nazwisko;
		nowy->mWiek=wiek;
		nowy->nastepny=poczatek;
		poczatek=nowy;
		nowy->nastepny->poprzedni=nowy;
		licznik++;

	}
	else
	{
		Wezel* nowy=new Wezel;
		nowy->mImie=imie;
		nowy->mNazwisko=nazwisko;
		nowy->mWiek=wiek;
		poczatek=nowy;
		koniec=nowy;
		licznik++;
	}

}
void Lista::dodajTyl(string imie, string nazwisko, int wiek)
{
	if(poczatek!=0)
	{
		Wezel* nowy=new Wezel;
		nowy->mImie=imie;
		nowy->mNazwisko=nazwisko;
		nowy->mWiek=wiek;
		nowy->poprzedni=koniec;
		nowy->poprzedni->nastepny=nowy;
		koniec=nowy;
		licznik++;


	}
	else
	{
		Wezel* nowy=new Wezel;
		nowy->mImie=imie;
		nowy->mNazwisko=nazwisko;
		nowy->mWiek=wiek;
		poczatek=nowy;
		koniec=nowy;
		licznik++;
	}
}
void Lista::pokaz()
{
	if(poczatek!=0)
	{
		Wezel* nowy=new Wezel;
		nowy=poczatek;

		while(nowy)
		{
			cout<<"Imie nazwisko: "<<nowy->mImie<<" "<<nowy->mNazwisko<<", ";
			cout<<"Wiek: "<<nowy->mWiek<<endl;
			nowy=nowy->nastepny;
		}
	}
	else
	{
		cout<<"Lista pusta"<<endl;
	}

}

void Lista::usunKoniec()
{
	if(licznik==1)
	{
		poczatek=0;
		poczatek=0;
		delete poczatek;
		licznik--;
	}
	else if(poczatek!=0)
	{
		Wezel *nowy=new Wezel;
		nowy=koniec;
		koniec->poprzedni->nastepny=0;
		koniec=koniec->poprzedni;
		delete nowy;
		licznik--;
	}
	else
	{
		cout<<"Lista pusta"<<endl;
	}

}
void Lista::usunPoczatek()
{
	if(licznik==1)
	{
		poczatek=0;
		koniec=0;
		delete poczatek;
		licznik--;
	}
	else if(poczatek!=0)
	{
		Wezel *nowy=new Wezel;
		nowy=poczatek;
		poczatek->nastepny->poprzedni=0;
		poczatek=poczatek->nastepny;
		delete nowy;
		licznik--;
	}
	else
	{
		cout<<"Lista pusta"<<endl;
	}
}

void Lista::usunListe()
{
	if(poczatek!=0)
	{
		while(poczatek)
		{
			Wezel *nowy=new Wezel;
			nowy=poczatek;
			poczatek=poczatek->nastepny;
			delete nowy;
		}
		licznik=0;
	}
	else
	{
		cout<<"lista pusta"<<endl;
	}
}

void Lista::pokazDany(int dany)
{
	if(poczatek!=0)
	{
		cout<<"Element "<<dany<<" to ";
		Wezel* nowy=new Wezel;
		nowy=poczatek;
		while(--dany)
		{
			nowy=nowy->nastepny;
		}
		cout<<"Imie nazwisko: "<<nowy->mImie<<" "<<nowy->mNazwisko<<", ";
		cout<<"Wiek: "<<nowy->mWiek<<endl;
	}
	else
	{
		cout<<"lista pusta"<<endl;
	}
}
void Lista::dodajDany(string imie, string nazwisko, int wiek, int miejsce)
{
	if(miejsce>licznik){miejsce=licznik;}
	if(miejsce<1){miejsce=1;}
	if(miejsce==1)
	{
		dodajPrzod(imie, nazwisko, wiek);
	}
	else if(miejsce==licznik)
	{
		dodajTyl(imie, nazwisko, wiek);
	}
	else if(poczatek!=0)
	{
		Wezel* nowy=new Wezel;
		Wezel* dodany=new Wezel;
		nowy=poczatek;
		while(--miejsce)
		{
			nowy=nowy->nastepny;
		}

		dodany->mImie=imie;
		dodany->mNazwisko=nazwisko;
		dodany->mWiek=wiek;

		nowy->poprzedni->nastepny=dodany;
		nowy->nastepny->poprzedni=dodany;
		dodany->poprzedni=nowy->poprzedni;
		dodany->nastepny=nowy;

		licznik++;

	}
	else
	{
		dodajPrzod(imie, nazwisko, wiek);
	}
}

int Lista::getLicznik() const {
	return licznik;
}

void Lista::usunDany(int dany)
{
	if(dany>licznik){dany=licznik;}
	if(dany<1){dany=1;}
	if(dany==1)
	{
		usunPoczatek();
	}
	else if(dany==licznik)
	{
		usunKoniec();
	}
	else if(poczatek!=0)
	{
		Wezel* nowy=new Wezel;
		Wezel* usun=new Wezel;
		nowy=poczatek;
		while(--dany)
		{
			nowy=nowy->nastepny;
		}
		usun=nowy;
		nowy->poprzedni->nastepny=nowy->nastepny;
		nowy->nastepny->poprzedni=nowy->poprzedni;
		delete usun;

		licznik--;

	}
	else
	{
		cout<<"lista pusta"<<endl;
	}
}
