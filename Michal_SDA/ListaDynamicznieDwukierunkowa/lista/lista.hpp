/*
 * lista.hpp
 *
 *  Created on: 18.04.2017
 *      Author: Admin
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_
#include <iostream>
using namespace std;


class Lista
{
	class Wezel
	{
	public:
		string mImie;
		string mNazwisko;
		int mWiek;
		Wezel* nastepny;
		Wezel* poprzedni;
		Wezel();

	};

	Wezel* poczatek;
	Wezel* koniec;
	int licznik;

public:
	Lista();
	~Lista();
	int getLicznik() const;
	void dodajPrzod(string imie, string nazwisko, int wiek);
	void dodajTyl(string imie, string nazwisko, int wiek);
	void pokaz();
	void usunKoniec();
	void usunPoczatek();
	void usunListe();
	void pokazDany(int dany);
	void usunDany(int dany);
	void dodajDany(string imie, string nazwisko, int wiek, int miejsce);

};



#endif /* LISTA_HPP_ */
