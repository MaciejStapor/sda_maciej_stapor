/*
 * Przyciaganie.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "Przyciaganie.hpp"
#include <cmath>

double Przyciaganie::getMasaObiektuDwa() const {
	return mMasaObiektuDwa;
}

void Przyciaganie::setMasaObiektuDwa(double masaObiektuDwa) {
	mMasaObiektuDwa = masaObiektuDwa;
}

double Przyciaganie::getMasaObiektuJeden() const {
	return mMasaObiektuJeden;
}

void Przyciaganie::setMasaObiektuJeden(double masaObiektuJeden) {
	mMasaObiektuJeden = masaObiektuJeden;
}

double Przyciaganie::getPromien() const {
	return mPromien;
}

void Przyciaganie::setPromien(double promien) {
	mPromien = promien;
}
Przyciaganie::Przyciaganie(): mMasaObiektuJeden(0), mMasaObiektuDwa(0),mObietosc(0), mPromien(0), mNazwa("")
{

}

void Przyciaganie::obietosc(double promien)
{
	mPromien=promien;
	mObietosc=(4/3)*3.14*mPromien*mPromien*mPromien;

}

const std::string& Przyciaganie::getNazwa() const {
	return mNazwa;
}

void Przyciaganie::setNazwa(const std::string& nazwa) {
	mNazwa = nazwa;
}

void Przyciaganie::masa(double odleglosc)
{
mMasaObiektuDwa=mObietosc*odleglosc;
}

double Przyciaganie::getObietosc() const {
	return mObietosc;
}

void Przyciaganie::setObietosc(double obietosc) {
	mObietosc = obietosc;
}

double Przyciaganie::silaOdzialywania(double masaObiektuJeden)
{
	long double g = 6.67 *pow(10, -11);

	return g*((masaObiektuJeden*mMasaObiektuDwa)/(pow(mPromien, 2)));
}
