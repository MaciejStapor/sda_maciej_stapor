/*
 * Przyciaganie.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef PRZYCIAGANIE_HPP_
#define PRZYCIAGANIE_HPP_
#include <string>
class Przyciaganie
{

	double mMasaObiektuJeden;
	double mMasaObiektuDwa;
	double mObietosc;
	double mPromien;
	std::string mNazwa;
public:

	Przyciaganie();
	double getMasaObiektuDwa() const;
	void setMasaObiektuDwa(double masaObiektuDwa);
	double getMasaObiektuJeden() const;
	void setMasaObiektuJeden(double masaObiektuJeden);
	double getPromien() const;
	void setPromien(double promien);
	void obietosc(double promien);
	void masa(double odleglosc);
	const std::string& getNazwa() const;
	void setNazwa(const std::string& nazwa);
	double silaOdzialywania(double masaObiektuJeden);
	double getObietosc() const;
	void setObietosc(double obietosc);
};



#endif /* PRZYCIAGANIE_HPP_ */
