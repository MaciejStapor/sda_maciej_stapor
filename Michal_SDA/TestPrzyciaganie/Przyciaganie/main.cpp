/*
 * main.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */


#include "Przyciaganie.hpp"
#include "gtest/gtest.h"


TEST(Konstruktor, Przyciaganie)
{
	Przyciaganie a;
	EXPECT_FLOAT_EQ(0.0, a.getMasaObiektuDwa());
	EXPECT_NEAR(0, a.getMasaObiektuJeden(), 0.002);
	EXPECT_EQ("", a.getNazwa());
	EXPECT_EQ(0, a.getPromien());
}

TEST(Przyciaganie, setNazwa)
{
	Przyciaganie a;
	a.setNazwa("Pluton");
	EXPECT_EQ("Pluton", a.getNazwa());
}


TEST(Przyciaganie, masa)
{
	Przyciaganie a;
	a.setObietosc(100);
	a.masa(10);
	EXPECT_NEAR(1000, a.getMasaObiektuDwa(), 0.003);
}

TEST(Przyciaganie, obietosc)
{
	Przyciaganie a;

	a.obietosc(100);
	EXPECT_NEAR(100, a.getPromien(), 0.003);
}



int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
