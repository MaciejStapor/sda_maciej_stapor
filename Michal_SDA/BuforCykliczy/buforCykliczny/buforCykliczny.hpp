/*
 * buforCykliczny.hpp
 *
 *  Created on: 17.05.2017
 *      Author: RENT
 */

#ifndef BUFORCYKLICZNY_HPP_
#define BUFORCYKLICZNY_HPP_
#include <cstdlib>
#include <exception>
#include <iostream>

template<typename Type>

class CircularBuffer
{
	struct Node
	{
		Type mValue;
		Node* mNext;
		Node(Type value):mValue(value), mNext(0)
		{

		}

	};

	Node* mReadPtr;
	Node* mAddPtr;

	size_t mSize;
	size_t mMaxSize;

public:
	CircularBuffer(size_t maxSize)
: mReadPtr(0)
, mAddPtr(0)
, mSize(0)
, mMaxSize(maxSize)
{

}

	void add(const Type& elem)
	{
		if(mSize==0)
		{
			Node* tmp=new Node(elem);
			mAddPtr=tmp;
			mAddPtr->mNext=tmp;
			mReadPtr=tmp;
			mSize++;
		}
		else if(mMaxSize>=mSize)
		{
			Node* tmp=new Node(elem);
			tmp=mAddPtr->mNext;
			mAddPtr->mNext=tmp;
			mAddPtr=tmp;
			mSize++;
		}
		else if(mMaxSize==mSize)
		{
			mAddPtr=mAddPtr->mNext;
			mAddPtr->mValue=elem;
		}
		else
		{
//			throw std::out_of_range();
		}
	}
	void clear();
	size_t maxSize()
	{
		return mMaxSize;
	}
	size_t size()
	{
		return mSize;
	}
	Type next()
	{
		if(mReadPtr == 0)
		{
			throw std::out_of_range();
		}
		else
		{
			mReadPtr = mReadPtr->mNext;
			return mReadPtr->mValue;
		}
	}

};



#endif /* BUFORCYKLICZNY_HPP_ */
