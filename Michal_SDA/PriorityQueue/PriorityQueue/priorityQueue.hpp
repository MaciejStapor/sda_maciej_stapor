/*
 * priorityQueue.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef PRIORITYQUEUE_HPP_
#define PRIORITYQUEUE_HPP_
#include <iostream>
#include <cstdlib>
#include <exception>


template<typename Type>

class PriorityQueue
{
	struct Node
	{
		Type mValue;
		Node* mNext;
		int mPriority;
		Node(Type value, int priority)
		: mValue(value)
		, mNext(0)
		, mPriority(priority)
		{

		}
	};

	Node* mHead;
	size_t mSize;
public:
	PriorityQueue()
	: mHead(0)
	, mSize(0)
  {

  }
	~PriorityQueue()
	{

	}

	void push(const Type& value, int priority)
	{
		if(mSize==0)
		{
			Node* newNode=new Node(value, priority);
			mHead=newNode;
			mSize++;
		}
		else
		{
			Node* newNode=mHead;
			size_t tmp=0;
			while(newNode->mPriority<priority)
			{
				tmp++;
				if(tmp==mSize)
				{
					break;
				}
				newNode=newNode->mNext;
			}

			Node* nodeToAdd=new Node(value, priority);
			nodeToAdd->mNext=newNode->mNext;
			newNode->mNext=nodeToAdd;
			mSize++;
		}
	}

	Type pop()
	{
		if(mSize==0)
		{
			throw std::out_of_range("pop: empty Queue!");
		}
		else
		{
			Node* tmp=mHead;
			mHead=mHead->mNext;
			Type value=tmp->mValue;
			delete tmp;
			mSize--;

			return value;
		}
	}
	size_t size()
	{
		return mSize;
	}

	bool empty()
	{
		return (mSize!=0) ? false : true;
	}

};



#endif /* PRIORITYQUEUE_HPP_ */
