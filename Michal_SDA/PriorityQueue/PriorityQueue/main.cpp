/*
 * main.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "priorityQueue.hpp"

int main()
{
	PriorityQueue<int> newQueue;

	newQueue.push(1, 1);
	newQueue.push(2, 2);
	newQueue.push(3, 3);
	newQueue.push(2, 2);
	newQueue.push(5, 5);
	newQueue.push(2, 2);
	newQueue.push(2, 2);
	newQueue.push(2, 2);

	size_t tmp=newQueue.size();
	try
	{
		for(size_t i=0; i<tmp; i++)
		{
			std::cout<<i+1<<". "<<newQueue.pop()<<std::endl;
//			newQueue.pop();
		}
	}
	catch(std::out_of_range& ex)
	{
		std::cout<<ex.what()<<std::endl;
	}




	return 0;
}


