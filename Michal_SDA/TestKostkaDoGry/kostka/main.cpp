/*
 * main.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "kostka.hpp"
#include "gtest/gtest.h"


TEST(Kostka, losuj)
{
	Kostka a;
	a.losuj();
	EXPECT_GT(7,a.getPole());
	EXPECT_LT(0,a.getPole());
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}



