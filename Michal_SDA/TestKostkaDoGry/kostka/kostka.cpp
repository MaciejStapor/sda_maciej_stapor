/*
 * kostaka.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "kostka.hpp"
#include <ctime>
#include <cstdlib>


Kostka::Kostka(): mPole(0)
{
	srand(time(NULL));
}

int Kostka::getPole() const {
	return mPole;
}

void Kostka::setPole(int pole) {
	mPole = pole;
}

int Kostka::losuj()
{
mPole=rand()%5+1;
return mPole;
}
