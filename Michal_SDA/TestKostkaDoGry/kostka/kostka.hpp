/*
 * kostka.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef KOSTKA_HPP_
#define KOSTKA_HPP_

class Kostka
{
	int mPole;
public:
	Kostka();
	int losuj();
	int getPole() const;
	void setPole(int pole);
};



#endif /* KOSTKA_HPP_ */
