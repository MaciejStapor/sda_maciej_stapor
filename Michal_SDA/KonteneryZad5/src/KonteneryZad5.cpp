//============================================================================
// Name        : KonteneryZad5.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
using namespace std;

int zliczWystapieniaPodanejLitery(string napis, char litera)
{
	int suma = count(napis.begin(), napis.end(), litera);

	if(litera >= 97)
		litera -= 32;
	else
		litera += 32;

	suma += count(napis.begin(), napis.end(), litera);

	return suma;
}

int main() {

	string napis = "Ala ma kota";
	char litera = 'a';

	cout<<"Literka: "<<litera<<", wystapila "<<zliczWystapieniaPodanejLitery(napis, litera)<<" razy w napisie: "<<napis<<endl;

	return 0;
}
