/*
 * kostka.h
 *
 *  Created on: 23.03.2017
 *      Author: RENT
 */

#ifndef KOSTKA_H_
#define KOSTKA_H_

class Kostka
{
private:
	unsigned int mIloscScian;
	unsigned int mLiczbaLosowa;
	unsigned int mWylosowana;
public:
	Kostka(unsigned int i);

	void losowanieFunkcja();
	void getWylosowanaFunkcja();
};




#endif /* KOSTKA_H_ */
