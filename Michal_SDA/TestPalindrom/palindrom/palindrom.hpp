/*
 * palindrom.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef PALINDROM_HPP_
#define PALINDROM_HPP_
#include <string>
class Palindrom
{
	int mLiczba;
	int mPrzejscia;
	std::string odwroc();
	std::string odwrocStringa(std::string nowy);
	int naInta(std::string nowa);
public:
	Palindrom();
	int getLiczba() const;
	void setLiczba(int liczba);
	std::string liczPalindrom(int liczba);
	int getPrzejscia() const;
	void setPrzejscia(int przejscia);
};



#endif /* PALINDROM_HPP_ */
