/*
 * palindrom.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "palindrom.hpp"
#include <string>

int Palindrom::getLiczba() const {
	return mLiczba;
}

void Palindrom::setLiczba(int liczba) {
	mLiczba = liczba;
}

Palindrom::Palindrom():mLiczba(0), mPrzejscia(0)
{

}

std::string Palindrom::odwroc()
{
	int pomocnicza=mLiczba;
	int tymczasowa=0;
	std::string nowy;

	do
	{
		tymczasowa=pomocnicza%10;
		nowy+=tymczasowa+'0';
		pomocnicza=pomocnicza/10;
	}while(pomocnicza!=0);

	return nowy;
}

std::string Palindrom::odwrocStringa(std::string nowy)
{
	std::string tym;

	for(int i=nowy.size()-1; i>=0; i--)
	{
		tym+=nowy[i];
	}

	return tym;
}

int Palindrom::naInta(std::string nowa)
{

	int tym=0;
	int mnoznik=1;

	for(unsigned int i=1; i<nowa.size(); i++)
	{
		mnoznik*=10;
	}

	for(unsigned int i=0; i<nowa.size(); i++, mnoznik/=10)
	{
		tym+=(nowa[i]-'0')*mnoznik;
	}

	return tym;
}

std::string Palindrom::liczPalindrom(int liczba)
{
	std::string odwrocona;
	std::string poprawna;
	int pom=-1;
	do
	{
		setLiczba(liczba);

		odwrocona=odwroc();
		poprawna=odwrocStringa(odwrocona);

		int liczbaOdwrocona = naInta(odwrocona);

		liczba=liczba+liczbaOdwrocona;
		pom++;
		setPrzejscia(pom);

	}while(odwrocona!=poprawna);


	return odwrocona;
}

int Palindrom::getPrzejscia() const {
	return mPrzejscia;
}

void Palindrom::setPrzejscia(int przejscia) {
	mPrzejscia = przejscia;
}
