/*
 * main.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "palindrom.hpp"

TEST(Konstruktor,Palindrom )
{
	Palindrom a;
	EXPECT_EQ(0, a.getLiczba());
	EXPECT_EQ(0, a.getPrzejscia());
}


TEST(Palindrom, setPrzejscia)
{
	Palindrom a;
	EXPECT_EQ("66", a.liczPalindrom(24));
}

TEST(Palindrom, geterySetery)
{
	Palindrom a;
	a.setLiczba(24);
	EXPECT_EQ(24, a.getLiczba());
	a.setPrzejscia(1);
	EXPECT_EQ(1, a.getPrzejscia());
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}


