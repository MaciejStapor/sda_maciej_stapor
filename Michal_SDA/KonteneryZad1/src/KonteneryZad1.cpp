//============================================================================
// Name        : KonteneryZad1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
using namespace std;

class KlasaSortujaca
{
public:
	bool operator() (char pierwszy, char drugi)
	{
		return pierwszy>drugi;
	}
};

bool funkcjaSortujaca(char& pierwszy, char& drugi)
{
	return pierwszy>drugi;
}

string pomieszajStringa(string& napis)
{
//	sort(napis.begin(), napis.end());
//	sort(napis.begin(), napis.end(), funkcjaSortujaca);
	sort(napis.begin(), napis.end(), KlasaSortujaca());
	return napis;
}


int main() {

	string napis = "Ala Ma Kota";
	cout<<"Napis przed sortowaniem: "<<napis<<endl;

	pomieszajStringa(napis);
	cout<<"Napis po sortowaniu: "<<napis<<endl;

	return 0;
}
