//============================================================================
// Name        : KonteneryIteratory.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
#include <deque>
#include <vector>
#include <iterator>
#include <list>

using namespace std;

void showVector(vector<int>& newVector)
{
	cout<<"vector [ ";
	for(vector<int>::iterator it = newVector.begin(); it != newVector.end(); advance(it, 1))
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;
}

void showList(list<float>& newList)
{
	cout<<"list [ ";
	for(list<float>::iterator it = newList.begin(); it != newList.end(); advance(it, 1))
	{
		cout<<*it<<", ";
	}
	cout<<"]"<<endl;
}

void printIntegerPart(float x)
{
	cout<<static_cast<int>(x)<<", ";
}

struct Functor
{
	float sum;
	Functor(): sum(0)
	{

	}
	void operator() (float x)
	{
		sum+=x;
	}
};



int main() {

	vector<int> small;
	small.push_back(1);
	small.push_back(2);
	small.push_back(3);
	small.push_back(4);
	small.push_back(5);


	vector<int> large;
	large.push_back(10);
	large.push_back(20);
	large.push_back(30);
	large.push_back(40);
	large.push_back(50);


//	back_insert_iterator< vector<int> > backIt(large);

	copy(small.rbegin(), small.rend(), back_inserter(large));

	showVector(large);

	cout<<"ostream iterator: [ ";
	copy(large.begin(), large.end(), ostream_iterator<int>(cout, ", "));
	cout<<"]"<<endl;

	list<float> floatList;

	floatList.push_back(2.34);
	floatList.push_back(23.1);
	floatList.push_back(2.98);
	floatList.push_back(2.34);
	floatList.push_back(3.12);
	floatList.push_back(-33.87);

	showList(floatList);


	cout<<"printIntegerPart: [ ";
	for_each(floatList.begin(), floatList.end(), printIntegerPart);
	cout<<"]"<<endl;

	Functor sum = for_each(floatList.begin(), floatList.end(), Functor());

	cout<<"Functor sum "<<sum.sum<<endl;

	return 0;
}
