//============================================================================
// Name        : C++move.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Produkt
{
	float mCena;
	string mNazwa;

public:
	Produkt(float cena, string nazwa)
	:mCena(cena)
	,mNazwa(nazwa)
	{

	}

	Produkt(float cena)
	:Produkt(cena, "")
	{

	}

	Produkt()
	:Produkt(0, "")
	{

	}

	void pokaz()
	{
		cout<<"Cena: "<<mCena<<", Nazwa "<<mNazwa<<endl;
	}
};

Produkt stworz(int klasa, string nazwa)
{
	return klasa == 1 ? Produkt(1000.99, nazwa) : (klasa == 2 ? Produkt(500, nazwa) : Produkt(19.99, nazwa));
}

Produkt stworzMmove(int klasa, string nazwa)
{
	Produkt tmp = klasa == 1 ? Produkt(1000.99, nazwa) : (klasa == 2 ? Produkt(500, nazwa) : Produkt(19.99, nazwa));
	return move(tmp);
}

int main() {

	Produkt produkt1 = stworz(1, "BMX");

	produkt1.pokaz();

	return 0;
}
