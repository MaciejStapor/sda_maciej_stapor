#include <iostream>

using namespace std;

void sortowanieBombelkowe (int* tab, int n)
{
    for(int j = 1; j<10 ; j++)
    {
        for(int i = n-1; i>=1 ; i--)
        {
            if(tab[i] < tab[i-1])
            {
                int pomocnicza;
                pomocnicza = tab[i-1];
                tab[i-1] = tab[i];
                tab[i] = pomocnicza;
            }
        }
    }
}

int main()
{
    int maks = 10;
    int tab[maks];
    tab[0] = 12;
    tab[1] = 35;
    tab[2] = 2;
    tab[3] = 78;
    tab[4] = 22;
    tab[5] = 45;
    tab[6] = 4;
    tab[7] = 3;
    tab[8] = 8;
    tab[9] = 15;


    cout << "[ ";
    for(int i=0; i<10; i++)
    {
        cout << " " << tab[i] << " ";
    }
    cout << " ]" << endl;

    sortowanieBombelkowe(tab, maks);

    cout<<"Posortowane"<<endl;
    cout << "[ ";
    for(int i=0; i<10; i++)
    {
        cout << " " << tab[i] << " ";
    }
    cout << " ]" << endl;


    return 0;
}
