/*
 * bmi.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "bmi.hpp"


KalkulatorKalor::KalkulatorKalor(): mWaga(0), mWzrost(0), mWiek(0)
{

}
double KalkulatorKalor::klakulatorBMI(double waga, double wzrost)
{
	mWaga=waga;
	mWzrost=wzrost;
	return mWaga/(mWzrost*mWzrost);
}

double KalkulatorKalor::getWaga() const {
	return mWaga;
}

void KalkulatorKalor::setWaga(double waga) {
	mWaga = waga;
}

int KalkulatorKalor::getWiek() const {
	return mWiek;
}

void KalkulatorKalor::setWiek(int wiek) {
	mWiek = wiek;
}

double KalkulatorKalor::getWzrost() const {
	return mWzrost;
}

void KalkulatorKalor::setWzrost(double wzrost) {
	mWzrost = wzrost;
}

double KalkulatorKalor::kalkulatorKalori(double waga, double wzrost, int wiek, Plec plec)
{
	mWaga=waga;
	mWzrost=wzrost;
	mWiek=wiek;

	switch(plec)
	{
	case mezczyzna :
		return 66.47+(13.7*mWaga)+(5*mWzrost)-(6.76*mWiek);
	case kobieta:
		return 655.1+(9.567*mWaga)+(1.85*mWzrost)-(4.68*mWiek);

	}

	return -1;

}

