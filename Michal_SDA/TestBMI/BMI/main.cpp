/*
 * main.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "bmi.hpp"
#include "gtest/gtest.h"


TEST(KalkulatorKalor,klakulatorBMI )
{
	KalkulatorKalor a;
	a.klakulatorBMI(97.2, 186);
	EXPECT_EQ(97.2, a.getWaga());
	EXPECT_EQ(186, a.getWzrost());
}

TEST(KalkulatorKalor,kalkulatorKalori )
{
	KalkulatorKalor a;
	a.kalkulatorKalori(97.2, 186.0, 26, KalkulatorKalor::mezczyzna);
	EXPECT_EQ(97.2, a.getWaga());
	EXPECT_EQ(186.0, a.getWzrost());
	EXPECT_EQ(26, a.getWiek());
}


int main(int argc, char **argv)
{

	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}
