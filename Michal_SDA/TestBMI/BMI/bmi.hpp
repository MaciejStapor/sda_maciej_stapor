/*
 * bmi.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef BMI_HPP_
#define BMI_HPP_

class KalkulatorKalor
{
	double mWaga;
	double mWzrost;
	int mWiek;
public:
	enum Plec
	{
		mezczyzna,
		kobieta
	};
	KalkulatorKalor();
	double klakulatorBMI(double waga, double wzrost);
	double kalkulatorKalori(double waga, double wzrost, int wiek, Plec plec);
	double getWaga() const;
	void setWaga(double waga);
	int getWiek() const;
	void setWiek(int wiek);
	double getWzrost() const;
	void setWzrost(double wzrost);
};




#endif /* BMI_HPP_ */
