/*
 * kolorowy.cpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */

#include <iostream>
#include <cmath>
#include "zad1.hpp"
#include "kolorowy.hpp"

using namespace std;


KolorowyPunkt::KolorowyPunkt(): Punkt(), mKolor(0)
{

}

void KolorowyPunkt::wypisz()
{
  Punkt::wypisz();
  switch(mKolor)
  {
    case bialy:
    cout<<" Kolor: Bialy "<<endl;
    break;
    case czarny:
        cout<<" Kolor: czarny "<<endl;
        break;
    case zielony:
        cout<<" Kolor: zielony "<<endl;
        break;
    default:
      cout << "Nie zdefiniowany kolor "<<endl;
  }
}

KolorowyPunkt::KolorowyPunkt(int x, int y, int kolor): Punkt(x, y), mKolor(kolor)
{

}
