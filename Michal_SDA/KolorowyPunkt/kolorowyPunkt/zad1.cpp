/*
 * zad1.cpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */

#include <iostream>
#include <cmath>
#include "zad1.hpp"
#include "kolorowy.hpp"

using namespace std;

Punkt::Punkt (): mX(0), mY(0)
{

}
Punkt::Punkt (int x, int y): mX(x), mY(y)
{

}

int
Punkt::getX () const
{
  return mX;
}

void
Punkt::setX (int x)
{
  if(mX+x<=0) {mX=0;}
  else if(mX+x>=300) {mX=300;}
  else {mX+=x;}
}

int
Punkt::getY () const
{
  return mY;
}

void
Punkt::setY (int y)
{
  if(mY+y<=0) {mY=0;}
  else if(mY+y>=300) {mY=300;}
  else {mY+=y;}

}

void Punkt::wypisz()
{
  cout<<endl<<"Wspolrzedne: ["<<mX<<", "<<mY<<"] ";
}

void Punkt::przesunX(int x)
{
  setX (x);
}
void Punkt::przesunY(int y)
{
  setY (y);
}

void Punkt::przesunXY(int x, int y)
{
  setX (x);
  setY (y);
}

void Punkt::przesunPunkt(Punkt& nowy)
{
  nowy.przesunXY(nowy.mX, nowy.mY);
}

double Punkt::obliczOdleglosc(int x, int y)
{
  double pomocniczaX = mX-x;
  double pomocniczaY = mY-y;
  return sqrt(pomocniczaX*pomocniczaX+pomocniczaY*pomocniczaY);

}
double Punkt::obliczOdleglosc(Punkt& nowy)
{
  double pomocniczaX = nowy.mX-mX;
  double pomocniczaY = nowy.mY-mY;
  return sqrt(pomocniczaX*pomocniczaX+pomocniczaY*pomocniczaY);
}


