/*
 * zad1.hpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */

#ifndef ZAD1_HPP_
#define ZAD1_HPP_
class Punkt
{
protected:
  int mX,mY;
public:
  Punkt ();
  Punkt (int x, int y);
  int  getX () const;
  void  setX (int x);
  int  getY () const;
  void  setY (int y);
  void wypisz();
  void przesunX(int x);
  void przesunY(int y);
  void przesunXY(int x, int y);
  void przesunPunkt(Punkt& nowy);
  double obliczOdleglosc(int x, int y);
  double obliczOdleglosc(Punkt& nowy);
};




#endif /* ZAD1_HPP_ */
