/*
 * kolorowy.hpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */
#include <iostream>
#include "zad1.hpp"
using namespace std;
#ifndef KOLOROWY_HPP_
#define KOLOROWY_HPP_

class KolorowyPunkt: public Punkt
{
  int mKolor;
public:
  KolorowyPunkt();
  KolorowyPunkt(int x, int y, int kolor);
  enum Kolor
  {
    bialy,
    czarny,
    zielony
  };

  void wypisz();
};




#endif /* KOLOROWY_HPP_ */
