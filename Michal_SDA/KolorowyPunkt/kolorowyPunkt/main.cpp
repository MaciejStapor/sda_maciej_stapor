/*
 * main.cpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */

#include <iostream>
#include "zad1.hpp"
#include "kolorowy.hpp"

using namespace std;

int main()
{
  Punkt punkt(3, 3);
  punkt.wypisz();

  punkt.przesunX(22);
  punkt.wypisz();

  punkt.przesunPunkt(punkt);
  punkt.wypisz();

cout<<endl<<"Odleglosc: "<<punkt.obliczOdleglosc(5, 9)<<endl;

  KolorowyPunkt kolorP(4, 8, KolorowyPunkt::zielony);
  kolorP.wypisz();




  return 0;
}


