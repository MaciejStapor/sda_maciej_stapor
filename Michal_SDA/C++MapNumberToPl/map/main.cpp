/*
 * main.cpp
 *
 *  Created on: 10.06.2017
 *      Author: Admin
 */
#include <iostream>
#include <map>
#include <unordered_map>

int main()
{
	std::map<int, std::string> mapSort;

	mapSort.insert(std::make_pair(4, "Cztery"));
	mapSort.insert(std::make_pair(6, "Szesc"));
	mapSort.insert(std::make_pair(2, "Dwa"));
	mapSort.insert(std::make_pair(1, "Jeden"));
	mapSort.insert(std::make_pair(5, "Piec"));
	mapSort.insert(std::make_pair(3, "Trzy"));
	mapSort.insert(std::make_pair(7, "Siedem"));
	mapSort.insert(std::make_pair(9, "Dziewiec"));
	mapSort.insert(std::make_pair(8, "Osiem"));

	for(auto& one : mapSort)
		std::cout<<one.second<<" ";
	std::cout<<std::endl;

	std::unordered_map<int, std::string> mapUnordered;

	mapUnordered.insert(std::make_pair(4, "Cztery"));
	mapUnordered.insert(std::make_pair(6, "Szesc"));
	mapUnordered.insert(std::make_pair(2, "Dwa"));
	mapUnordered.insert(std::make_pair(1, "Jeden"));
	mapUnordered.insert(std::make_pair(5, "Piec"));
	mapUnordered.insert(std::make_pair(3, "Trzy"));
	mapUnordered.insert(std::make_pair(7, "Siedem"));
	mapUnordered.insert(std::make_pair(9, "Dziewiec"));
	mapUnordered.insert(std::make_pair(8, "Osiem"));

	for(auto& one : mapUnordered)
		std::cout<<one.second<<" ";
	std::cout<<std::endl;
	return 0;
}



