#include <iostream>

using namespace std;
class Poczatek
{
protected:
	int mX;
public:
	Poczatek()
{
		mX=0;
		cout<<"Poczatek"<<endl;
}

	void przedstawSie()
	{
		cout<<" Witam jestem Pczatek"<<endl;
	}
	void zmienX()
	{
		mX=11;
	}
	void wypiszX()
	{
		cout<<"Poczatek zmienia x: "<<mX<<endl;
	}

	~Poczatek()
	{
		cout<<"~Poczatek"<<endl;
	}

};

class Srodek: public Poczatek
{
protected:
	int mX;
public:
	Srodek()
{
		mX=0;
		cout<<"Srodek"<<endl;
}

	void przedstawSie()
	{
		cout<<"Witam jestem Srodek"<<endl;
	}

	void zmienX()
	{
		Poczatek::mX=111;
		this->mX=222;
	}
	void wypiszX()
	{
		cout<<"Srodek zmienia x: "<<mX<<endl;
		cout<<"Poczatek x: "<<Poczatek::mX<<endl;
	}

	~Srodek()
	{
		cout<<"~Srodek"<<endl;
	}
};


class Koniec: public Srodek
{
protected:
	int mX;
public:
	Koniec()
{
		mX=0;
		cout<<"Koniec"<<endl;
}

	void przedstawSie()
	{
		cout<<"Witam jestem Koniec"<<endl;
	}

	void zmienX()
	{

		Srodek::Poczatek::mX=2222;
		Srodek::mX=1111;
		this->mX=33333;
	}
	void wypiszX()
	{
		cout<<"Srodek zmienia x:"<<mX<<endl;
		cout<<"Poczatek x: "<<Srodek::Poczatek::mX<<endl;
		cout<<"Srodek x: "<<Srodek::mX<<endl;
	}

	~Koniec()
	{
		cout<<"~Koniec"<<endl;
	}
};


int main() {

	Poczatek p;
	p.przedstawSie();
	p.zmienX();
	p.wypiszX();
	Srodek s;
	s.przedstawSie();
	s.zmienX();
	s.wypiszX();
	Koniec k;
	k.przedstawSie();
	k.zmienX();
	k.wypiszX();
	cout<<"++++++++++++++++++++++++++++++++++++++++"<<endl;
	p.wypiszX();




	return 0;
}
