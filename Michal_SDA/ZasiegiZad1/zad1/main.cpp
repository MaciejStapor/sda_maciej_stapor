/*
 * main.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include <iostream>


using namespace std;

int global = 20;

void zwieksz(int a);
void zmniejsz(int a);
void wypisz();


int main()
{
	wypisz();
	zwieksz(20);
	wypisz();
	zmniejsz(10);
	wypisz();

	return 0;
}


void zwieksz(int a)
{
	cout<<"Zwiekszam o "<<a;
	global+=a;
}

void zmniejsz(int a)
{
	cout<<"Zmniejszam o "<<a;
	global-=a;
}

void wypisz()
{
	cout<<" "<<global<<endl;;
}


