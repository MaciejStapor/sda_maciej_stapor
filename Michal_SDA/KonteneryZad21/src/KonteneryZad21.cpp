//============================================================================
// Name        : KonteneryZad21.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
#include <algorithm>
using namespace std;

void pokaz(list<int> nowa)
{
	for(list<int>::iterator it = nowa.begin(); it != nowa.end(); it++)
			cout<<*it<<" ";
	cout<<endl;
}

bool sortFunkacja(int& first, int& second)
{
	return second%2 < first%2;
}

bool szukajParzystej(int& first)
{
	return first%2 == 0;
}
int main() {

	list<int> doRozdzielenia;

	for(int i = 1; i < 20; i++)
		doRozdzielenia.push_back(i);

	doRozdzielenia.sort(sortFunkacja);
//	pokaz(doRozdzielenia);
	list<int>::iterator it = find_if(doRozdzielenia.begin(), doRozdzielenia.end(), szukajParzystej);

	list<int> parzyste;
	parzyste.splice(parzyste.begin(), doRozdzielenia, it, doRozdzielenia.end());

	pokaz(parzyste);
	pokaz(doRozdzielenia);

	return 0;
}
