/*
 * pralka.hpp
 *
 *  Created on: 05.04.2017
 *      Author: RENT
 */

#ifndef PRALKA_HPP_
#define PRALKA_HPP_

class Sensor
{
public:
	virtual bool check() = 0;
	virtual ~Sensor();
};

class DoorSensor: public Sensor
{
public:
	bool check();
};

class TempSensor: public Sensor
{
public:
	 bool check();
};

class WaterSensor: public Sensor
{
public:
	int mCurrentLevel;
	int mDesiredLevel;
	bool check();
};



class Machine
{
public:
	virtual void turnOn()=0;
	virtual void turnOff()=0;
	virtual ~Machine()
	{

	}
};

class Engine: public Machine
{
private:
	int mRotation;
public:
	void turnOn();
	void turnOff();
};



class Timer
{
private:
	int mValue;
	int mDuration;
public:
	void setDuration(int in);
	void start();
	int count();
	int getValue();
	int getDuration();
};

class WashOption
{
private:
	int mWashSelection;
public:
	int getwashSelection();
};

class WashingMachine: public Machine
{
public:
	int mWashTime;
	int mRinseTime;
	int mSpinTime;

	void mainWM();
	void wash();
	void rinse();
	void spin();
	void fill();
	void empty();
	void standardWash();
	void twiceRinse();

public:

public:
	void turnOn();
	void turnOff();
};

#endif /* PRALKA_HPP_ */
