/*
 * ser.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SER_HPP_
#define SER_HPP_
#include <iostream>

class Ser
{
protected:
	int mCena;
public:
	virtual void podajCene()
	{
		std::cout<<"Ser cena-"<<mCena<<std::endl;
	}
	Ser(int cena)
	: mCena(cena)
	{

	}
	Ser()
	: mCena(0)
	{

	}
	virtual ~Ser()
	{

	}

};



#endif /* SER_HPP_ */
