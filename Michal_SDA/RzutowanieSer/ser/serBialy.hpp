/*
 * serBialy.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SERBIALY_HPP_
#define SERBIALY_HPP_
#include "ser.hpp"

class SerBialy: public Ser
{

public:
	enum Zawartosc
	{
		polTusty,
		tusty,
		chudy
	};

	Zawartosc mZawartosc;

	virtual void podajCene()
	{
		switch(mZawartosc)
		{
		case polTusty:
			std::cout<<"SerBialy cena-"<<mCena<<", "<<"polTusty"<<std::endl;
			break;
		case tusty:
			std::cout<<"SerBialy cena-"<<mCena<<", "<<"tusty"<<std::endl;
			break;
		case chudy:
			std::cout<<"SerBialy cena-"<<mCena<<", "<<"chudy"<<std::endl;
			break;
		}

	}

	SerBialy(int cena, Zawartosc nowa)
	: Ser(cena)
	, mZawartosc(nowa)
	{

	}
	virtual ~SerBialy()
	{

	}

};



#endif /* SERBIALY_HPP_ */
