/*
 * serZolty.hpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#ifndef SERZOLTY_HPP_
#define SERZOLTY_HPP_
#include "ser.hpp"

class SerZolty: public Ser
{

	int mWiek;
public:
	virtual void podajCene()
	{
		std::cout<<"SerZolty cena-"<<mCena<<", wiek "<<mWiek<<std::endl;
	}
	SerZolty(int cena, int wiek)
	: Ser(cena)
	, mWiek(wiek)
	{

	}
	virtual ~SerZolty()
	{

	}

};



#endif /* SERZOLTY_HPP_ */
