/*
 * main.cpp
 *
 *  Created on: 20.05.2017
 *      Author: RENT
 */

#include "serBialy.hpp"
#include "serZolty.hpp"

void sortuj(Ser** tab, int rozmiar)
{
	SerBialy** bialaTab=new SerBialy*[rozmiar];
	SerZolty** zoltyTab=new SerZolty*[rozmiar];
	int ileBialych=0;
	int ileZoltych=0;

	for(int i=0; i<rozmiar; i++)
	{
		if(dynamic_cast<SerBialy*> (tab[i]))
		{
			bialaTab[ileBialych]=dynamic_cast<SerBialy*> (tab[i]);
			ileBialych++;
		}
		else if(dynamic_cast<SerZolty*> (tab[i]))
		{
			zoltyTab[ileZoltych]=dynamic_cast<SerZolty*> (tab[i]);
			ileZoltych++;
		}
	}
	for(int i=0; i<ileBialych; i++)
	{
		bialaTab[i]->podajCene();
	}

	std::cout<<"ileBialych: "<<ileBialych<<std::endl;
	for(int i=0; i<ileZoltych; i++)
	{
		zoltyTab[i]->podajCene();
	}
	std::cout<<"ileZoltych: "<<ileZoltych<<std::endl;

	delete[] bialaTab;
	delete[] zoltyTab;
}

int main()
{
	Ser* ser[6];
	ser[0]=new SerZolty(25, 3);
	ser[1]=new SerBialy(14, SerBialy::polTusty);
	ser[2]=new SerZolty(11, 5);
	ser[3]=new SerBialy(146, SerBialy::chudy);
	ser[4]=new SerZolty(22, 6);
	ser[5]=new SerBialy(17, SerBialy::tusty);


	sortuj(ser, 6);

	for(int i=0; i<6; i++)
		{
			delete ser[i];
		}

	return 0;
}



