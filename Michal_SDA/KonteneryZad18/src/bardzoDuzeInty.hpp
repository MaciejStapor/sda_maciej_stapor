/*
 * bardzoDuzeInty.hpp
 *
 *  Created on: 30.05.2017
 *      Author: RENT
 */

#ifndef BARDZODUZEINTY_HPP_
#define BARDZODUZEINTY_HPP_
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

class BardzoDuzeInty
{
	vector<int> bardzoDuzyInt;

	BardzoDuzeInty przepiszPozostale(vector<int>::reverse_iterator& itW, BardzoDuzeInty& tmp, BardzoDuzeInty& wiekszy)
	{
		while(itW != wiekszy.getBardzoDuzyInt().rend())
		{
			tmp.getBardzoDuzyInt().push_back(*itW);
			itW++;
		}
		return tmp;
	}

	BardzoDuzeInty dodajVec(BardzoDuzeInty& wiekszy,BardzoDuzeInty& mniejszy)
	{
		BardzoDuzeInty tmp;
		vector<int>::reverse_iterator itW = wiekszy.getBardzoDuzyInt().rbegin();
		vector<int>::reverse_iterator itM = mniejszy.getBardzoDuzyInt().rbegin();
		int suma = 0;

		while(itM != mniejszy.getBardzoDuzyInt().rend())
		{
			suma = *itW + *itM + suma;
			int dodaj = suma%10;
			tmp.getBardzoDuzyInt().push_back(dodaj);
			itW++;
			itM++;
			suma = suma / 10;
		}

		if(suma == 0)
		{
			return przepiszPozostale(itW, tmp, wiekszy);
		}
		else
		{
			while(suma != 0)
			{
				int tmpInt = suma%10;
				int dodaj = tmpInt + *itW;
				tmp.getBardzoDuzyInt().push_back(dodaj);
				suma = suma/10;
			}
			return przepiszPozostale(itW, tmp, wiekszy);
		}
	}

public:

	void StringNaVector(string napis)
	{
		while(napis.size() != 0)
		{
			bardzoDuzyInt.push_back((napis[0]-'0'));
			napis.erase(0,1);
		}
	}

	BardzoDuzeInty(string napis)
	{
		StringNaVector(napis);
	}

	BardzoDuzeInty()
	{
	}

	void pokaz()
	{
		cout<<"Suma vectorow: ";
		for(vector<int>::iterator it = bardzoDuzyInt.begin(); it != bardzoDuzyInt.end(); it++)
		{
			cout<<*it;
		}
		cout<<endl;
	}

	BardzoDuzeInty operator+(BardzoDuzeInty& nowy)
	{
		if(this->bardzoDuzyInt.size() > nowy.getBardzoDuzyInt().size())
		{
			BardzoDuzeInty tmp = dodajVec(*this, nowy);
			reverse(tmp.getBardzoDuzyInt().begin(), tmp.getBardzoDuzyInt().end());
			return tmp;
		}
		else
		{
			BardzoDuzeInty tmp = dodajVec(nowy, *this);
			reverse(tmp.getBardzoDuzyInt().begin(), tmp.getBardzoDuzyInt().end());
			return tmp;
		}
	}

	vector<int>& getBardzoDuzyInt()
			{
		return bardzoDuzyInt;
			}

};



#endif /* BARDZODUZEINTY_HPP_ */
