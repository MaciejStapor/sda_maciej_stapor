//============================================================================
// Name        : KonteneryZad20.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
using namespace std;

list<char> stringNaVector(string napis)
{
	list<char> tmp;
	for(unsigned int i = 0; i < napis.size(); i++)
		tmp.push_back(napis[i]);

	return tmp;
}


int main() {

	string napis = "ala ma kota? ";

	list<char> napisVec = stringNaVector(napis);
	list<char> napisVecOdwrocony = stringNaVector(napis);
	napisVecOdwrocony.reverse();

	napisVec.splice(napisVec.end(), napisVecOdwrocony);

	for(list<char>::iterator it = napisVec.begin(); it != napisVec.end(); it++)
		cout<<*it;
	cout<<endl;


	return 0;
}
