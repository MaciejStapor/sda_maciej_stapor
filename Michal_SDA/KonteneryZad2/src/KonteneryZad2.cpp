//============================================================================
// Name        : KonteneryZad2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
#include <ctime>
#include <cstdlib>
using namespace std;

int funkcjaMieszjaca(int i)
{
	return rand()%i;
}

string pomieszajStringa(string& napis)
{
//	random_shuffle(napis.begin(), napis.end());
	random_shuffle(napis.begin(), napis.end(), funkcjaMieszjaca);
	return napis;
}


int main() {

	srand(time(NULL));

	string napis = "Ala Ma Kota";
	cout<<"Napis przed pomieszaniem: "<<napis<<endl;

	pomieszajStringa(napis);
	cout<<"Napis po pomieszaniu: "<<napis<<endl;

	return 0;
}
