/*
 * staticSingleTon.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#include "staticSingleTon.hpp"

SingleTon* SingleTon::mNewSingleTon = 0;

SingleTon* SingleTon::instance() {

	if(!mNewSingleTon)
		mNewSingleTon=new SingleTon;

	return mNewSingleTon;

}

int SingleTon::getValue() const {
	return mValue;
}

void SingleTon::setValue(int value) {
	mValue = value;
}
SingleTon::SingleTon():mValue(22)
{

}
