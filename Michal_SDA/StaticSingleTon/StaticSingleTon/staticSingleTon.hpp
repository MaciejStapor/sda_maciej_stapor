/*
 * staticSingleTon.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef STATICSINGLETON_HPP_
#define STATICSINGLETON_HPP_

class SingleTon
{
	int mValue;
	static SingleTon *mNewSingleTon;

	SingleTon();
public:

	static SingleTon* instance();
	int getValue() const;
	void setValue(int value);
};


#endif /* STATICSINGLETON_HPP_ */
