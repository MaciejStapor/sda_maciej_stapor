/*
 * player.cpp
 *
 *  Created on: 10.06.2017
 *      Author: Admin
 */
#include "player.hpp"

	bool Player::isEmpty()
	{
		return mSongVector.size() == 0;
	}

	void Player::addSong(const Song& one)
	{
		if(!isEmpty())
			mSongVector.push_back(std::make_shared<Song>(one));
		else
		{
			mSongVector.push_back(std::make_shared<Song>(one));
			mCurrent = *(mSongVector.begin());
		}
	}
	void Player::play()
	{
		if(!isEmpty())
		{
			mCurrent->show();
			next();
		}
		else
			std::cout<<"List song empty! Add song!"<<std::endl;
	}

	void Player::next()
	{
		if(isEmpty())
			std::cout<<"List song empty! Add song!"<<std::endl;
		else if(mCurrent != *(mSongVector.end()-1))
		{
			auto it = find(mSongVector.begin(), mSongVector.end(), mCurrent);
			mCurrent = *(++it);
		}
		else
		{
			mCurrent = *(mSongVector.begin());
		}
	}

	void Player::previous()
	{
		if(isEmpty())
			std::cout<<"List song empty! Add song!"<<std::endl;
		else if(mCurrent != *(mSongVector.begin()))
		{
			auto it = find(mSongVector.begin(), mSongVector.end(), mCurrent);
			mCurrent = *(--it);
		}
		else
		{
			mCurrent = *(mSongVector.end()-1);
		}
	}

	void Player::searchTitle(std::string title)
	{
		auto it = std::find_if(mSongVector.begin(), mSongVector.end(), [title](std::shared_ptr<Song>& second){return second->getTitle() == title;});
		mCurrent = *(it);
	}



