/*
 * song.cpp
 *
 *  Created on: 10.06.2017
 *      Author: Admin
 */
#include "song.hpp"

	Song::Song(std::string artist, std::string title, float length)
	:mArtist(artist)
	,mTitle(title)
	,mLength(length)
	{

	}

const std::string& Song::getArtist() const {
	return mArtist;
}

float Song::getLength() const {
	return mLength;
}

const std::string& Song::getTitle() const {
	return mTitle;
}

	void Song::show()
	{
		std::cout<<"Playing: "<<mTitle<<"\nauthor "<<mArtist<<" length "<<mLength<<std::endl;
	}



