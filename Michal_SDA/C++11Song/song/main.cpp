/*
 * main.cpp
 *
 *  Created on: 10.06.2017
 *      Author: Admin
 */
#include "player.hpp"

int main()
{
	Player player;

	player.addSong(Song("ACDC", "Tom", 3.43));
	player.addSong(Song("Mike_Tomy", "Love", 1.23));
	player.addSong(Song( "Alice", "We_are", 2.54));
	player.addSong(Song("Miki", "Kuku", 2.01));
	player.addSong(Song("ACDC", "Home", 4.51));


	player.searchTitle("We_are");
	player.play();

	player.play();
	player.play();
	player.play();
	player.play();
	player.play();
	player.play();

	player.next();
	player.play();
	player.previous();
	player.play();
	return 0;
}



