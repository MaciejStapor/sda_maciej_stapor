/*
 * song.hpp
 *
 *  Created on: 10.06.2017
 *      Author: Admin
 */

#ifndef SONG_HPP_
#define SONG_HPP_
#include <iostream>

class Song
{
	std::string mArtist;
	std::string mTitle;
	float mLength;
public:
	Song(std::string artist, std::string title, float length);
	void show();
	const std::string& getArtist() const;
	float getLength() const;
	const std::string& getTitle() const;
};




#endif /* SONG_HPP_ */
