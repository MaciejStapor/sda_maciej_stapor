/*
 * player.hpp
 *
 *  Created on: 10.06.2017
 *      Author: Admin
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_
#include <memory>
#include <vector>
#include <algorithm>
#include "song.hpp"

class Player
{
	std::shared_ptr<Song> mCurrent;
	std::vector<std::shared_ptr<Song>> mSongVector;
	bool isEmpty();
public:
	void addSong(const Song& one);
	void play();
	void next();
	void previous();
	void searchTitle(std::string title);
};




#endif /* PLAYER_HPP_ */
