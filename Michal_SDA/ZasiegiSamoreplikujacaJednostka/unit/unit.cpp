/*
 * unit.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include "unit.hpp"
#include "group.hpp"
#include <iostream>
#include <ctime>
#include <cstdlib>


Unit::Unit(std::string id): mId(id), mGroupPtr(0)
{
	srand(time(NULL));
}


void Unit::printId()
{
	std::cout<<mId<<std::endl;
}

void Unit::replicate()
{
	int idVariation = rand()%10;
	char temp = idVariation + '0';

	Unit* newborn = new Unit(mId + temp);

	newborn->addTOGroup(mGroupPtr);

	mGroupPtr->add(newborn);
}


Unit::~Unit()
{

}

void Unit::addTOGroup(Group* x)
{
	mGroupPtr=x;
}
