/*
 * group.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#include "group.hpp"
#include "unit.hpp"
class Unit;

Group::Group(): mUnits(0), mSize(0)
{
}

void Group::add (Unit* x)
{
	resize();
	mUnits[mSize-1]=x;
}
void Group::clear()
{


	for(unsigned int i=0; i<mSize; i++)
	{
		delete mUnits[i];
	}

	delete[] mUnits;

	mSize=0;
	mUnits=0;
}
void Group::replicateGroup()
{
	unsigned int currentSize=mSize;
	for(unsigned int i=0; i<currentSize; i++)
	{
		mUnits[i]->replicate();
	}
}
void Group::printUnits()
{
	for(unsigned int i=0; i<mSize; i++)
	{
		mUnits[i]->printId();
	}
}
void Group::resize()
{
	if(mSize==0)
	{
		mSize++;
		mUnits = new Unit*[mSize];
	}
	else
	{
	mSize++;
	Unit** newUnitsArray = new Unit*[mSize];
	for(unsigned int i=0; i<mSize-1; i++)
		{
			newUnitsArray[i]= mUnits[i];
		}

	delete[] mUnits;

	mUnits=newUnitsArray;
	}
}


Group::~Group()
{
	clear();
}
