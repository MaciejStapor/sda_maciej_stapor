/*
 * unit.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */

#ifndef UNIT_HPP_
#define UNIT_HPP_
#include <string>

class Group;

class Unit
{
	std::string mId;
	Group* mGroupPtr;

public:
	Unit(std::string);
	~Unit();
	void printId();
	void replicate();
	void addTOGroup(Group*);

};



#endif /* UNIT_HPP_ */
