//============================================================================
// Name        : KonteneryZad10.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

void permutacje(vector<int>& nowy)
{
	sort(nowy.begin(), nowy.end());

	do
	{	cout<<"[ ";
		for(vector<int>::iterator it = nowy.begin(); it != nowy.end(); it++)
			cout<<*it<<", ";
		cout<<"]"<<endl;

	}while(next_permutation(nowy.begin(), nowy.end()));

}

int iloscPermutacji(vector<int> nowy)
{
	sort(nowy.begin(), nowy.end());
	int iloscPermutacji = 0;
	do
	{
		int tmp = 1;
		int liczbaVector = 0;
		for(vector<int>::reverse_iterator it = nowy.rbegin(); it != nowy.rend(); it++, tmp*=10)
			{
				liczbaVector+=*it*tmp;

				if(liczbaVector%11 == 0)
					iloscPermutacji++;
			}
	}while(next_permutation(nowy.begin(), nowy.end()));

	return iloscPermutacji;
}

int main() {

	vector<int> nowy;

	nowy.push_back(4);
	nowy.push_back(1);
	nowy.push_back(2);
	nowy.push_back(5);

	permutacje(nowy);

	cout<<"ilosc permutacji: "<<iloscPermutacji(nowy)<<endl;

	return 0;
}
