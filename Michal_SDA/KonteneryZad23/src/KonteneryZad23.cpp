//============================================================================
// Name        : KonteneryZad23.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
#include <cstdlib>
#include <ctime>
#include <algorithm>

using namespace std;

const ostream& operator<<(ostream& os, list<int>& nowa)
{
	os<<"[ ";
	for(list<int>::iterator it = nowa.begin(); it != nowa.end(); it++)
		os<<*it<<", ";
	os<<"]"<<endl;

	return os;
}

bool sortCzyParzyste(int& first, int& second)
{
	return first%2 < second%2;
}

bool szukajPierwszejNieparzystej(int& first)
{
	return first%2 != 0;
}

bool sortNieparzysteMalejaco(int& first, int& second)
{
	return first > second;
}

int main() {
	srand(time(NULL));

	list<int> listaInt;

	for(int i = 0; i < 30; i++)
		listaInt.push_back(rand()%100);

	listaInt.sort(sortCzyParzyste);

	list<int>::iterator it = find_if(listaInt.begin(), listaInt.end(), szukajPierwszejNieparzystej);

	list<int> parzyste;
	listaInt.splice(parzyste.begin(), listaInt, listaInt.begin(), it);

	parzyste.sort();
	listaInt.sort(sortNieparzysteMalejaco);


	parzyste.merge(listaInt);
	cout<<parzyste;
//	cout<<listaInt;

	return 0;
}
