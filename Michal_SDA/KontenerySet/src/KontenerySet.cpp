//============================================================================
// Name        : KontenerySet.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <set>
using namespace std;

bool classCompare(string& first, string& second)
{
	return first.size()<second.size();
}

struct CompareFloatToInt
{
	bool operator()(const float& x, const float& y) const
	{
		return (static_cast<int>(x))<(static_cast<int>(y));
	}
};



int main() {

	set<float,CompareFloatToInt> liczbyFloat;

	liczbyFloat.insert(2.3);
	liczbyFloat.insert(2.54);
	liczbyFloat.insert(3.6);
	liczbyFloat.insert(4.7);
	liczbyFloat.insert(4.734);

	cout<<"set liczbyFloat: [ ";
	for(set<float>::iterator i=liczbyFloat.begin(); i!=liczbyFloat.end(); i++)
	{
		cout<<*i<<", ";
	}
	cout<<"]"<<endl;

	set<float>::iterator i=liczbyFloat.find(2.3);

	liczbyFloat.erase(i);

	cout<<"set liczbyFloat erase: [ ";
	for(set<float>::iterator i=liczbyFloat.begin(); i!=liczbyFloat.end(); i++)
	{
		cout<<*i<<", ";
	}
	cout<<"]"<<endl;


	set<string> name;

	name.insert("tom");
	name.insert("chris");
	name.insert("kate");
	name.insert("jo");

	string tab[2]={"many", "good"};

	name.insert(tab, tab+2);

	cout<<"set name: [ ";
	for(set<string>::iterator i=name.begin(); i!=name.end(); i++)
	{
		cout<<*i<<", ";
	}
	cout<<"]"<<endl;

	return 0;
}
