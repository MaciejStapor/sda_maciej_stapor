/*
 * zad.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */


#ifndef ZAD_HPP_
#define ZAD_HPP_

class Osoba
{
	int mPesel;
public:

	Osoba (int pesel=0);
	void podajPesel();

};

class Student:  public virtual Osoba
{
	int mNrIndeksu;
	std::string mKierunek;

public:
	Student(int nrIndeksu, std::string kierunek, int pesel);
	Student(int nrIndeksu, std::string kierunek);
	virtual void uczeSie();
	void podajNr ();
	virtual ~Student(){}
};

class Pracownik:  public virtual Osoba
{
	int mPensja;
	std::string mZawod;
public:
	Pracownik(int pensja, std::string zawod, int pesel);
	Pracownik(int pensja, std::string zawod);
	virtual void pracuj();
	virtual ~Pracownik(){}
};

class PracujacyStudent: public Student, public Pracownik
{
public:
PracujacyStudent(int nrIndeksu, std::string kierunek, int pensja, std::string zawod, int pesel);

void uczeSie();
void pracuj();
};





#endif /* ZAD_HPP_ */
