/*
 * operatory.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef OPERATORY_HPP_
#define OPERATORY_HPP_

class Wektor
{
	int mX;
	int mY;

public:
	Wektor(int, int);
	void operator ! ();
	bool operator == (Wektor nowy);
	Wektor *operator * (Wektor nowy);
	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);
	void wypisz();
};





#endif /* OPERATORY_HPP_ */
