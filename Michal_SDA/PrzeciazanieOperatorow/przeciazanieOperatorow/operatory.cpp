/*
 * operatory.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include "operatory.hpp"
#include <iostream>


Wektor::Wektor(int x, int y): mX(x), mY(y)
{

}
void Wektor::operator!()
{
	int temp = mX;
	mX=mY;
	mY=temp;
}


bool Wektor::operator == (Wektor nowy)
		{

	if(mX==nowy.mX && mY==nowy.mY)
	{
		return true;
	}

	return false;

		}


Wektor *Wektor::operator * (Wektor nowy)
		{

	mX *= nowy.mX;
	mY *= nowy.mY;

	return this;
		}


void Wektor::wypisz()
{
	std::cout<<"X "<<mX<<" Y "<<mY<<std::endl;
}

int Wektor::getX() const {
	return mX;
}

void Wektor::setX(int x) {
	mX = x;
}

int Wektor::getY() const {
	return mY;
}

void Wektor::setY(int y) {
	mY = y;
}
