/*
 * za2.cpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */
#include <iostream>
#include <cmath>
#include "zad2.hpp"


using namespace std;

Data::Data(): mRok(1900),  mDzien(1), mMiesiac(1)
{

}

int
Data::getDzien () const
{
  return mDzien;
}

void
Data::setDzien (int dzien)
{
  if(dzien>31){mDzien=31;}
  else{ mDzien = dzien;}
}

int
Data::getMiesiac () const
{
  return mMiesiac;
}

void
Data::setMiesiac (int miesiac)
{
  if(miesiac>12){mMiesiac=12;}
  else{ mMiesiac = miesiac;}
}

int
Data::getRok () const
{
  return mRok;
}

void
Data::setRok (int rok)
{
  if(rok<1900){mRok=1900;}
  else{ mRok = rok;}
}

Data::Data(int rok, int miesiac, int dzien):mRok(rok), mDzien(dzien), mMiesiac(miesiac)
{

}

void Data::przesunLata(int  x)
{
  mRok+=x;
}
  void Data::przesunMiesiace(int  x)
  {
    if(mMiesiac+x<=12){mMiesiac+=x;}
    else
      {
	int pom=0;
	mMiesiac=(mMiesiac+x)%12;
	pom=(mMiesiac+x)/12;
	przesunLata(pom);
      }
  }
  void Data::przesunDni(int  x)
  {
    if(mDzien+x<=31){mDzien+=x;}
       else
         {
   	int pom=0;
   	mDzien=(mDzien+x)%31;
   	pom=(mDzien+x)/31;
   	przesunMiesiace(pom);
         }
  }

  void Data::wypisz()
  {
    cout<<"Data: "<< mDzien<<"."<<mMiesiac<<"."<<mRok<<"r."<<endl;
  }

  Data Data::roznica(Data& nowa)
  {
    nowa.mRok=abs(nowa.mRok-mRok);
    nowa.mMiesiac=abs(nowa.mMiesiac-mMiesiac);
    nowa.mDzien=abs(nowa.mDzien-mDzien);
    return nowa;
  }


