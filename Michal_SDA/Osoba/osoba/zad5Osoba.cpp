/*
 * zad5Osoba.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include <iostream>
#include "zad5Osoba.hpp"

using namespace std;

Osoba::Osoba(): Data(), mImie("Brak"), mNazwisko("Brak"), mPlec(0), mPesel(0)
{

}
Osoba::Osoba(int rok, int miesiac, int dzien, string imie, string nazwisko, int plec, int *pesel ): Data(rok, miesiac, dzien), mImie(imie), mNazwisko(nazwisko), mPlec(plec)
{
	setPesel(pesel);
}

Osoba::Osoba(string imie, string nazwisko, int plec, int *pesel): mImie(imie), mNazwisko(nazwisko), mPlec(plec)
{
	setPesel(pesel);

	int a=0,b=0,c=0;
	a=1900+((pesel[0]*10)+pesel[1]);
	b=(pesel[2]*10)+pesel[3];
	c=(pesel[4]*10)+pesel[5];



	Data::setDzien(c);
	Data::setMiesiac(b);
	Data::setRok(a);
}

const string& Osoba::getImie() const
{
	return mImie;
}

void Osoba::setImie(const string& imie)
{
	mImie = imie;
}

const string& Osoba::getNazwisko() const
{
	return mNazwisko;
}

void Osoba::setNazwisko(const string& nazwisko)
{
	mNazwisko = nazwisko;
}

const int* Osoba::getPesel() const
{
	return mPesel;
}

int Osoba::getPlec() const
{
	return mPlec;
}

void Osoba::setPlec(int plec)
{
	mPlec = plec;
}

void Osoba::setPesel(int* pesel)
{
	if(pesel == 0)
	{
		cerr<<"Blad pesel niepoprawny";
	}
	else{
		const int wagi[] = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };
		int suma = 0,pomocnicza=0;
		for (int i = 0; i < 10; ++i)
		{
			suma += pesel[i] * wagi[i];
		}

		pomocnicza= 10 - (suma % 10);
		if (pomocnicza == pesel[10])
		{
			mPesel = pesel;
			cout<<"PESEL POPRAWNY"<<endl;
		}
		else
		{
			cerr<<"Blad pesel niepoprawny";
		}

	}
}

void Osoba::wypisz()
{
	cout << "Imie: "<< mImie<<endl;
	cout << "Nazwisko: "<< mNazwisko<<endl;
	switch(mPlec)
	{
	case kobieta:
		cout << "Plec: Kobieta"<<endl;
		break;
	case mezczyzna:
		cout << "Plec: Mezczyzna"<<endl;
		break;
	default:
		cerr<<"Bledne dane"<<endl;
	}
	cout<<"Pesel: ";
	for(int i=0; i<11; i++)
	{
		cout << mPesel[i];
	}
	cout<<endl;
	cout<<"Data urodzenia: "<<mRok<<"."<<mMiesiac<<"."<<mDzien<<endl;
}

bool Osoba::czyStarszy(Osoba nowa)
{
	if(mRok>nowa.mRok)
	{
		cout<<"Jest starsza!"<<endl;
		return true;
	}
	else
	{
		if(mMiesiac>nowa.mMiesiac)
		{
//			cout<<"Jest starsza!"<<endl;
			return true;
		}
		else
		{
			if(mDzien>nowa.mDzien)
			{
//				cout<<"Jest starsza!"<<endl;
				return true;
			}
			else
			{
//				cout <<"Sa tego samego wieku"<<endl;
				return false;
			}
		}
	}


	return false;
}
