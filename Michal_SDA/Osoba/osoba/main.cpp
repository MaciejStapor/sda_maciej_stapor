/*
 * main.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include <iostream>
#include "zad5Osoba.hpp"
#include "zad2.hpp"

using namespace std;

int main()
{
	int pesel[11]={8,9,0,7,2,9,1,5,0,1,8};

	Osoba osoba(1989, 7,29,"Maciej","Stapor",Osoba::mezczyzna, pesel);



	osoba.wypisz();

	cout<<"........................................................."<<endl;
	Osoba osoba2("Maciej","Stapor",Osoba::mezczyzna, pesel);
	osoba2.wypisz();

	Osoba osoba3(1989, 7,30,"Maciej","Stapor",Osoba::mezczyzna, pesel);

	int pomocnicza=0;
	pomocnicza=osoba.czyStarszy(osoba3);
	if(pomocnicza == true) cout<< "Jest starsza!"<<endl;
	else cout<< "Nie jest starsza!"<<endl;

	return 0;
}

