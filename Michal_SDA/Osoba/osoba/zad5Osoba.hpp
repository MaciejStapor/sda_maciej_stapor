/*
 * zad5Osoba.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */
#include <iostream>
#include "zad2.hpp"

using namespace std;

#ifndef ZAD5OSOBA_HPP_
#define ZAD5OSOBA_HPP_
class Osoba: public Data
{
	string mImie, mNazwisko;
	int mPlec;
	int *mPesel;
public:
Osoba();

enum Plec
{
	kobieta,
	mezczyzna
};

Osoba(int rok, int miesiac, int dzien, string imie, string nazwisko, int plec, int *pesel );
Osoba(string imie, string nazwisko, int plec, int *pesel);
	const string& getImie() const;
	void setImie(const string& imie);
	const string& getNazwisko() const;
	void setNazwisko(const string& nazwisko);
	const int* getPesel() const;
	int getPlec() const;
	void setPlec(int plec);
	void setPesel(int* pesel);
	void wypisz();
	bool czyStarszy(Osoba nowa);
};





#endif /* ZAD5OSOBA_HPP_ */
