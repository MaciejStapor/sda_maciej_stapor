/*
 * kurs.hpp
 *
 *  Created on: 28.05.2017
 *      Author: Admin
 */

#ifndef KURS_HPP_
#define KURS_HPP_
#include "czas.hpp"
#include "dzien.hpp"

using namespace std;



class Kurs
{
	string mNazwa;
	Dzien mDzien;
	Czas mPoczatek;
	Czas mKoniec;
	unsigned int mNumerSali;
//	static int licznik;
public:
	Kurs(string nazwa, int dzien, string poczatek, string koniec, unsigned int numerSali)
	: mNazwa(nazwa)
	, mDzien(dzien)
	, mPoczatek(poczatek)
	, mKoniec(koniec)
	, mNumerSali(numerSali)
	{
//		++licznik;
	}

	Kurs()
	: mNazwa(0)
	, mDzien(0)
	, mPoczatek(0)
	, mKoniec(0)
	, mNumerSali(0)
	{

	}

	void pokaz()
	{
		cout<<"Nazwa: "<<mNazwa<<endl;
		cout<<"Numer sali: "<<mNumerSali<<endl;
		cout<<"Dzien: ";
		mDzien.pokaz();
		cout<<"Poczatek: ";
		mPoczatek.pokaz();
		cout<<"Koniec: ";
		mKoniec.pokaz();
		cout<<"....................."<<endl;
	}

	bool operator==(Kurs nowy)
	{
		if(this->mDzien == nowy.getDzien() && this->mKoniec >= nowy.getPoczatek())
			return true;
		else
			return false;
	}

	bool operator<(Kurs nowy)
	{
		if(this->mDzien == nowy.getDzien())
			return this->mPoczatek < nowy.getPoczatek();
		else
			return this->mDzien < nowy.getDzien();
	}

const Dzien& getDzien() const {
	return mDzien;
}

const Czas& getKoniec() const {
	return mKoniec;
}

const string& getNazwa() const {
	return mNazwa;
}

const Czas& getPoczatek() const {
	return mPoczatek;
}
};





#endif /* KURS_HPP_ */
