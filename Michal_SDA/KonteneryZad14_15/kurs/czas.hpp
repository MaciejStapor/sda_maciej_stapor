/*
 * czas.hpp
 *
 *  Created on: 28.05.2017
 *      Author: Admin
 */

#ifndef CZAS_HPP_
#define CZAS_HPP_
#include <iostream>
#include <string>
#include <map>

using namespace std;
class Czas
{
	string mGodziny;
	string mMinuty;
public:
	Czas(string napis)
{
		mGodziny = napis.substr(0, 2);
		mMinuty = napis.substr(2, 2);
}

	const string& getGodziny() const {
		return mGodziny;
	}

	const string& getMinuty() const {
		return mMinuty;
	}

	bool operator<(Czas nowy)
	{
		if(this->mGodziny != nowy.getGodziny())
		{
			int godz = ((this->mGodziny)[0])*10;
			godz += ((this->mGodziny)[1]);

			int godz2 = ((nowy.getGodziny())[0])*10;
			godz += ((nowy.getGodziny())[1]);

			return godz<godz2?true:false;
		}
		else
		{
			int godz = ((this->mMinuty)[0])*10;
			godz += ((this->mMinuty)[1]);

			int godz2 = ((nowy.getMinuty())[0])*10;
			godz += ((nowy.getMinuty())[1]);

			return godz<godz2?true:false;
		}
	}


	bool operator>=(Czas nowy)
		{
			if(this->mGodziny != nowy.getGodziny())
			{
				int godz = ((this->mGodziny)[0])*10;
				godz += ((this->mGodziny)[1]);

				int godz2 = ((nowy.getGodziny())[0])*10;
				godz += ((nowy.getGodziny())[1]);

				return godz>=godz2?true:false;
			}
			else
			{
				int godz = ((this->mMinuty)[0])*10;
				godz += ((this->mMinuty)[1]);

				int godz2 = ((nowy.getMinuty())[0])*10;
				godz += ((nowy.getMinuty())[1]);

				return godz>=godz2?true:false;
			}
		}

	void pokaz()
	{
		cout<<mGodziny<<":"<<mMinuty<<endl;
	}

	bool operator==(Czas nowy)
	{
		if(this->mGodziny == nowy.getGodziny() && this->mMinuty == nowy.getMinuty())
			return true;
		else
			return false;
	}
};




#endif /* CZAS_HPP_ */
