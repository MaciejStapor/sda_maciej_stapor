#include <fstream>
#include <list>
#include <algorithm>
#include "kurs.hpp"
#include "student.hpp"
using namespace std;

//int Kurs::licznik = 1;

void pokaz(list<Kurs> nowy)
{
	unsigned int licznik = 0;
	for(list<Kurs>::iterator it = nowy.begin(); it != nowy.end(); it++ )
	{
		licznik++;
		cout<<licznik<<"."<<endl;
		it->pokaz();
	}
}

bool sortList(Kurs& first, Kurs& second)
{
	return first<second;
}

void pokazKolizje(list<Kurs> nowy)
{
	for(list<Kurs>::iterator it = nowy.begin(); it != --nowy.end(); it++)
	{
		list<Kurs>::iterator tmp = it;
		tmp++;
		if(*it == *tmp)
		{
			cerr<<"Kolizja!"<<endl;
			cerr<<"Nazwa: "<<it->getNazwa()<<" | "<<tmp->getNazwa()<<endl;;

		}
	}
}

class szukajKurs
{
	string mNazwa;
public:
	szukajKurs(string nazwa)
	:mNazwa(nazwa)
{

}
	bool operator()(Kurs& nowy)
		{
		return mNazwa.compare(nowy.getNazwa()) ? false:true;
		}
};

int main()
{
		list<Kurs> listKursow;
		fstream plik;
		plik.open("kurs.txt", ios::in);

		if(!plik.good())
			cout<<"Nie udalo sie otworzyc plik\nKoiec programu!"<<endl;

		while(!plik.eof())
		{
			string nazwa;
			int dzien;
			string poczatek;
			string koniec;
			unsigned int numerSali;

			plik>>nazwa>>dzien>>poczatek>>koniec>>numerSali;

			listKursow.push_back(Kurs(nazwa, dzien, poczatek, koniec, numerSali));
		}

		listKursow.sort(sortList);

//		pokaz(listKursow);
//		pokazKolizje(listKursow);

		plik.close();

	list<Student> listStudentow;
	fstream plikStudent;
	plikStudent.open("student.txt", ios::in);

	if(!plikStudent.good())
		cout<<"Nie udalo sie otworzyc plik\nKoiec programu!"<<endl;

	while(!plikStudent.eof())
	{
		string imieNazwisko;
		int iloscKursow;
		plikStudent>>imieNazwisko>>iloscKursow;
//		cout<<imieNazwisko<<" "<<iloscKursow<<endl;

		Student tmpS(imieNazwisko, iloscKursow);
		for(int i = 0; i < iloscKursow; i++)
		{
			string kurs;
			plikStudent>>kurs;
			list<Kurs>::iterator tmp = find_if(listKursow.begin(), listKursow.end(), szukajKurs(kurs));
			tmpS.dodajKurs(*tmp);
		}
		listStudentow.push_back(tmpS);

	}

	for(list<Student>::iterator its = listStudentow.begin(); its != listStudentow.end(); its++)
		its->pokaz();


	return 0;
}
