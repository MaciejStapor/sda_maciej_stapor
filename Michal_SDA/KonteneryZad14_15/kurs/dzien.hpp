/*
 * data.hpp
 *
 *  Created on: 28.05.2017
 *      Author: Admin
 */

#ifndef DZIEN_HPP_
#define DZIEN_HPP_
#include <iostream>
#include <map>

using namespace std;
class Dzien
{
	string mDzien;
	int mX;
public:
	Dzien(int x)
{
		map<int, string> dzienTyg;

		dzienTyg.insert(make_pair(1, "Poniedzialek"));
		dzienTyg.insert(make_pair(2, "Wtorek"));
		dzienTyg.insert(make_pair(3, "Sroda"));
		dzienTyg.insert(make_pair(4, "Czwartek"));
		dzienTyg.insert(make_pair(5, "Piatek"));
		dzienTyg.insert(make_pair(6, "Sobota"));
		dzienTyg.insert(make_pair(7, "Niedziela"));

		mDzien=dzienTyg.find(x)->second;
		mX=x;

}
	bool operator<(Dzien nowy)
	{
		return this->mX<nowy.getX()?true:false;
	}

	const string& getDzien() const {
		return mDzien;
	}

	void pokaz()
	{
		cout<<mDzien<<endl;
	}

	bool operator==(Dzien nowy)
	{
		if(this->mDzien == nowy.getDzien())
			return true;
		else
			return false;
	}

	int getX() const {
		return mX;
	}
};





#endif /* DZIEN_HPP_ */
