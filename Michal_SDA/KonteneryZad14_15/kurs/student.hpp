/*
 * student.hpp
 *
 *  Created on: 29.05.2017
 *      Author: RENT
 */

#ifndef STUDENT_HPP_
#define STUDENT_HPP_
#include "kurs.hpp"

using namespace std;
class Student
{
	string mImie;
	string mNazwisko;
	unsigned int mIloscKursow;
	list<Kurs> mListKursu;


public:
	Student(string imieNazwisko,int iloscKursow)
	{
		int pos = imieNazwisko.find_first_of("_");
		mImie = imieNazwisko.substr(0, pos);
		mNazwisko = imieNazwisko.substr(pos+1, imieNazwisko.size());

		mIloscKursow = iloscKursow;
	}

	void dodajKurs(Kurs nowy)
	{
		mListKursu.push_back(nowy);
	}

	void pokaz()
	{
		cout<<"Imie: "<<mImie<<endl;
		cout<<"Nazwisko: "<<mNazwisko<<endl;
		cout<<"Lista kursow: "<<endl;
		list<Kurs>::iterator it;

		for(it = mListKursu.begin(); it != --mListKursu.end(); it++)
		{
			list<Kurs>::iterator kolizja = it;
			kolizja++;

			if(*kolizja == *it)
				cerr<<"kolizja ! "<< kolizja->getNazwa()<<" | "<<it->getNazwa()<<endl;

			it->pokaz();
		}
		it->pokaz();
		cout<<"___________________________________________"<<endl;
	}

	unsigned int getIloscKursow() const {
		return mIloscKursow;
	}

	const list<Kurs>& getListKursu() const {
		return mListKursu;
	}
};




#endif /* STUDENT_HPP_ */
