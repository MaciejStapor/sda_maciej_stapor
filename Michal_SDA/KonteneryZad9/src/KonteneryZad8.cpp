//============================================================================
// Name        : KonteneryZad8.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

ostream& operator<<(ostream& os, vector<int> nowy)
{
	os<<"[ ";
	for(vector<int>::iterator it = nowy.begin(); it != nowy.end(); it++ )
	{
		os<<*it<<", ";
	}
	os<<"]"<<endl;

	return os;
}

bool wielokrotnoscDwaFun(int x)
{
	if(x == 1)
		return true;

	return x%2 != 0;
}

bool wielokrotnoscTrzyFun(int x)
{
	if(x == 1)
			return true;

	return x%3 != 0;
}


vector<int> vectorWspolny(int x)
{
	vector<int> nowy;
	for(int i = 1; i < x; i++)
		nowy.push_back(i);

	vector<int> wielokrotnoscDwa(nowy.size());
	vector<int>::iterator it = remove_copy_if(nowy.begin(), nowy.end(), wielokrotnoscDwa.begin(), wielokrotnoscDwaFun);
	wielokrotnoscDwa.resize(distance(wielokrotnoscDwa.begin(), it));

	vector<int> wielokrotnoscTrzy(nowy.size());
	it = remove_copy_if(nowy.begin(), nowy.end(), wielokrotnoscTrzy.begin(), wielokrotnoscTrzyFun);
	wielokrotnoscTrzy.resize(distance(wielokrotnoscTrzy.begin(), it));

	vector<int> wynikowy(wielokrotnoscTrzy.size()<wielokrotnoscDwa.size()?wielokrotnoscDwa.size():wielokrotnoscTrzy.size());
	it = set_intersection(wielokrotnoscDwa.begin(), wielokrotnoscDwa.end(), wielokrotnoscTrzy.begin(), wielokrotnoscTrzy.end(), wynikowy.begin());
	wynikowy.resize(distance(wynikowy.begin(), it));

	return wynikowy;
}

int main() {

	int zakres = 100;

	vector<int> nowy = vectorWspolny(zakres);
	cout<<"Vector czesci wspolnych: ";
	cout<<nowy;

	return 0;
}
