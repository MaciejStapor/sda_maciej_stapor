/*
 * kalkulator.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef KALKULATOR_HPP_
#define KALKULATOR_HPP_
#include "StaticLoggerSingleTon.hpp"

class Kalkulator
{
public:
	int podziel(int a, int b)
	{
		Logger::log()->addData("podziel: ");
		if(b==0)
		{
			Logger::log()->addData("nie dziel przez 0!");
			return 0;
		}
		else
		{
			return a/b;
		}
	}

};



#endif /* KALKULATOR_HPP_ */
