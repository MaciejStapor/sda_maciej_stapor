/*
 * StaticLoggerSingleTon.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */
#include "StaticLoggerSingleTon.hpp"
#include <ctime>
#include <iomanip>

Logger* Logger::mNewLogger=0;

Logger::Logger()
{
	mFile.open("log.txt");
}


Logger* Logger::log()
{
	if(!mNewLogger)
		mNewLogger=new Logger;

	return mNewLogger;
}


void Logger::addData(string dataToLog)
{
	time_t t;
	t=time(NULL);
	struct tm* current = localtime(&t);
	mFile<<"Data: "<<current->tm_year +1900<<"."
			<< current->tm_mon+1<<"."
			<< current->tm_mday<< "  Godzina: "
			<< current->tm_hour<<":"
			<< current->tm_min<<":"
			<< current->tm_sec
			<<endl;

	mFile << dataToLog<<endl;
}
