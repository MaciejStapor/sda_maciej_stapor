/*
 * StaticLoggerSingleTon.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef STATICLOGGERSINGLETON_HPP_
#define STATICLOGGERSINGLETON_HPP_
#include <iostream>
#include <fstream>

using namespace std;

class Logger
{
	static Logger* mNewLogger;
	ofstream mFile;
	Logger();

public:
	static Logger* log();
	void addData(string dataToLog);
};




#endif /* STATICLOGGERSINGLETON_HPP_ */
