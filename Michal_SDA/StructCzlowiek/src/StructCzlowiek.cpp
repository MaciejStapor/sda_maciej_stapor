//============================================================================
// Name        : StructCzlowiek.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
using namespace std;

struct Czlowiek
{
	int mDzienUrodzenia;
	int mMiesiacUrodzenia;
	int mRokUrodzenia;


	Czlowiek(int dzien,int miesiac,int rok)
	: mDzienUrodzenia(dzien)
	, mMiesiacUrodzenia(miesiac)
	, mRokUrodzenia(rok)
	{

	}
	Czlowiek(const Czlowiek &max)
		: mDzienUrodzenia(max.mDzienUrodzenia)
		, mMiesiacUrodzenia(max.mMiesiacUrodzenia)
		, mRokUrodzenia(max.mRokUrodzenia)
		{

		}
	Czlowiek()
	: mDzienUrodzenia(0)
	, mMiesiacUrodzenia(0)
	, mRokUrodzenia(0)
	{

	}
	int podajWiek(int rok)
	{
		return (rok-mRokUrodzenia < 0 ? 0 : rok-mRokUrodzenia );
	}
	void wypisz()
	{
		cout<<"data urodzenia "<<mDzienUrodzenia<<"."<<mMiesiacUrodzenia<<"."<<mRokUrodzenia<<"r."<<endl;
	}

};

struct Pracownik: Czlowiek
{
	float mPensja;
	int mStaz;
	Pracownik(const Czlowiek &max, int staz)
	:Czlowiek(max)
	, mPensja(0)
	, mStaz(staz)
	{

	}
	float wyliczPensje()
	{
		mPensja=(1+(mStaz*0.05));
		return mPensja;
	}
};

class Student: Czlowiek
{
	int mNumerIndeksu;
public:
	Student(int nr): mNumerIndeksu(nr)
	{

	}
	void bawSie()
	{
		cout<<"Bawie sie aaa!"<<endl;
	}
};

int main() {

	Czlowiek maciej(29,7,1989);
	Pracownik tomek(maciej, 4);
	Student student(123456);

	cout<<"Wiek "<<maciej.podajWiek(2017)<<endl;
	cout<<"pensja "<<tomek.wyliczPensje()<<endl;
	student.bawSie();

	cout<<"Wiek "<<tomek.podajWiek(2017)<<endl;
	tomek.wypisz();
	maciej.wypisz();


	return 0;
}
