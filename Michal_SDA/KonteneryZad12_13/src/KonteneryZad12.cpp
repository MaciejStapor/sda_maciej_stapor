//============================================================================
// Name        : KonteneryZad12.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

ostream& operator<<(ostream& os, vector<int> nowy)
{
	os<<"[ ";
	for(vector<int>::iterator it = nowy.begin(); it != nowy.end(); it++ )
	{
		os<<*it<<", ";
	}
	os<<"]"<<endl;

	return os;
}

vector<int> intNaVector(int x)
{
	int tmp;
	vector<int> nowy;
	do
	{
		tmp = x%10;
		nowy.push_back(tmp);
		x /=10;
	}while(x != 0);

	reverse(nowy.begin(), nowy.end());

	return nowy;
}

int vectorNaInt(vector<int> nowy)
{
	int tmp = 1;
	int liczba = 0;
	for(vector<int>::reverse_iterator it = nowy.rbegin(); it != nowy.rend(); it++, tmp*=10)
	{
		liczba += *it*tmp;
	}
	return liczba;
}

int pierwszyPalindrom(int liczba)
{
	for(int i = liczba+1; ; i++)
	{
		vector<int> nowy = intNaVector(i);

		int poprawny = vectorNaInt(nowy);
		reverse(nowy.begin(), nowy.end());
		int odwrocony = vectorNaInt(nowy);

		if(poprawny == odwrocony)
			return i;

	}
	return 0;
}

int main() {

	int liczba = 121;

	vector<int> nowy = intNaVector(liczba);
	cout<<nowy;

	cout<<"Pierwszy palindrom od zadanej liczby: "<<pierwszyPalindrom(liczba)<<endl;

	return 0;
}
