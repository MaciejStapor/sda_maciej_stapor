/*
 * kolo.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include "figura.hpp"

#ifndef KOLO_HPP_
#define KOLO_HPP_

class Kolo: public Figura
{
	const float mPi;
	int mR;

public:

	Kolo(int r);
	float pole();

};



#endif /* KOLO_HPP_ */
