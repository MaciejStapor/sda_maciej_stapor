/*
 * trojkat.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#include "figura.hpp"

#ifndef TROJKAT_HPP_
#define TROJKAT_HPP_

class Trojkat : public Figura
{
	float mH,mA;


public:

	Trojkat(int h, int a);
	float pole();

};


#endif /* TROJKAT_HPP_ */
