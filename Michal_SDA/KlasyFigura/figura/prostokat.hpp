/*
 * prostokat.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include "figura.hpp"

#ifndef PROSTOKAT_HPP_
#define PROSTOKAT_HPP_

class Prostokat: public Figura
{
	float mA,mB;
public:
	Prostokat(int a, int b);
	float pole();
};




#endif /* PROSTOKAT_HPP_ */
