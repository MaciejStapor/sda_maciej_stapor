/*
 * kolo.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "kolo.hpp"
#include <iostream>
using namespace All;
using namespace Geometria;

Kolo::Kolo(std::string nazwa, int promien, std::string color): mNazwa(nazwa), mPromien(promien), mKolor(color)
{

}

void Kolo::wypisz()
{
	std::cout<<mNazwa<<" "<< mKolor<<" o promieniu "<<mPromien<<std::endl;
}

Kolo::~Kolo()
{

}

