/*
 * mian.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */



#include <iostream>
#include <string>
#include "namespace.hpp"
#include "kolo.hpp"
#include "kwadrat.hpp"
#include "figura.hpp"
using namespace All;
using namespace Geometria;

void wypiszFunkcja(Figura &f)
{
	f.wypisz();
}


using namespace Kolor;

int main()
{
	Figura *f;

	Kwadrat k("kwadrat", 20,convertToString(Kolor::bialy));
	Kolo ko("kolo", 13, convertToString(Kolor::czarny));

	f=&k;
	f->wypisz();


	f=&ko;
	f->wypisz();

	std::cout<<"........................funkcja"<<std::endl;
	wypiszFunkcja(k);
	wypiszFunkcja(ko);


	return 0;
}
