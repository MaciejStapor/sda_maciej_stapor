/*
 * kwadrat.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */




#include "kwadrat.hpp"
#include <iostream>
using namespace All;

Kwadrat::Kwadrat(std::string nazwa, int bok, std::string color): mNazwa(nazwa), mBok(bok), mKolor(color)
{

}

void Kwadrat::wypisz()
{
	std::cout<<mNazwa<<" "<< mKolor<<" o boku "<<mBok<<std::endl;
}

Kwadrat::~Kwadrat()
{

}

