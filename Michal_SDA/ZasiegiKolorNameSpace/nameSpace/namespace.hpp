/*
 * namespace.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef NAMESPACE_HPP_
#define NAMESPACE_HPP_

#include <string>
namespace All
{
namespace Kolor{

enum mKolor
{
	niebieski,
	czarny,
	bialy,
};

std::string convertToString(mKolor kolor);

}
}



#endif /* NAMESPACE_HPP_ */
