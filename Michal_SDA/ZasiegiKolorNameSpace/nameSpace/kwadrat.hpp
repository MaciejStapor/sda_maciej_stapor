/*
 * kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KWADRAT_HPP_
#define KWADRAT_HPP_

#include "figura.hpp"
#include  <string>
namespace All
{
class Kwadrat: public Geometria::Figura
{
 std::string mNazwa;
 int mBok;
 std::string mKolor;

public:
 Kwadrat();
 Kwadrat(std::string nazwa, int bok, std::string color);
 void wypisz();
 ~Kwadrat();
};

}

#endif /* KWADRAT_HPP_ */
