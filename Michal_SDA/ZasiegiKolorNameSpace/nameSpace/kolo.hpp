/*
 * kolo.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KOLO_HPP_
#define KOLO_HPP_
#include "figura.hpp"
#include  <string>
namespace All
{
namespace Geometria
{
class Kolo: public Geometria::Figura
{
 std::string mNazwa;
 int mPromien;
 std::string mKolor;

public:
 Kolo();
 Kolo(std::string nazwa, int promien, std::string color);
 void wypisz();
 ~Kolo();
};

}
}

#endif /* KOLO_HPP_ */
