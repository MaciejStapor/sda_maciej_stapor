/*
 * main.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "silniaNaOdwrot.hpp"
#include "gtest/gtest.h"


TEST(Konstruktor, SilniaNaOdwrot)
{
	SilniaNaOdwrot a;
	EXPECT_EQ(0, a.getLiczba());
}

TEST(SilniaNaOdwrot, policz)
{
	SilniaNaOdwrot a;
	EXPECT_EQ(5, a.policz(120));
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


