/*
 * silniaNaOdwrot.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef SILNIANAODWROT_HPP_
#define SILNIANAODWROT_HPP_

class SilniaNaOdwrot
{
	int mLiczba;
public:
	SilniaNaOdwrot();
	int policz(int liczba);
	int getLiczba() const;
	void setLiczba(int liczba);

};




#endif /* SILNIANAODWROT_HPP_ */
