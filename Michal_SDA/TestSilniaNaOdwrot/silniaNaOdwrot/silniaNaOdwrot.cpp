/*
 * silniaNaOdwrot.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "silniaNaOdwrot.hpp"

int SilniaNaOdwrot::getLiczba() const {
	return mLiczba;
}

void SilniaNaOdwrot::setLiczba(int liczba) {
	mLiczba = liczba;
}

SilniaNaOdwrot::SilniaNaOdwrot(): mLiczba(0)
{

}

int SilniaNaOdwrot::policz(int liczba)
{
	setLiczba(liczba);
	int pom=2;

	while(liczba!=pom)
	{
		liczba/=pom;
		pom++;
	}

	return pom;
}
