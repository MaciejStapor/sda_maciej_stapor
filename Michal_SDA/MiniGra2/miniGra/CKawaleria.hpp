/*
 * CKawaleria.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef CKAWALERIA_HPP_
#define CKAWALERIA_HPP_

#include "CJednostka.hpp"

class CKawaleria : public CJednostka
{
public:
	CKawaleria(char symbol, int zycie, int atak, int szybkosc);
};


#endif /* CKAWALERIA_HPP_ */
