/*
 * uml.cpp
 *
 *  Created on: 03.04.2017
 *      Author: RENT
 */

#include <iostream>
#include "uml.hpp"

using namespace std;

int Time::getHour() const
{
	return mHour;
}

void Time::setHour(int hour)
{
	if(hour>=0 && hour<=23)
		{
		mHour = hour;
		}
		else
		{
			cerr<<"godziny przedzial od 0 do 23 "<<endl;
		}

}

int Time::getMintute() const
{
	return mMintute;
}

void Time::setMintute(int mintute)
{
	if(mintute>=0 && mintute<=60)
	{
	mMintute = mintute;
	}
	else
	{
		cerr<<"minuty przedzial od 0 do 60 "<<endl;
	}
}

int Time::getSecond() const
{
	return mSecond;
}

void Time::setSecond(int second)
{
	if(second>=0 && second<=60)
		{
		mSecond = second;
		}
		else
		{
			cerr<<"sekundy przedzial od 0 do 60 "<<endl;
		}

}

Time::Time(int hour, int minute, int second):mHour(hour), mMintute(minute), mSecond(second)
{

}

void Time::setTime(int hour, int minute, int second)
{
setSecond(second);
setMintute(minute);
setHour(hour);
}

string Time::toString()
{
	string tymczasowy="";

return tymczasowy;
}

const string& Circle::getColor() const
{
	return mColor;
}

void Circle::setColor(const string& color)
{
	mColor = color;
}

double Circle::getRadius() const
{
	return mRadius;
}

void Circle::setRadius(double radius)
{
	mRadius = radius;
}

double Cylinder::getHeight() const
{
	return mHeight;
}

void Cylinder::setHeight(double height)
{
	mHeight = height;
}
