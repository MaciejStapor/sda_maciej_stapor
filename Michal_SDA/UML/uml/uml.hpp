/*
 * uml.hpp
 *
 *  Created on: 03.04.2017
 *      Author: RENT
 */

#include <iostream>
using namespace std;

#ifndef UML_HPP_
#define UML_HPP_

class Time
{
private:
	int mHour;
	int mMintute;
	int mSecond;
public:
	Time(int hour, int minute, int second);
	int getHour() const;
	void setHour(int hour);
	int getMintute() const;
	void setMintute(int mintute);
	int getSecond() const;
	void setSecond(int second);
	void setTime(int hour, int minute, int second);
	string toString();
	Time nextSecond();
	Time previousSecond();
};

class MyPoint
{
private:
	int mX;
	int mY;

};

class MyTriangle
{
private:
	MyPoint mV1;
	MyPoint mV2;
	MyPoint mV3;
public:
	MyTriangle(int x1, int y1,int x2, int y2,int x3, int y3);
	MyTriangle(MyPoint v1, MyPoint v2, MyPoint v3);
	string toString();
	double getPerimetr();
	string getType();

};


class Circle
{
private:
	double mRadius;
	string mColor;
public:
	Circle();
	Circle(double radius);
	Circle(double radius, string color);
	const string& getColor() const;
	void setColor(const string& color);
	double getRadius() const;
	void setRadius(double radius);
	double getArea();
	string toString();
};

class Cylinder: public Circle
{
	double mHeight;
public:
	Cylinder();
	Cylinder(double radius);
	Cylinder(double radius, double height);
	Cylinder(double radius, double height, string color);
	double getHeight() const;
	void setHeight(double height);
	double getVolume();
};


#endif /* UML_HPP_ */
