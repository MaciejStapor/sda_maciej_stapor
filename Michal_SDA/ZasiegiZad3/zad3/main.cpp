/*
 * main.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "a.hpp"
#include "b.hpp"

int main(int argc, char* argv[])
{
    A a(10);
    B b(3.14);
    a.Print();
    a.SetB(&b);
    b.Print();
    b.SetA(&a);
    return 0;
}
