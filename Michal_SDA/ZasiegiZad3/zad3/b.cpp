/*
 * zad1.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "b.hpp"
#include "a.hpp"
#include <iostream>

using namespace std;

B::B(double val)
:_val(val)
{
}

void B::SetA(A *a)
{
    _a = a;
    cout<<"Inside SetA()"<<endl;
    _a->Print();
}

void B::Print()
{
    cout<<"Type:B val="<<_val<<endl;
}

B::~B()
{
	cout<<"~B"<<endl;
}
