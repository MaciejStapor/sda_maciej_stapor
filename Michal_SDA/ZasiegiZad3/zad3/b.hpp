/*
 * zad1.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef B_HPP_
#define B_HPP_

class A;

class B
{
    double _val;
    A* _a;
public:

    B(double val);
    void SetA(A *a);
    void Print();
    ~B();
};



#endif /* A_HPP_ */
