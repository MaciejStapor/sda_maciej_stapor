/*
 * zad1.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "a.hpp"
#include "b.hpp"

#include <iostream>

using namespace std;

A::A(int val)
:_val(val)
{
}

void A::SetB(B *b)
{
    _b = b;
    cout<<"Inside SetB()"<<endl;
    _b->Print();
}

void A::Print()
{
    cout<<"Type:A val="<<_val<<endl;
}
A::~A()
{
	cout<<"~A"<<endl;
}

