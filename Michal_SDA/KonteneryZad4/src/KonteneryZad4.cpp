//============================================================================
// Name        : KonteneryZad4.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
using namespace std;

string usunSpacjeVector(string napis)
{
	vector<char> nowy;
	for(unsigned int i = 0; i < napis.size(); i++)
	{
		nowy.push_back(napis[i]);
	}
	for(vector<char>::iterator it = nowy.begin(); it != nowy.end(); it++)
	{
		if(*it == ' ')
		{
			nowy.erase(it);
			it--;
		}
	}
	napis = "";
	for(vector<char>::iterator it = nowy.begin(); it != nowy.end(); it++)
		{
		napis+=*it;
		}
	return napis;
}

string usunSpacjeList(string napis)
{
	list<char> nowa;

	for(unsigned int i = 0; i < napis.size(); i++)
	{
		nowa.push_back(napis[i]);
	}
	list<char>::iterator tmp = remove(nowa.begin(), nowa.end(), ' ');
	nowa.resize(distance(nowa.begin(), tmp));

	napis = "";
	for(list<char>::iterator it = nowa.begin(); it != nowa.end(); it++)
		{
		napis+=*it;
		}
	return napis;
}

int main() {
	string napis = " Ala   ma kot a  ";
	cout<<"Napis ze spacjami: "<<napis<<endl;
	cout<<"Napis bez spacji (vector): "<<usunSpacjeVector(napis)<<endl;
	cout<<"Napis bez spacji (list): "<<usunSpacjeList(napis)<<endl;

	return 0;
}
