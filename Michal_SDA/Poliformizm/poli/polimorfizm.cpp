#include <iostream>
class Dzialania
{
public:
  virtual void policz()=0;
  virtual ~Dzialania(){}
};
class Srednia
{
protected:
  float a,b,c,d;
public:
  Srednia(float aa=2, float bb=2, float cc=2, float dd=2): a(aa),b(bb),c(cc),d(dd)
{
    std::cout<<"Konstruktor sredniej"<<std::endl;
}
  void policz()
  {
    std::cout<<"Srednia to: "<<(a+b+c+d)/4<<std::endl;
  }
};
class Sumowanie: public Srednia, public Dzialania
{
private:
  float e,f;
public:
  Sumowanie(float aa=2, float bb=2, float cc=2, float dd=2, float e=2, float f=2):Srednia::Srednia(aa,bb,cc,dd)
  {
    std::cout<<"Konstruktor sumy"<<std::endl;
    this->e=e;
    this->f=f;
  }
  void policz()
  {
    std::cout<<"Suma to: "<<Srednia::a+Srednia::b+Srednia::c+Srednia::d+e+f<<std::endl;
  }
};
class Silnia: public Dzialania
{
private:
  int n;
  long suma;
public:
  Silnia(int nn=2,int suma=1)
{
    n=nn;
    this->suma=suma;
}
  void policz()
  {
    n+=1;
    while(--n)
      {
    suma*=n;
      }
    std::cout<<"Silnia to: "<< suma<<std::endl;
  }
};
void liczy(Dzialania* d)
{
  d->policz();
}
int main()
{
  Srednia srednia(3.2, 5.0);
  srednia.policz();
  Sumowanie sumowanie(1000);
  sumowanie.policz();
  Silnia silnia(3);
  silnia.policz();
  std::cout<<"virtual........................................................"<<std::endl;
  Dzialania* dzialania;
  dzialania=&sumowanie;
  dzialania->policz();
  dzialania=&silnia;
  dzialania->policz();
  std::cout<<"virtual funkcja........................................................"<<std::endl;
  liczy(dzialania);
  dzialania=&sumowanie;
  liczy(dzialania);
  return 0;
}
