#include <iostream>
#include <cmath>
#include "zad4DC.hpp"


using namespace std;

DataCzas::DataCzas(): Data(), Czas()
{

}

DataCzas::DataCzas(int rok, int miesiac, int dzien, int sekundy, int minuty, int godziny):Data(rok, miesiac, dzien), Czas(sekundy, minuty, godziny)
{

}

DataCzas::DataCzas(Data nowa, Czas nowy)
{

}

int
DataCzas::getDzien () const
{
	return mDzien;
}

void
DataCzas::setDzien (int dzien)
{
	if(dzien>31){mDzien=31;}
	else{ mDzien = dzien;}
}

int
DataCzas::getMiesiac () const
{
	return mMiesiac;
}

void
DataCzas::setMiesiac (int miesiac)
{
	if(miesiac>12){mMiesiac=12;}
	else{ mMiesiac = miesiac;}
}

int
DataCzas::getRok () const
{
	return mRok;
}

void
DataCzas::setRok (int rok)
{
	if(rok<1900){mRok=1900;}
	else{ mRok = rok;}
}

int
DataCzas::getGodziny () const
{
	return mGodziny;
}

void
DataCzas::setGodziny (int godziny)
{
	mGodziny = godziny;
}

int
DataCzas::getMinuty () const
{
	return mMinuty;
}

void
DataCzas::setMinuty (int minuty)
{
	if(minuty>=60){mMinuty=60;}
	else if(minuty<=1){mMinuty=1;}
	else {mMinuty=minuty;}
}

int
DataCzas::getSekundy () const
{
	return mSekundy;
}

void
DataCzas::setSekundy (int sekundy)
{
	if(sekundy>=60){mSekundy=60;}
	else if(sekundy<=1){mSekundy=1;}
	else {mSekundy=sekundy;}

}


void DataCzas::wypisz()
{
	cout<<"Data: "<< mDzien<<"."<<mMiesiac<<"."<<mRok<<"r."<<endl;
	cout<<"Godzina: "<<mGodziny<<":"<<mMinuty<<":"<<mSekundy<<endl;
}

DataCzas DataCzas::przesunDC(DataCzas& nowa)
{
	nowa.mDzien+=mDzien;
	nowa.mMiesiac+=mMiesiac;
	nowa.mRok+=mRok;
	Czas::przesunGodziny(mGodziny);
	Czas::przesunMinuty(mMinuty);
	Czas::przesunSekundy(mSekundy);
	return nowa;
}

DataCzas DataCzas::roznica(DataCzas& nowa)
{
	nowa.mDzien=abs(nowa.mDzien-mDzien);
	nowa.mMiesiac=abs(nowa.mMiesiac-mMiesiac);
	nowa.mRok=abs(nowa.mRok-mRok);
	nowa.mGodziny=abs(nowa.mGodziny-mGodziny);
	nowa.mMinuty=abs(nowa.mMinuty-mMinuty);
	nowa.mSekundy=abs(nowa.mSekundy-mSekundy);
	return nowa;
}
