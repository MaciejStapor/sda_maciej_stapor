/*
 * zad4Data.hpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */

#ifndef ZAD4DATA_HPP_
#define ZAD4DATA_HPP_

class Data
{
protected:
  int mRok, mDzien, mMiesiac;
public:
  Data();
  Data(int rok, int miesiac, int dzien);
  int  getDzien () const;
  void  setDzien (int dzien);
  int  getMiesiac () const;
  void  setMiesiac (int miesiac);
  int  getRok () const;
  void  setRok (int rok);
  void wypisz();
  void przesunLata(int  x);
  void przesunMiesiace(int  x);
  void przesunDni(int  x);
  Data roznica(Data& nowa);
};




#endif /* ZAD4DATA_HPP_ */
