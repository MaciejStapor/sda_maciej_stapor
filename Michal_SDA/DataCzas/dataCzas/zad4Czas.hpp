/*
 * zad4.hpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */

#ifndef ZAD4CZAS_HPP_
#define ZAD4CZAS_HPP_

class Czas
{
  protected:
  int mSekundy, mMinuty, mGodziny;
public:
  Czas();
  Czas(int sekundy, int minuty, int godziny);
  int  getGodziny () const;
  void  setGodziny (int godziny);
  int  getMinuty () const;
  void  setMinuty (int minuty);
  int  getSekundy () const;
  void  setSekundy (int sekundy);
  void wypisz();
  void przesunGodziny(int x);
  void przesunMinuty(int x);
  void przesunSekundy(int x);
  Czas roznica(Czas& nowa);
};




#endif /* ZAD4CZAS_HPP_ */
