/*
 * zad4DC.hpp
 *
 *  Created on: 29.03.2017
 *      Author: Admin
 */

#include "zad4Czas.hpp"
#include "zad4Data.hpp"

#ifndef ZAD4DC_HPP_
#define ZAD4DC_HPP_

class DataCzas : public Data, public Czas
{
public:
  DataCzas();
  DataCzas(int rok, int miesiac, int dzien, int sekundy, int minuty, int godziny);
  DataCzas(Data nowa, Czas nowy);
  int  getDzien () const;
  void  setDzien (int dzien);
  int  getMiesiac () const;
  void  setMiesiac (int miesiac);
  int  getRok () const;
  void  setRok (int rok);
  int  getGodziny () const;
  void  setGodziny (int godziny);
  int  getMinuty () const;
  void  setMinuty (int minuty);
  int  getSekundy () const;
  void  setSekundy (int sekundy);
  void wypisz();
  DataCzas przesunDC(DataCzas& nowa);
  DataCzas roznica(DataCzas& nowa);
};



#endif /* ZAD4DC_HPP_ */
