/*
 * lista.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef LISTA_HPP_
#define LISTA_HPP_

class Lista
{
protected:


	class Node
	{
	public:

		int mDane;
		Node *nastepny;

	};

	Node *poczatek;
	Node* tymczasowy;
	int licznik;

public:
	Lista();
	~Lista();

	void add( int x);
	void pokaz();

	void addPoczatek( int x);
	void usun();
	void pokazDanyElement(int x);
	void usunDanyElement(int x);


};



#endif /* LISTA_HPP_ */
