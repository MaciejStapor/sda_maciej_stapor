/*
 * main.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */
#include <iostream>

#include "lista.hpp"

using namespace std;


int main()
{

	Lista nowa;

	nowa.addPoczatek( 20);
	nowa.addPoczatek(30);
	nowa.addPoczatek( 40);
	nowa.addPoczatek( 50);
	nowa.addPoczatek( 60);
	nowa.addPoczatek( 70);

	nowa.pokaz();


	nowa.pokazDanyElement(3);
	nowa.pokazDanyElement(2);

	nowa.usunDanyElement(6);

	nowa.pokaz();



	nowa.usunDanyElement(1);

	nowa.pokaz();

	nowa.usunDanyElement(3);

	nowa.pokaz();

	nowa.usunDanyElement(3);

	nowa.pokaz();

	nowa.usun();


	return 0;
}



