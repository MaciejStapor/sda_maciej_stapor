/*
 * lista.cpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#include "lista.hpp"

#include <iostream>

using namespace std;



void Lista::addPoczatek( int x)
{
	if(poczatek==NULL)
	{

		poczatek=new Node;
		tymczasowy=poczatek;
		tymczasowy->mDane = x;
		tymczasowy->nastepny=NULL;
		licznik++;
	}
	else
	{
		tymczasowy->nastepny=new Node;
		tymczasowy=tymczasowy->nastepny;
		tymczasowy->mDane = x;
		tymczasowy->nastepny=NULL;
		licznik++;
	}

}



void Lista::pokaz()
{
	cout<<"[";
	for (Node *nowy=poczatek; nowy!=NULL; nowy=nowy->nastepny)
	{
		cout<<nowy->mDane<<" ";
	}
	cout<<"]"<<endl;
	cout<<"Licznik: "<<licznik<<endl;
}

void Lista::usun()
{
	while(poczatek!=NULL)
	{
		Node *tym=poczatek;
		poczatek=poczatek->nastepny;
		delete tym;
	}
	poczatek=NULL;
}

Lista::Lista(): poczatek(NULL), tymczasowy(NULL),licznik(0)
{

}

void Lista::pokazDanyElement(int x)
{
	int a=x;
	Node *nowy;
	nowy=poczatek;
	while(--x)
	{
		nowy=nowy->nastepny;
	}

	cout<<"Pokaz "<<a<<" element: "<<nowy->mDane<<endl;

}

void Lista::usunDanyElement(int x)
{


	if(x==1)
	{
		Node *tym=poczatek;
		cout<<"Usuwany element: "<<poczatek->mDane<<endl;
		poczatek=poczatek->nastepny;
		delete tym;
		licznik--;
	}
	else if(x>1)
	{
		x--;
		Node *nowy;
		Node *usun;
		nowy=poczatek;

		while(--x)
		{
			nowy=nowy->nastepny;
		}

		cout<<"Usuwany element: "<<nowy->nastepny->mDane<<endl;
		usun=nowy->nastepny;
		nowy->nastepny=nowy->nastepny->nastepny;
		delete usun;
		licznik--;
	}

	else
	{
		cerr<<"Nie mozna usunac elementu"<<endl;
	}





}

Lista::~Lista()
{
	while(poczatek!=NULL)
	{
		Node *tym=poczatek;
		poczatek=poczatek->nastepny;
		delete tym;
	}
	poczatek=NULL;
}


