/*
 * kalkulator.hpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#ifndef KALKULATOR_HPP_
#define KALKULATOR_HPP_

class Kalkulator
{

public:

	int add(int jeden, int dwa);
	int sub(int jeden, int dwa);
	int pow(int jeden, int dwa);
	double multi(int jeden, int dwa);
	int modulo(int jeden, int dwa);
};



#endif /* KALKULATOR_HPP_ */
