/*
 * main.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */
#include "gtest/gtest.h"
#include "kal.hpp"

TEST(CalculatorTest, ConstructorTest)
{
	Calculator c(2,3);
	EXPECT_EQ(0, c.getResult());
}

TEST(CalculatorTest, add)
{
	Calculator c(2,3);
	EXPECT_EQ(5, c.add());
	EXPECT_EQ(5, c.getResult());
	Calculator b(-2,-3);
	EXPECT_EQ(-5, b.add());
	EXPECT_EQ(-5, b.getResult());
}

TEST(CalculatorTest, substrTest)
{
	Calculator c(2,3);
	c.substr();
	EXPECT_EQ(-1, c.getResult());
}



int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}

