/*
 * zad.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */


#ifndef ZAD_HPP_
#define ZAD_HPP_

#include <iostream>
using namespace std;


class Shape
{
public:
	virtual void pole()=0;
	virtual string nazwa()=0;

	virtual ~Shape()
	{

	}
};

class Circle: public Shape
{
protected:
	int mR;
	string mNazwa;
public:
	Circle(string nazwa, int r);
	Circle(int r);
	void pole();
	string nazwa();
};

class Cylinder: public Circle
{
	int mH;

public:
	Cylinder(int h, int r);
	void pole();
	string nazwa();
};




#endif /* ZAD_HPP_ */
