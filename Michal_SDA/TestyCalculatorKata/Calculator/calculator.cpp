/*
 * calculator.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#include "calculator.hpp"
#include <string>
#include <cstdlib>


int StringCalculate::add(std::string numbers)
{
	if(numbers.size()==0)
	{
		return 0;
	}
	else if(numbers.find(',')==std::string::npos)
	{
		return std::atoi(numbers.c_str());
	}
	else
	{
		int comaPosition = numbers.find(',');
		std::string numberOne = numbers.substr(0,comaPosition);
		std::string nuberTwo = numbers.substr(comaPosition+1, std::string::npos);

		int numberOneInt =std::atoi(numberOne.c_str());
		int numberTwoInt=std::atoi(nuberTwo.c_str());

		return numberOneInt+numberTwoInt;
	}


	return -1;
}
