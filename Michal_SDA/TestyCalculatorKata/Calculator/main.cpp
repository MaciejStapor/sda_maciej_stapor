/*
 * main.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */

#include "calculator.hpp"
#include "gtest/gtest.h"

TEST(StringCalculator, EmptyString)
{
	StringCalculate a;

	EXPECT_EQ(0, a.add(""));
}

TEST(StringCalculator, OneNumber)
{
	StringCalculate a;

	EXPECT_EQ(1, a.add("1"));
	EXPECT_EQ(121, a.add("121"));
}

TEST(StringCalculator, TwoNumbers)
{
	StringCalculate a;

	EXPECT_EQ(3, a.add("1,2"));
	EXPECT_EQ(12, a.add("10,2"));
	EXPECT_EQ(120, a.add("100,20"));
}

TEST(StringCalculator, UnknowNumbers)
{
	StringCalculate a;

	EXPECT_EQ(3, a.add("1,2"));
	EXPECT_EQ(12, a.add("10,2"));
	EXPECT_EQ(120, a.add("100,20"));
	EXPECT_EQ(421, a.add("1,20,100,300"));
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
