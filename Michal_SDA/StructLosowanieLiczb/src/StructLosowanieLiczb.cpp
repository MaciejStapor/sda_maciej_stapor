//============================================================================
// Name        : StructLosowanieLiczb.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;


struct UserInputData
{
	int mMax;
	int mMin;
	int mIloscLiczb;
	bool mCzyRosnaco;
};

UserInputData pobierzDane(int max, int min, int ilosc, bool czyRosnaco)
{
	UserInputData x;
	x.mMax=max;
	x.mMin=min;
	x.mIloscLiczb=ilosc;
	x.mCzyRosnaco=czyRosnaco;

	return x;
}

int* wylosuj(UserInputData &max)
{
	int *tab=new int[max.mIloscLiczb];

	for(int i=0; i<max.mIloscLiczb; i++)
	{
		tab[i]=rand()%((max.mMax-max.mMin))+((max.mMax-(max.mMax-max.mMin)));
	}

	return tab;
}

void pokaz(UserInputData &max, int *tab)
{
	cout<<"[ ";
	for(int i=0; i<max.mIloscLiczb; i++)
	{
		cout<<tab[i]<<" ";
	}
	cout<<"]"<<endl;
}



void sort(UserInputData &max, int *tab)
{
	if(max.mCzyRosnaco)
	{
		int x=max.mIloscLiczb-1;
		for(int k=0; k<max.mIloscLiczb; k++)
		{
			for(int i=0; i<x; i++)
			{
				if(tab[i]>tab[i+1])
				{
					int tmp=tab[i];
					tab[i]=tab[i+1];
					tab[i+1]=tmp;
				}
				--x;
			}
		}
	}
}


int main() {
	srand(time(NULL));
	UserInputData maciej = pobierzDane(100,30,20,1);
	int *tab=wylosuj(maciej);
	sort(maciej, tab);
	pokaz(maciej, tab);
	delete tab;
	return 0;
}
