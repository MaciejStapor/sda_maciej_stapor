/*
 * zad6Pesel.hpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */
#include <iostream>
using namespace std;
#ifndef ZAD6PESEL_HPP_
#define ZAD6PESEL_HPP_

class Pesel
{
	int *mPesel;
public:
	Pesel();
	Pesel(int* pesel);
	int* getPesel() const;
	void setPesel(int *pesel);
	void pobierzDate(int *pesel);
	string naStringa(int *pesel);
};




#endif /* ZAD6PESEL_HPP_ */
