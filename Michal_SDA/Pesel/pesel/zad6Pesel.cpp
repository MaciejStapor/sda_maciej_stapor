/*
 * zad6Pesel.cpp
 *
 *  Created on: 29.03.2017
 *      Author: RENT
 */

#include <iostream>

#include "zad6Pesel.hpp"

using namespace std;


Pesel::Pesel()
{
	int pesel[11]={8,9,0,7,2,9,1,5,0,1,8};
	mPesel=pesel;
}

int* Pesel::getPesel() const
{
	return mPesel;
}

void Pesel::setPesel(int* pesel)
{
	if(pesel == 0)
	{
		cerr<<"Blad pesel niepoprawny";
	}
	else
	{
		const int wagi[10] = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };
		int suma = 0,pomocnicza=0;
		for (int i = 0; i < 10; ++i)
		{
			suma += pesel[i] * wagi[i];
		}

		pomocnicza=  10-(suma % 10);


		if (pomocnicza == pesel[10])
		{
			mPesel = pesel;
			cout<<"PESEL POPRAWNY"<<endl;
		}
		else
		{
			cerr<<"Blad pesel niepoprawny";
		}

	}
}

Pesel::Pesel(int* pesel)
{
	setPesel(pesel);
}

void Pesel::pobierzDate(int *pesel)
{
	int a=0,b=0,c=0;
	a=1900+((pesel[0]*10)+pesel[1]);
	b=(pesel[2]*10)+pesel[3];
	c=(pesel[4]*10)+pesel[5];

	cout<<"Data urodzenia: "<<a<<"."<<b<<"."<<c<<endl;
}

string Pesel::naStringa(int *pesel)
{
	string pom="";
	for(int i=0; i<11; i++)
	{
		pom+=(pesel[i]+'0');
	}

	return pom;
}
